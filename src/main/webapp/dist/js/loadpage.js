function loadPage(id, url) {
    $.ajax({
        beforeSend: function() {
            $("#"+id).empty().append('<div style = "margin-top: 20%; padding: 0; width: 100%; text-align: center">' +
                '<h1 style = "color: #707070; font-size: 15px; line-height: 100%">&nbsp;&nbsp;加载中，请稍后……</h1>' +
            '</div>');
        },
        type: "post",
        url: url,
        cache: false,

        success: function(msg) {
            $("#" + id).empty().append(msg);
        },
        error: function() {
            alert('加载页面' + url + '时出错！');
        }
    });
}


function Add(name, url) {
    var form = new FormData(document.getElementById(name.toString()));

    $.ajax({
        type: "post",
        processData:false,
        contentType:false,
        async:true,
        cache:false,
        data: form,
        url: url,
        success: function(msg) {
            alert(msg);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
            alert("新建失败！");
        }
    });
}


function Modify(url, name) {
    $.ajax({
        type: "post",
        dataType: "text",
        data: $("#form1").serialize(),      // 序列化表单，提交所有表项
        url: url,
        success: function(msg) {
            if(msg === "no-manager") {
                $("#managercheck").empty().append("✲该管理员不存在").css("color", "#FF7043");
                return;
            }
            if(msg === "no-scenicarea") {
                $("#belongToSceniccheck").empty().append("✲该景区不存在").css("color", "#FF7043");
                return;
            }
            if(msg === "no-scenicarea-infra") {
                $("#infraInSceniccheck").empty().append("✲该景区不存在").css("color", "#FF7043");
                return;
            }
            alert('修改成功！');
            loadPage('tab_content', name);  // 修改成功后跳转到查看页面
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
            alert("修改失败！");
        }
    });
}


function Del(url, name) {
    $.ajax({
        type: "get",
        dataType: "text",
        url: url,
        success: function(msg) {
            // alert("删除成功！");
            $("#tab_content").load("../controller/autocode/" + name);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
            alert(errorThrown + textStatus);
            alert("删除失败！");
        }
    });
}


function getImage(url) {
    $.ajax({
        beforeSend: function() {
            $("#modelsrc").empty().append('<div style = "margin-top: 20%; padding: 0; width: 100%; text-align: center">' +
                '<h1 style = "color: #707070; font-size: 15px; line-height: 100%">&nbsp;&nbsp;加载中，请稍后……</h1>' +
                '</div>');
        },
        type: "get",
        dataType: "text",
        url: url,
        success: function(msg) {
            $("#modelsrc").empty().append('<img src = "' + msg + '">');
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
            alert(errorThrown + textStatus);
            alert("获取图片失败！");
        }
    });
}


function getText(url) {
    $.ajax({
        beforeSend: function() {
            $("#modeltext").empty().append('<div style = "margin-top: 20%; padding: 0; width: 100%; text-align: center">' +
                '<h1 style = "color: #707070; font-size: 15px; line-height: 100%">&nbsp;&nbsp;加载中，请稍后……</h1>' +
                '</div>');
        },
        type: "get",
        dataType: "text",
        url: url,
        success: function(msg) {
            $("#modeltext").empty().append(msg);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown){
            alert(errorThrown + textStatus);
            alert("获取图片失败！");
        }
    });
}
