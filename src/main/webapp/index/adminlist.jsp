<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>管理员列表</title>
    <script src="../js/jquery-3.3.1.js"></script>
</head>
<body>

    <table>
        <c:forEach var="listitem" items="${adminlist}">
            <tr>
                <td>${listitem.adminName}</td>
                <td>${listitem.adminPassword}</td>
                <td><button class="upd" data-id="${listitem.adminId}">修改</button></td>
                <td><button class="del" data-id="${listitem.adminId}">删除</button></td>
            </tr>
        </c:forEach>
    </table>

</body>



<script>
    $(document).ready(function() {
        var mess = "${mess}";
        if (mess === "succ") {
            alert("删除成功");
        } else if (mess === "err") {
            alert("删除失败");
        }
        $(".del").click(function () {
             var id = $(this).data("id");
             if (confirm("确定删除这条记录吗？")) {
                 location.href = "/delete?id=" +id;
             }
        });
    });
</script>
</html>
