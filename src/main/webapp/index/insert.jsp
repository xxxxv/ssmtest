<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>

<head>
    <title>插入一条管理员信息</title>
    <script src="../js/jquery-3.3.1.js"></script>
</head>

<body>

    <form id="myform" method="post" style="margin: 50px auto; width: 400px; height: 400px;">
        <span>管理员名：</span>
        <br/>
        <input type="text" name="name" />
        <br/>
        <span>管理员密码：</span>
        <br/>
        <input type="password" name="password" />
        <br/><br/><br/>
        <input id="subbtn" type="button" value="添加"/>
        <br/><br/><br/>
        <span id="mess" style="font-size: 16px; color: red; font-weight: bold;"></span>
    </form>

    </div>

</body>

    <script>
        $(document).ready(function () {
            $("#subbtn").click(function() {
                $.ajax({
                    type: "POST",
                    dataType: 'html',
                    url: "/insert",
                    data: $("#myform").serialize(),
                    success: function(result) {
                        if ($.trim(result) === "success") {
                            location.href = "/getlist";
                        }
                        $("#mess").empty().append(result).show(0).delay(1500).hide(0);
                    },
                    error: function(data) {
                        console.log(data);
                    }
                })
            });
        });
    </script>

</html>