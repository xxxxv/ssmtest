<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: LKP
  Date: 2019/1/16
  Time: 14:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- 公共URL -->
<%@ page import="cn.lkp.Util.CommonURL" %>
<c:set var="preurl" value="<%=CommonURL.COMMON_URL%>" />

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>个人网站管理后台 V1.0</title>

    <!-- Bootstrap Core CSS -->
    <link href="${preurl}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="${preurl}/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="${preurl}/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="${preurl}/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="${preurl}/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .table {
            table-layout: fixed;
            font-size: 14px;
        }

        .watch-img, .watch-img:hover {
            color: #449d44;
            text-decoration: none;
        }


        td {
            /*overflow: hidden;*/
            vertical-align: middle;
            word-wrap:break-word;
            word-break:break-all;
        }

        .btn {
            font-size: 14px;
        }
    </style>
</head>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">个人网站管理后台 <strong>v1.0</strong></a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i> ${sessionScope.username}</a>
                    </li>
                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> 修改个人信息</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="./login.html"><i class="fa fa-sign-out fa-fw"></i> 注销</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> 项目管理<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="javascript:loadPage('row-2','/pages/addProject.jsp')">上传项目</a>
                            </li>
                            <li>
                                <a href="#">修改项目</a>
                            </li>
                            <li>
                                <a href="#">删除项目</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> 动态管理<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#">创建新动态</a>
                            </li>
                            <li>
                                <a href="#">修改动态</a>
                            </li>
                            <li>
                                <a href="#">删除动态</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> 个人信息管理<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#">上传信息</a>
                            </li>
                            <li>
                                <a href="#">修改信息</a>
                            </li>
                            <li>
                                <a href="#">删除信息</a>
                            </li>
                        </ul>

                    </li>

                    <li class="active">
                        <a href="#"><i class="fa fa-sitemap fa-fw"></i> 友情链接管理<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="/admin/addlinks">添加链接信息</a>
                            </li>
                            <li>
                                <a href="/admin/displaylinks" class="active">查看链接信息</a>
                            </li>
                        </ul>
                    </li>


                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper">
        <div class="row" id="row-1">
            <div class="col-lg-12">
                <h3 class="page-header">查看链接信息</h3>
            </div>
        </div>

        <div class="links-container">
            <table class="table table-striped">
                <%--<caption></caption>--%>
                <thead>
                <tr>
                    <th style="width: 50px"><input type="checkbox" /></th>
                    <th style="width: 100px;">序号</th>
                    <th>URL</th>
                    <th>链接标题</th>
                    <th>链接描述</th>
                    <th style="width: 100px;">链接图片</th>
                    <th style="width: 360px;">操作</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="link" items="${linklist}" varStatus="status">
                    <tr>
                        <td><input type="checkbox" /></td>
                        <td>${status.index + 1} </td>
                        <td>${link.linkUrl}</td>
                        <td>${link.linkTitle}</td>
                        <td>${link.linkDescription}</td>
                        <td><a href="${link.linkImage}" class="watch-img" target="_blank">查看</a></td>
                        <td>
                            <button type="button" class="btn btn-default">修改</button>
                            <button type="button" class="btn btn-danger" data-id="${link.linkId}">删除</button>
                            <button type="button" class="btn btn-success">详细信息</button>
                        </td>
                    </tr>
                </c:forEach>

                </tbody>
            </table>
        </div>

    </div>


</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="${preurl}/vendor/jquery/jquery.min.js"></script>
<script src="${preurl}/js/jquery.form.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${preurl}/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="${preurl}/vendor/metisMenu/metisMenu.min.js"></script>



<!-- Custom Theme JavaScript -->
<script src="${preurl}/dist/js/sb-admin-2.js"></script>
<script src="${preurl}/dist/js/loadpage.js"></script>

<script>
    $(document).ready(function () {
        $(".btn-danger").click(function () {
            if (confirm("确定要删除这条信息吗？")) {
                location.href = "/admin/deletelink?id=" + $(this).data("id");
            }
        });
    });
</script>

</body>

</html>
