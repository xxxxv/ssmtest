<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- 公共URL -->
<%@ page import="cn.lkp.Util.CommonURL" %>
<c:set var="preurl" value="<%=CommonURL.COMMON_URL%>" />


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>个人网站管理后台 V1.0</title>

    <!-- Bootstrap Core CSS -->
    <link href="${preurl}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="${preurl}/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="${preurl}/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="${preurl}/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="${preurl}/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .imgbutton {
            position: relative;
            width: 1000px;
            padding-left: 30px;
        }

        .upbtn {
            margin-right: 20px;
        }

        .filebtn-item {
            display: none;
            margin-left: 10px;
            color: #4cae4c;
            cursor: pointer;
        }

        .upload-err {
            display: none;
            margin-left: 10px;
            color: red;
        }
    </style>

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">个人网站管理后台 <strong>v1.0</strong></a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i> ${sessionScope.username}</a>
                    </li>
                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> 修改个人信息</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="./login.html"><i class="fa fa-sign-out fa-fw"></i> 注销</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> 项目管理<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="javascript:loadPage('row-2','/pages/addProject.jsp')">上传项目</a>
                            </li>
                            <li>
                                <a href="#">修改项目</a>
                            </li>
                            <li>
                                <a href="#">删除项目</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> 动态管理<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#">创建新动态</a>
                            </li>
                            <li>
                                <a href="#">修改动态</a>
                            </li>
                            <li>
                                <a href="#">删除动态</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> 个人信息管理<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#">上传信息</a>
                            </li>
                            <li>
                                <a href="#">修改信息</a>
                            </li>
                            <li>
                                <a href="#">删除信息</a>
                            </li>
                        </ul>

                    </li>

                    <li class="active">
                        <a href="#"><i class="fa fa-sitemap fa-fw"></i> 友情链接管理<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="/admin/addlinks" class="active">添加链接信息</a>
                            </li>
                            <li>
                                <a href="/admin/displaylinks">查看链接信息</a>
                            </li>
                        </ul>
                    </li>


                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper">
        <div class="row" id="row-1">
            <div class="col-lg-12">
                <h3 class="page-header">添加新链接信息</h3>
            </div>
        </div>
        <section class="panel">
            <div class="panel-body">
                <form id="linksform" action="/admin/insertlink" method="post" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="linkURL" class="col-lg-1 col-sm-1 control-label">URL</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" id="linkURL" name="linkurl">
                            <p class="help-block">URL不能为空</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="linkTitle" class="col-lg-1 col-sm-1 control-label">链接标题</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" id="linkTitle" name="linktitle">
                            <p class="help-block">链接标题不能为空</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="linkDesc" class="col-lg-1 col-sm-1 control-label">链接描述</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" id="linkDesc" name="linkdesc">
                            <p class="help-block">链接描述可以为空</p>
                        </div>
                    </div>

                    <div class="form-group" id="upload-panel">
                        <div class="imgbutton">
                            <div class="filebtn">
                                <button type="button" class="btn btn-success btn-icon upbtn">
                                    <i class="fas fa-cloud-upload-alt"></i>
                                    <span>上传图片</span>
                                </button>
                                <input type="file" name="file" class="fileup" style="opacity: 0; float: left; position: absolute; top: 0; left: 0; z-index: -1"/>
                                <span class="filebtn-item watch" data-url="">查看图片</span>
                                <span class="filebtn-item concel">取消上传</span>
                                <span class="upload-err">上传失败，图片大小不能超过10M</span>
                            </div>
                            <div class="uprogress">
                                <div class="spinner">
                                    <div class="bounce1"></div>
                                    <div class="bounce2"></div>
                                    <div class="bounce3"></div>
                                </div>
                            </div>
                        </div>
                        <div class="showimg">
                            <div class="img-container">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-10">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="addurl">选用该图片作为图片链接
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-4 col-lg-10">
                            <button type="submit" class="btn btn-success" style="width: 300px; height: 40px;">提交</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>


</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="${preurl}/vendor/jquery/jquery.min.js"></script>
<script src="${preurl}/js/jquery.form.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${preurl}/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="${preurl}/vendor/metisMenu/metisMenu.min.js"></script>



<!-- Custom Theme JavaScript -->
<script src="${preurl}/dist/js/sb-admin-2.js"></script>
<script src="${preurl}/dist/js/loadpage.js"></script>

<script>

    // 上传路径
    var upurl;

    $(document).ready(function() {
        // fileupload
        $(".upbtn").click(function () {
            $(this).siblings().eq(0).click();
        });
        $("#upload-panel").on("change", ".fileup", function() {
            var that = $(this);
            // var loading = that.parent().parent().find(".uprogress");
            var button = that.parent().parent();
            var img = that.parent().parent().parent().find(".showimg").find(".img-container");
            // var modalbody = $("#myModal").find(".modal-dialog").find(".modal-content").find(".modal-body");
            // var modal = $("#myModal");
            that.wrap("<form action='/image/upload' method='post' enctype='multipart/form-data'></form>");
            that.parent().ajaxSubmit({
                dataType: 'text',
                beforeSend: function () {
                    //loading.show();
                },
                uploadProgress: function (event, position, total, percentComplete) {
                    //loading.show();
                },
                success: function (data) {
                    that.unwrap();
                    $(".upbtn").attr("disabled", "disabled");
                    upurl = $.trim(data);
                    $(".watch").data("url", upurl);
                    $(".filebtn-item").show(100);
                    $(".upload-err").text("上传成功！").css("color", "#4cae4c").show(100);
                    // loading.hide();
                    // var url = JSON.parse(data);
                    // img.append("<img src='" + url.imgurl + "' class='img-rounded imgtip' height='180'>");
                    // $("#index-form").append("<input type='hidden' class='imgpara' name='imgurls' value='" + url.imgurl + "'>");
                    // img.parent().show();
                },
                error: function (xhr) {
                    // loading.hide();
                    // modalbody.text("上传失败，上传的图片大小不能超过10MB，且仅允许上传png，jpg，jpeg格式！");
                    // modal.modal();
                    that.unwrap();
                    button.show();
                }
            });
        });

        $(".watch").click(function() {
            var url = "http://localhost:8080" + $(this).data("url");
            window.open(url);
        });

        $(".concel").click(function() {
            $(".upbtn").removeAttr("disabled");
            $(".filebtn-item").hide(100);
            $(".upload-err").text("").hide(100);
        });

        $("#addurl").click(function() {
            if (!$(this).is(":checked")) {
                $("#linkurl").remove();
            }
            else {
                $("#linksform").append("<input id='linkurl' type='hidden' name='linkimage' value='" + upurl + "'>");
            }
        });
    });
</script>

</body>

</html>
