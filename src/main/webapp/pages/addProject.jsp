<%--
  Created by IntelliJ IDEA.
  User: 杨孟新
  Date: 2019/1/14
  Time: 15:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="panel-heading">
    新增项目
</div>
<div class="panel-body">
    <div class="row">
        <div class="col-lg-12">
            <form  id="form-project">
                <div class="form-group">
                    <label>项目名称</label>
                    <input name="projectName" class="form-control" placeholder="请输入项目名称">
                </div>
                <div class="form-group">
                    <label>项目简介</label>
                    <input name="projectSim" class="form-control" placeholder="请输入项目简介">
                </div>
                <div class="form-group">
                    <label>项目主图</label>
                    <input name="projectPicture" type="file">
                </div>
                <div class="form-group">
                    <label>项目描述</label>
                    <textarea name="projectContent" class="form-control" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <input class="btn btn-primary btn-lg btn-block"
                           onclick="javascript:Add('form-project','/addProject');"
                           value="创建"
                    >
                </div>
            </form>
        </div>
    </div>
</div>
