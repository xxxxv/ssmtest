$('.asyn_fill').each(function() {
    var url = $(this).attr('controller-url');
    var mine = $(this);
    var data = "";
    var urlarr = url.split('?');
    if (urlarr.length > 1) {
        url = urlarr[0];
        data = urlarr[1];
    }
    $.ajax({
        type: "POST",
        url: url,
        data: data,

        success: function(data) {
            mine.html(data);
        }

    })
});