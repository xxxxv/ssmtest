package cn.lkp.service;

import org.springframework.stereotype.Service;


public interface AdminService {

    String adminLogin(String username,String password);
}
