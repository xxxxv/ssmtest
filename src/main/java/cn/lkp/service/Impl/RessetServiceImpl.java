package cn.lkp.service.Impl;


import cn.lkp.bean.Resset;
import cn.lkp.bean.RessetExample;
import cn.lkp.dao.RessetMapper;
import cn.lkp.service.RessetService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RessetServiceImpl implements RessetService {

    @Autowired
    private RessetMapper mapper;

    @Autowired
    private RessetExample example;

    private Logger logger = Logger.getLogger(RessetServiceImpl.class);

    /**
     * @description 在资源集表中插入数据
     * @param resset
     * @return 返回插入记录的id
     */
    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
    public int Add(Resset resset) {
        try {
            return mapper.insert(resset);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("插入链接信息出错：" + e);
        }
        return -1;
    }


    /**
     * @description 查询资源集合
     * @param typeName
     * @return 资源集合
     */
    @Override
    public List<Resset> getResourcesId(String typeName) {
        RessetExample.Criteria cri = example.createCriteria();
        cri.andSetNameEqualTo(typeName);
        try {
            return mapper.selectByExample(example);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("插入链接信息出错：" + e);
        }
        return null;
    }
}
