package cn.lkp.service.Impl;

import cn.lkp.bean.Admin;
import cn.lkp.bean.AdminExample;
import cn.lkp.dao.AdminMapper;
import cn.lkp.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    AdminMapper adminMapper;

    @Override
    public String adminLogin(String username, String password) {
        AdminExample adminExample = new AdminExample();
        AdminExample.Criteria criteria = adminExample.createCriteria();
        criteria.andAdminNameEqualTo(username);
        criteria.andAdminPasswordEqualTo(password);
        List<Admin> admins = adminMapper.selectByExample(adminExample);
        if (admins.size() >= 1)
            return username;
        return "fail";
    }
}
