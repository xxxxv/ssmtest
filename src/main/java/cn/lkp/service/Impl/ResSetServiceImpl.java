package cn.lkp.service.Impl;

import cn.lkp.bean.*;
import cn.lkp.dao.HypertextMapper;
import cn.lkp.dao.ImagesMapper;
import cn.lkp.dao.RessetMapper;
import cn.lkp.service.ResSetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ResSetServiceImpl implements ResSetService {

    @Autowired
    ImagesMapper imagesMapper;
    @Autowired
    Images images;
    @Autowired
    HypertextMapper hypertextMapper;
    @Autowired
    Hypertext hypertext;

    @Autowired
    RessetMapper ressetMapper;
    @Autowired
    Resset resset;

    @Override
    public String AddImages(String ImageTitle, String ImageUrl) {
        images.setImageTitle(ImageTitle);
        images.setImageUrl(ImageUrl);
        imagesMapper.insert(images);
        long l = imagesMapper.countByExample(new ImagesExample());
        resset.setIdInType((int) l);
        resset.setSetName("project_sim");
        resset.setSetType("images");
        ressetMapper.insert(resset);
        return "success";
    }

    @Override
    public String AddHypertexts(String textTitle, String textViceTitle, String textImageUrl, String textContent) {
        hypertext.setTextTitle(textTitle);
        hypertext.setTextViceTitle(textViceTitle);
        hypertext.setTextImageUrl(textImageUrl);
        hypertext.setTextContent(textContent);
        hypertextMapper.insert(hypertext);
        long l = hypertextMapper.countByExample(new HypertextExample());
        resset.setIdInType((int) l);
        resset.setSetName("project_com");
        resset.setSetType("hypertext");
        ressetMapper.insert(resset);
        return "success";
    }


}
