package cn.lkp.service.Impl;

import cn.lkp.bean.Hyperlinks;
import cn.lkp.bean.HyperlinksExample;
import cn.lkp.bean.Resset;
import cn.lkp.dao.HyperlinksMapper;
import cn.lkp.service.LinkService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class LinkServiceImpl implements LinkService {

    @Autowired
    private HyperlinksMapper mapper;

    private Logger logger = Logger.getLogger(LinkServiceImpl.class);

    /**
     * @description 在超链接表中插入数据
     * @param link
     * @return 返回插入记录的id
     */
    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
    public int Add(Hyperlinks link) {
        try {
            return mapper.insert(link);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("插入链接信息出错：" + e);
        }
        return -1;
    }

    /**
     * @description 返回数据库的记录数
     * @param
     * @return 返回插入记录的id
     */
    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
    public Hyperlinks getLinkById(int id) {
        try {
            return mapper.selectByPrimaryKey(id);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("查询链接信息出错：" + e);
        }
        return null;
    }


    /**
     * @description 返回超链接信息列表
     * @param
     * @return 返回插入记录的id
     */
    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
    public List<Hyperlinks> getList(List<Resset> resset) {
        try {
            List<Hyperlinks> links = new ArrayList<>();

            for (Resset rs : resset) {
                Hyperlinks link = getLinkById(rs.getIdInType());
                if (link != null) {
                    links.add(link);
                }
            }
            return links;

        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("查询链接信息出错：" + e);
        }

        return null;
    }

    @Override
    public boolean deleteById(int id) {
        try {
            mapper.deleteByPrimaryKey(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("删除链接信息出错：" + e);
        }
        return false;
    }
}
