package cn.lkp.service;

import cn.lkp.bean.Hyperlinks;
import cn.lkp.bean.Resset;

import java.util.List;

public interface LinkService {

    public int Add(Hyperlinks link);

    public Hyperlinks getLinkById(int id);

    public List<Hyperlinks> getList(List<Resset> resset);

    public boolean deleteById(int id);

}
