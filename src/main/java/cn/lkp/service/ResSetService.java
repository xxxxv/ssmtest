package cn.lkp.service;

import org.springframework.stereotype.Service;


public interface ResSetService {

    String AddImages(String ImageTitle,String ImageUrl );
    String AddHypertexts(String textTitle,String textViceTitle,String textImageUrl,String textContent );

}
