package cn.lkp.service;

import cn.lkp.bean.Resset;

import java.util.List;

public interface RessetService {

    public int Add(Resset resset);

    public List<Resset> getResourcesId(String typeName);

}
