package cn.lkp.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Service
public interface FileService {
    public String fileUpload(MultipartFile file, String path) throws IOException;

    public void fileDownload(HttpServletRequest request, HttpServletResponse response, String filename);

}
