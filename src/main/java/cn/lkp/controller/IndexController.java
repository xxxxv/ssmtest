package cn.lkp.controller;

import cn.lkp.bean.Hypertext;
import cn.lkp.bean.HypertextExample;
import cn.lkp.dao.HypertextMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class IndexController {

    @Autowired
    Hypertext hypertext;
    @Autowired
    HypertextMapper hypertextMapper;


    @RequestMapping(value = "/index")
    public String news_center(Model model)
    {
        List<Hypertext> hypertexts = hypertextMapper.selectByExample(new HypertextExample());
        model.addAttribute("hypertexts",hypertexts);
        return "template/index";
    }
}
