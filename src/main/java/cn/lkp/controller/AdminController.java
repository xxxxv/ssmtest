package cn.lkp.controller;

import cn.lkp.service.AdminService;
import cn.lkp.service.Impl.AdminServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

@Controller
public class AdminController {

    @Autowired
    AdminService adminService;

    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public String adminLogin(String email, String password, Model model)
    {
        String s = adminService.adminLogin(email, password);
        if ("fail".equals(s))
            return "pages/error";
        model.addAttribute("username",s);
        return "pages/index";
    }
}
