package cn.lkp.controller;

import cn.lkp.bean.Hyperlinks;
import cn.lkp.bean.Resset;
import cn.lkp.service.LinkService;
import cn.lkp.service.RessetService;
import javafx.scene.control.Hyperlink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;


@Controller
public class userController {

    @Autowired
    RessetService ressetService;

    @Autowired
    LinkService linkService;


    @RequestMapping(value = "/userindex", method = RequestMethod.GET)
    public String displayUserPage(Model model) {

        List<Resset> resset = ressetService.getResourcesId("project-hyperlink");
        List<Hyperlinks> links = new ArrayList<>();

        for (Resset rs : resset) {
            Hyperlinks link = linkService.getLinkById(rs.getIdInType());
            if (link != null) {
                links.add(link);
            }
        }

        // 将值传给前端
        model.addAttribute("links", links);

        return "userpages/index";
    }
}
