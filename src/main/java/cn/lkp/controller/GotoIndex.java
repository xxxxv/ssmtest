package cn.lkp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class GotoIndex {

    @RequestMapping(value = "/admin/index", method = RequestMethod.GET)
    public String toManagerIndex() {
        return "pages/index";
    }
}
