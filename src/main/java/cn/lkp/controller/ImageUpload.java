package cn.lkp.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


@Controller
public class ImageUpload {

    @RequestMapping(value = "/image/upload", method = RequestMethod.POST)
    @ResponseBody
    public String imageUpload(@RequestParam MultipartFile file, HttpServletRequest request) throws IOException {

        // 格式化日期，作为区分文件的标志
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSS");
        String res = sdf.format(new Date());

        // 存放上传文件的实际位置
        String rootPath = request.getSession().getServletContext().getRealPath("upload/");
        // 原文件名
        String fileName = file.getOriginalFilename();
        // 新文件名
        String newFileName = res + fileName.substring(fileName.lastIndexOf("."));

        // 新文件
        File newFile = new File(rootPath + File.separator + newFileName);
        // 判断上传文件的目标目录是否存在
        if (!newFile.getParentFile().exists()) {
            // 如果目标目录不存在，则创建该目录
            newFile.getParentFile().mkdir();
        }

        // 将文件写入磁盘
        file.transferTo(newFile);

        // 返回部分文件路径
        return "/upload/" + newFileName;
    }
}
