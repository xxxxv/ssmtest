package cn.lkp.controller;

import cn.lkp.service.FileService;
import cn.lkp.service.ResSetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Controller
public class ProjectController {

    @Autowired
    FileService fileService;

    @Autowired
    ResSetService resSetService;



    @RequestMapping(value = "/addProject",method = RequestMethod.POST,produces = "application/text;charset=UTF-8")
    @ResponseBody
    public String addProject(String projectName, String projectSim, MultipartFile projectPicture, String projectContent){
        try {
            String src =  fileService.fileUpload(projectPicture, null);
            resSetService.AddImages(projectName,src);
            resSetService.AddHypertexts(projectName,projectSim,src,projectContent);
            return "创建成功";
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "controller fail";
    }
}
