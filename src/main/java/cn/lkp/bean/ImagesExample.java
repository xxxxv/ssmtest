package cn.lkp.bean;

import java.util.ArrayList;
import java.util.List;

public class ImagesExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ImagesExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * images 2019-01-13
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andImageIdIsNull() {
            addCriterion("image_id is null");
            return (Criteria) this;
        }

        public Criteria andImageIdIsNotNull() {
            addCriterion("image_id is not null");
            return (Criteria) this;
        }

        public Criteria andImageIdEqualTo(Integer value) {
            addCriterion("image_id =", value, "imageId");
            return (Criteria) this;
        }

        public Criteria andImageIdNotEqualTo(Integer value) {
            addCriterion("image_id <>", value, "imageId");
            return (Criteria) this;
        }

        public Criteria andImageIdGreaterThan(Integer value) {
            addCriterion("image_id >", value, "imageId");
            return (Criteria) this;
        }

        public Criteria andImageIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("image_id >=", value, "imageId");
            return (Criteria) this;
        }

        public Criteria andImageIdLessThan(Integer value) {
            addCriterion("image_id <", value, "imageId");
            return (Criteria) this;
        }

        public Criteria andImageIdLessThanOrEqualTo(Integer value) {
            addCriterion("image_id <=", value, "imageId");
            return (Criteria) this;
        }

        public Criteria andImageIdIn(List<Integer> values) {
            addCriterion("image_id in", values, "imageId");
            return (Criteria) this;
        }

        public Criteria andImageIdNotIn(List<Integer> values) {
            addCriterion("image_id not in", values, "imageId");
            return (Criteria) this;
        }

        public Criteria andImageIdBetween(Integer value1, Integer value2) {
            addCriterion("image_id between", value1, value2, "imageId");
            return (Criteria) this;
        }

        public Criteria andImageIdNotBetween(Integer value1, Integer value2) {
            addCriterion("image_id not between", value1, value2, "imageId");
            return (Criteria) this;
        }

        public Criteria andImageUuidIsNull() {
            addCriterion("image_uuid is null");
            return (Criteria) this;
        }

        public Criteria andImageUuidIsNotNull() {
            addCriterion("image_uuid is not null");
            return (Criteria) this;
        }

        public Criteria andImageUuidEqualTo(String value) {
            addCriterion("image_uuid =", value, "imageUuid");
            return (Criteria) this;
        }

        public Criteria andImageUuidNotEqualTo(String value) {
            addCriterion("image_uuid <>", value, "imageUuid");
            return (Criteria) this;
        }

        public Criteria andImageUuidGreaterThan(String value) {
            addCriterion("image_uuid >", value, "imageUuid");
            return (Criteria) this;
        }

        public Criteria andImageUuidGreaterThanOrEqualTo(String value) {
            addCriterion("image_uuid >=", value, "imageUuid");
            return (Criteria) this;
        }

        public Criteria andImageUuidLessThan(String value) {
            addCriterion("image_uuid <", value, "imageUuid");
            return (Criteria) this;
        }

        public Criteria andImageUuidLessThanOrEqualTo(String value) {
            addCriterion("image_uuid <=", value, "imageUuid");
            return (Criteria) this;
        }

        public Criteria andImageUuidLike(String value) {
            addCriterion("image_uuid like", value, "imageUuid");
            return (Criteria) this;
        }

        public Criteria andImageUuidNotLike(String value) {
            addCriterion("image_uuid not like", value, "imageUuid");
            return (Criteria) this;
        }

        public Criteria andImageUuidIn(List<String> values) {
            addCriterion("image_uuid in", values, "imageUuid");
            return (Criteria) this;
        }

        public Criteria andImageUuidNotIn(List<String> values) {
            addCriterion("image_uuid not in", values, "imageUuid");
            return (Criteria) this;
        }

        public Criteria andImageUuidBetween(String value1, String value2) {
            addCriterion("image_uuid between", value1, value2, "imageUuid");
            return (Criteria) this;
        }

        public Criteria andImageUuidNotBetween(String value1, String value2) {
            addCriterion("image_uuid not between", value1, value2, "imageUuid");
            return (Criteria) this;
        }

        public Criteria andImageTitleIsNull() {
            addCriterion("image_title is null");
            return (Criteria) this;
        }

        public Criteria andImageTitleIsNotNull() {
            addCriterion("image_title is not null");
            return (Criteria) this;
        }

        public Criteria andImageTitleEqualTo(String value) {
            addCriterion("image_title =", value, "imageTitle");
            return (Criteria) this;
        }

        public Criteria andImageTitleNotEqualTo(String value) {
            addCriterion("image_title <>", value, "imageTitle");
            return (Criteria) this;
        }

        public Criteria andImageTitleGreaterThan(String value) {
            addCriterion("image_title >", value, "imageTitle");
            return (Criteria) this;
        }

        public Criteria andImageTitleGreaterThanOrEqualTo(String value) {
            addCriterion("image_title >=", value, "imageTitle");
            return (Criteria) this;
        }

        public Criteria andImageTitleLessThan(String value) {
            addCriterion("image_title <", value, "imageTitle");
            return (Criteria) this;
        }

        public Criteria andImageTitleLessThanOrEqualTo(String value) {
            addCriterion("image_title <=", value, "imageTitle");
            return (Criteria) this;
        }

        public Criteria andImageTitleLike(String value) {
            addCriterion("image_title like", value, "imageTitle");
            return (Criteria) this;
        }

        public Criteria andImageTitleNotLike(String value) {
            addCriterion("image_title not like", value, "imageTitle");
            return (Criteria) this;
        }

        public Criteria andImageTitleIn(List<String> values) {
            addCriterion("image_title in", values, "imageTitle");
            return (Criteria) this;
        }

        public Criteria andImageTitleNotIn(List<String> values) {
            addCriterion("image_title not in", values, "imageTitle");
            return (Criteria) this;
        }

        public Criteria andImageTitleBetween(String value1, String value2) {
            addCriterion("image_title between", value1, value2, "imageTitle");
            return (Criteria) this;
        }

        public Criteria andImageTitleNotBetween(String value1, String value2) {
            addCriterion("image_title not between", value1, value2, "imageTitle");
            return (Criteria) this;
        }

        public Criteria andImageDescriptionIsNull() {
            addCriterion("image_description is null");
            return (Criteria) this;
        }

        public Criteria andImageDescriptionIsNotNull() {
            addCriterion("image_description is not null");
            return (Criteria) this;
        }

        public Criteria andImageDescriptionEqualTo(String value) {
            addCriterion("image_description =", value, "imageDescription");
            return (Criteria) this;
        }

        public Criteria andImageDescriptionNotEqualTo(String value) {
            addCriterion("image_description <>", value, "imageDescription");
            return (Criteria) this;
        }

        public Criteria andImageDescriptionGreaterThan(String value) {
            addCriterion("image_description >", value, "imageDescription");
            return (Criteria) this;
        }

        public Criteria andImageDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("image_description >=", value, "imageDescription");
            return (Criteria) this;
        }

        public Criteria andImageDescriptionLessThan(String value) {
            addCriterion("image_description <", value, "imageDescription");
            return (Criteria) this;
        }

        public Criteria andImageDescriptionLessThanOrEqualTo(String value) {
            addCriterion("image_description <=", value, "imageDescription");
            return (Criteria) this;
        }

        public Criteria andImageDescriptionLike(String value) {
            addCriterion("image_description like", value, "imageDescription");
            return (Criteria) this;
        }

        public Criteria andImageDescriptionNotLike(String value) {
            addCriterion("image_description not like", value, "imageDescription");
            return (Criteria) this;
        }

        public Criteria andImageDescriptionIn(List<String> values) {
            addCriterion("image_description in", values, "imageDescription");
            return (Criteria) this;
        }

        public Criteria andImageDescriptionNotIn(List<String> values) {
            addCriterion("image_description not in", values, "imageDescription");
            return (Criteria) this;
        }

        public Criteria andImageDescriptionBetween(String value1, String value2) {
            addCriterion("image_description between", value1, value2, "imageDescription");
            return (Criteria) this;
        }

        public Criteria andImageDescriptionNotBetween(String value1, String value2) {
            addCriterion("image_description not between", value1, value2, "imageDescription");
            return (Criteria) this;
        }

        public Criteria andImageUrlIsNull() {
            addCriterion("image_url is null");
            return (Criteria) this;
        }

        public Criteria andImageUrlIsNotNull() {
            addCriterion("image_url is not null");
            return (Criteria) this;
        }

        public Criteria andImageUrlEqualTo(String value) {
            addCriterion("image_url =", value, "imageUrl");
            return (Criteria) this;
        }

        public Criteria andImageUrlNotEqualTo(String value) {
            addCriterion("image_url <>", value, "imageUrl");
            return (Criteria) this;
        }

        public Criteria andImageUrlGreaterThan(String value) {
            addCriterion("image_url >", value, "imageUrl");
            return (Criteria) this;
        }

        public Criteria andImageUrlGreaterThanOrEqualTo(String value) {
            addCriterion("image_url >=", value, "imageUrl");
            return (Criteria) this;
        }

        public Criteria andImageUrlLessThan(String value) {
            addCriterion("image_url <", value, "imageUrl");
            return (Criteria) this;
        }

        public Criteria andImageUrlLessThanOrEqualTo(String value) {
            addCriterion("image_url <=", value, "imageUrl");
            return (Criteria) this;
        }

        public Criteria andImageUrlLike(String value) {
            addCriterion("image_url like", value, "imageUrl");
            return (Criteria) this;
        }

        public Criteria andImageUrlNotLike(String value) {
            addCriterion("image_url not like", value, "imageUrl");
            return (Criteria) this;
        }

        public Criteria andImageUrlIn(List<String> values) {
            addCriterion("image_url in", values, "imageUrl");
            return (Criteria) this;
        }

        public Criteria andImageUrlNotIn(List<String> values) {
            addCriterion("image_url not in", values, "imageUrl");
            return (Criteria) this;
        }

        public Criteria andImageUrlBetween(String value1, String value2) {
            addCriterion("image_url between", value1, value2, "imageUrl");
            return (Criteria) this;
        }

        public Criteria andImageUrlNotBetween(String value1, String value2) {
            addCriterion("image_url not between", value1, value2, "imageUrl");
            return (Criteria) this;
        }

        public Criteria andImageCheckedIsNull() {
            addCriterion("image_checked is null");
            return (Criteria) this;
        }

        public Criteria andImageCheckedIsNotNull() {
            addCriterion("image_checked is not null");
            return (Criteria) this;
        }

        public Criteria andImageCheckedEqualTo(Integer value) {
            addCriterion("image_checked =", value, "imageChecked");
            return (Criteria) this;
        }

        public Criteria andImageCheckedNotEqualTo(Integer value) {
            addCriterion("image_checked <>", value, "imageChecked");
            return (Criteria) this;
        }

        public Criteria andImageCheckedGreaterThan(Integer value) {
            addCriterion("image_checked >", value, "imageChecked");
            return (Criteria) this;
        }

        public Criteria andImageCheckedGreaterThanOrEqualTo(Integer value) {
            addCriterion("image_checked >=", value, "imageChecked");
            return (Criteria) this;
        }

        public Criteria andImageCheckedLessThan(Integer value) {
            addCriterion("image_checked <", value, "imageChecked");
            return (Criteria) this;
        }

        public Criteria andImageCheckedLessThanOrEqualTo(Integer value) {
            addCriterion("image_checked <=", value, "imageChecked");
            return (Criteria) this;
        }

        public Criteria andImageCheckedIn(List<Integer> values) {
            addCriterion("image_checked in", values, "imageChecked");
            return (Criteria) this;
        }

        public Criteria andImageCheckedNotIn(List<Integer> values) {
            addCriterion("image_checked not in", values, "imageChecked");
            return (Criteria) this;
        }

        public Criteria andImageCheckedBetween(Integer value1, Integer value2) {
            addCriterion("image_checked between", value1, value2, "imageChecked");
            return (Criteria) this;
        }

        public Criteria andImageCheckedNotBetween(Integer value1, Integer value2) {
            addCriterion("image_checked not between", value1, value2, "imageChecked");
            return (Criteria) this;
        }

        public Criteria andImagePublisherIsNull() {
            addCriterion("image_publisher is null");
            return (Criteria) this;
        }

        public Criteria andImagePublisherIsNotNull() {
            addCriterion("image_publisher is not null");
            return (Criteria) this;
        }

        public Criteria andImagePublisherEqualTo(Integer value) {
            addCriterion("image_publisher =", value, "imagePublisher");
            return (Criteria) this;
        }

        public Criteria andImagePublisherNotEqualTo(Integer value) {
            addCriterion("image_publisher <>", value, "imagePublisher");
            return (Criteria) this;
        }

        public Criteria andImagePublisherGreaterThan(Integer value) {
            addCriterion("image_publisher >", value, "imagePublisher");
            return (Criteria) this;
        }

        public Criteria andImagePublisherGreaterThanOrEqualTo(Integer value) {
            addCriterion("image_publisher >=", value, "imagePublisher");
            return (Criteria) this;
        }

        public Criteria andImagePublisherLessThan(Integer value) {
            addCriterion("image_publisher <", value, "imagePublisher");
            return (Criteria) this;
        }

        public Criteria andImagePublisherLessThanOrEqualTo(Integer value) {
            addCriterion("image_publisher <=", value, "imagePublisher");
            return (Criteria) this;
        }

        public Criteria andImagePublisherIn(List<Integer> values) {
            addCriterion("image_publisher in", values, "imagePublisher");
            return (Criteria) this;
        }

        public Criteria andImagePublisherNotIn(List<Integer> values) {
            addCriterion("image_publisher not in", values, "imagePublisher");
            return (Criteria) this;
        }

        public Criteria andImagePublisherBetween(Integer value1, Integer value2) {
            addCriterion("image_publisher between", value1, value2, "imagePublisher");
            return (Criteria) this;
        }

        public Criteria andImagePublisherNotBetween(Integer value1, Integer value2) {
            addCriterion("image_publisher not between", value1, value2, "imagePublisher");
            return (Criteria) this;
        }

        public Criteria andImagePublishtimeIsNull() {
            addCriterion("image_publishtime is null");
            return (Criteria) this;
        }

        public Criteria andImagePublishtimeIsNotNull() {
            addCriterion("image_publishtime is not null");
            return (Criteria) this;
        }

        public Criteria andImagePublishtimeEqualTo(String value) {
            addCriterion("image_publishtime =", value, "imagePublishtime");
            return (Criteria) this;
        }

        public Criteria andImagePublishtimeNotEqualTo(String value) {
            addCriterion("image_publishtime <>", value, "imagePublishtime");
            return (Criteria) this;
        }

        public Criteria andImagePublishtimeGreaterThan(String value) {
            addCriterion("image_publishtime >", value, "imagePublishtime");
            return (Criteria) this;
        }

        public Criteria andImagePublishtimeGreaterThanOrEqualTo(String value) {
            addCriterion("image_publishtime >=", value, "imagePublishtime");
            return (Criteria) this;
        }

        public Criteria andImagePublishtimeLessThan(String value) {
            addCriterion("image_publishtime <", value, "imagePublishtime");
            return (Criteria) this;
        }

        public Criteria andImagePublishtimeLessThanOrEqualTo(String value) {
            addCriterion("image_publishtime <=", value, "imagePublishtime");
            return (Criteria) this;
        }

        public Criteria andImagePublishtimeLike(String value) {
            addCriterion("image_publishtime like", value, "imagePublishtime");
            return (Criteria) this;
        }

        public Criteria andImagePublishtimeNotLike(String value) {
            addCriterion("image_publishtime not like", value, "imagePublishtime");
            return (Criteria) this;
        }

        public Criteria andImagePublishtimeIn(List<String> values) {
            addCriterion("image_publishtime in", values, "imagePublishtime");
            return (Criteria) this;
        }

        public Criteria andImagePublishtimeNotIn(List<String> values) {
            addCriterion("image_publishtime not in", values, "imagePublishtime");
            return (Criteria) this;
        }

        public Criteria andImagePublishtimeBetween(String value1, String value2) {
            addCriterion("image_publishtime between", value1, value2, "imagePublishtime");
            return (Criteria) this;
        }

        public Criteria andImagePublishtimeNotBetween(String value1, String value2) {
            addCriterion("image_publishtime not between", value1, value2, "imagePublishtime");
            return (Criteria) this;
        }

        public Criteria andImageCheckerIsNull() {
            addCriterion("image_checker is null");
            return (Criteria) this;
        }

        public Criteria andImageCheckerIsNotNull() {
            addCriterion("image_checker is not null");
            return (Criteria) this;
        }

        public Criteria andImageCheckerEqualTo(Integer value) {
            addCriterion("image_checker =", value, "imageChecker");
            return (Criteria) this;
        }

        public Criteria andImageCheckerNotEqualTo(Integer value) {
            addCriterion("image_checker <>", value, "imageChecker");
            return (Criteria) this;
        }

        public Criteria andImageCheckerGreaterThan(Integer value) {
            addCriterion("image_checker >", value, "imageChecker");
            return (Criteria) this;
        }

        public Criteria andImageCheckerGreaterThanOrEqualTo(Integer value) {
            addCriterion("image_checker >=", value, "imageChecker");
            return (Criteria) this;
        }

        public Criteria andImageCheckerLessThan(Integer value) {
            addCriterion("image_checker <", value, "imageChecker");
            return (Criteria) this;
        }

        public Criteria andImageCheckerLessThanOrEqualTo(Integer value) {
            addCriterion("image_checker <=", value, "imageChecker");
            return (Criteria) this;
        }

        public Criteria andImageCheckerIn(List<Integer> values) {
            addCriterion("image_checker in", values, "imageChecker");
            return (Criteria) this;
        }

        public Criteria andImageCheckerNotIn(List<Integer> values) {
            addCriterion("image_checker not in", values, "imageChecker");
            return (Criteria) this;
        }

        public Criteria andImageCheckerBetween(Integer value1, Integer value2) {
            addCriterion("image_checker between", value1, value2, "imageChecker");
            return (Criteria) this;
        }

        public Criteria andImageCheckerNotBetween(Integer value1, Integer value2) {
            addCriterion("image_checker not between", value1, value2, "imageChecker");
            return (Criteria) this;
        }

        public Criteria andImageChecktimeIsNull() {
            addCriterion("image_checktime is null");
            return (Criteria) this;
        }

        public Criteria andImageChecktimeIsNotNull() {
            addCriterion("image_checktime is not null");
            return (Criteria) this;
        }

        public Criteria andImageChecktimeEqualTo(String value) {
            addCriterion("image_checktime =", value, "imageChecktime");
            return (Criteria) this;
        }

        public Criteria andImageChecktimeNotEqualTo(String value) {
            addCriterion("image_checktime <>", value, "imageChecktime");
            return (Criteria) this;
        }

        public Criteria andImageChecktimeGreaterThan(String value) {
            addCriterion("image_checktime >", value, "imageChecktime");
            return (Criteria) this;
        }

        public Criteria andImageChecktimeGreaterThanOrEqualTo(String value) {
            addCriterion("image_checktime >=", value, "imageChecktime");
            return (Criteria) this;
        }

        public Criteria andImageChecktimeLessThan(String value) {
            addCriterion("image_checktime <", value, "imageChecktime");
            return (Criteria) this;
        }

        public Criteria andImageChecktimeLessThanOrEqualTo(String value) {
            addCriterion("image_checktime <=", value, "imageChecktime");
            return (Criteria) this;
        }

        public Criteria andImageChecktimeLike(String value) {
            addCriterion("image_checktime like", value, "imageChecktime");
            return (Criteria) this;
        }

        public Criteria andImageChecktimeNotLike(String value) {
            addCriterion("image_checktime not like", value, "imageChecktime");
            return (Criteria) this;
        }

        public Criteria andImageChecktimeIn(List<String> values) {
            addCriterion("image_checktime in", values, "imageChecktime");
            return (Criteria) this;
        }

        public Criteria andImageChecktimeNotIn(List<String> values) {
            addCriterion("image_checktime not in", values, "imageChecktime");
            return (Criteria) this;
        }

        public Criteria andImageChecktimeBetween(String value1, String value2) {
            addCriterion("image_checktime between", value1, value2, "imageChecktime");
            return (Criteria) this;
        }

        public Criteria andImageChecktimeNotBetween(String value1, String value2) {
            addCriterion("image_checktime not between", value1, value2, "imageChecktime");
            return (Criteria) this;
        }

        public Criteria andImageSrcIsNull() {
            addCriterion("image_src is null");
            return (Criteria) this;
        }

        public Criteria andImageSrcIsNotNull() {
            addCriterion("image_src is not null");
            return (Criteria) this;
        }

        public Criteria andImageSrcEqualTo(String value) {
            addCriterion("image_src =", value, "imageSrc");
            return (Criteria) this;
        }

        public Criteria andImageSrcNotEqualTo(String value) {
            addCriterion("image_src <>", value, "imageSrc");
            return (Criteria) this;
        }

        public Criteria andImageSrcGreaterThan(String value) {
            addCriterion("image_src >", value, "imageSrc");
            return (Criteria) this;
        }

        public Criteria andImageSrcGreaterThanOrEqualTo(String value) {
            addCriterion("image_src >=", value, "imageSrc");
            return (Criteria) this;
        }

        public Criteria andImageSrcLessThan(String value) {
            addCriterion("image_src <", value, "imageSrc");
            return (Criteria) this;
        }

        public Criteria andImageSrcLessThanOrEqualTo(String value) {
            addCriterion("image_src <=", value, "imageSrc");
            return (Criteria) this;
        }

        public Criteria andImageSrcLike(String value) {
            addCriterion("image_src like", value, "imageSrc");
            return (Criteria) this;
        }

        public Criteria andImageSrcNotLike(String value) {
            addCriterion("image_src not like", value, "imageSrc");
            return (Criteria) this;
        }

        public Criteria andImageSrcIn(List<String> values) {
            addCriterion("image_src in", values, "imageSrc");
            return (Criteria) this;
        }

        public Criteria andImageSrcNotIn(List<String> values) {
            addCriterion("image_src not in", values, "imageSrc");
            return (Criteria) this;
        }

        public Criteria andImageSrcBetween(String value1, String value2) {
            addCriterion("image_src between", value1, value2, "imageSrc");
            return (Criteria) this;
        }

        public Criteria andImageSrcNotBetween(String value1, String value2) {
            addCriterion("image_src not between", value1, value2, "imageSrc");
            return (Criteria) this;
        }

        public Criteria andImageDeletedIsNull() {
            addCriterion("image_deleted is null");
            return (Criteria) this;
        }

        public Criteria andImageDeletedIsNotNull() {
            addCriterion("image_deleted is not null");
            return (Criteria) this;
        }

        public Criteria andImageDeletedEqualTo(Integer value) {
            addCriterion("image_deleted =", value, "imageDeleted");
            return (Criteria) this;
        }

        public Criteria andImageDeletedNotEqualTo(Integer value) {
            addCriterion("image_deleted <>", value, "imageDeleted");
            return (Criteria) this;
        }

        public Criteria andImageDeletedGreaterThan(Integer value) {
            addCriterion("image_deleted >", value, "imageDeleted");
            return (Criteria) this;
        }

        public Criteria andImageDeletedGreaterThanOrEqualTo(Integer value) {
            addCriterion("image_deleted >=", value, "imageDeleted");
            return (Criteria) this;
        }

        public Criteria andImageDeletedLessThan(Integer value) {
            addCriterion("image_deleted <", value, "imageDeleted");
            return (Criteria) this;
        }

        public Criteria andImageDeletedLessThanOrEqualTo(Integer value) {
            addCriterion("image_deleted <=", value, "imageDeleted");
            return (Criteria) this;
        }

        public Criteria andImageDeletedIn(List<Integer> values) {
            addCriterion("image_deleted in", values, "imageDeleted");
            return (Criteria) this;
        }

        public Criteria andImageDeletedNotIn(List<Integer> values) {
            addCriterion("image_deleted not in", values, "imageDeleted");
            return (Criteria) this;
        }

        public Criteria andImageDeletedBetween(Integer value1, Integer value2) {
            addCriterion("image_deleted between", value1, value2, "imageDeleted");
            return (Criteria) this;
        }

        public Criteria andImageDeletedNotBetween(Integer value1, Integer value2) {
            addCriterion("image_deleted not between", value1, value2, "imageDeleted");
            return (Criteria) this;
        }

        public Criteria andImageAsynidIsNull() {
            addCriterion("image_asynid is null");
            return (Criteria) this;
        }

        public Criteria andImageAsynidIsNotNull() {
            addCriterion("image_asynid is not null");
            return (Criteria) this;
        }

        public Criteria andImageAsynidEqualTo(Integer value) {
            addCriterion("image_asynid =", value, "imageAsynid");
            return (Criteria) this;
        }

        public Criteria andImageAsynidNotEqualTo(Integer value) {
            addCriterion("image_asynid <>", value, "imageAsynid");
            return (Criteria) this;
        }

        public Criteria andImageAsynidGreaterThan(Integer value) {
            addCriterion("image_asynid >", value, "imageAsynid");
            return (Criteria) this;
        }

        public Criteria andImageAsynidGreaterThanOrEqualTo(Integer value) {
            addCriterion("image_asynid >=", value, "imageAsynid");
            return (Criteria) this;
        }

        public Criteria andImageAsynidLessThan(Integer value) {
            addCriterion("image_asynid <", value, "imageAsynid");
            return (Criteria) this;
        }

        public Criteria andImageAsynidLessThanOrEqualTo(Integer value) {
            addCriterion("image_asynid <=", value, "imageAsynid");
            return (Criteria) this;
        }

        public Criteria andImageAsynidIn(List<Integer> values) {
            addCriterion("image_asynid in", values, "imageAsynid");
            return (Criteria) this;
        }

        public Criteria andImageAsynidNotIn(List<Integer> values) {
            addCriterion("image_asynid not in", values, "imageAsynid");
            return (Criteria) this;
        }

        public Criteria andImageAsynidBetween(Integer value1, Integer value2) {
            addCriterion("image_asynid between", value1, value2, "imageAsynid");
            return (Criteria) this;
        }

        public Criteria andImageAsynidNotBetween(Integer value1, Integer value2) {
            addCriterion("image_asynid not between", value1, value2, "imageAsynid");
            return (Criteria) this;
        }

        public Criteria andImageVisitCounterIsNull() {
            addCriterion("image_visit_counter is null");
            return (Criteria) this;
        }

        public Criteria andImageVisitCounterIsNotNull() {
            addCriterion("image_visit_counter is not null");
            return (Criteria) this;
        }

        public Criteria andImageVisitCounterEqualTo(Integer value) {
            addCriterion("image_visit_counter =", value, "imageVisitCounter");
            return (Criteria) this;
        }

        public Criteria andImageVisitCounterNotEqualTo(Integer value) {
            addCriterion("image_visit_counter <>", value, "imageVisitCounter");
            return (Criteria) this;
        }

        public Criteria andImageVisitCounterGreaterThan(Integer value) {
            addCriterion("image_visit_counter >", value, "imageVisitCounter");
            return (Criteria) this;
        }

        public Criteria andImageVisitCounterGreaterThanOrEqualTo(Integer value) {
            addCriterion("image_visit_counter >=", value, "imageVisitCounter");
            return (Criteria) this;
        }

        public Criteria andImageVisitCounterLessThan(Integer value) {
            addCriterion("image_visit_counter <", value, "imageVisitCounter");
            return (Criteria) this;
        }

        public Criteria andImageVisitCounterLessThanOrEqualTo(Integer value) {
            addCriterion("image_visit_counter <=", value, "imageVisitCounter");
            return (Criteria) this;
        }

        public Criteria andImageVisitCounterIn(List<Integer> values) {
            addCriterion("image_visit_counter in", values, "imageVisitCounter");
            return (Criteria) this;
        }

        public Criteria andImageVisitCounterNotIn(List<Integer> values) {
            addCriterion("image_visit_counter not in", values, "imageVisitCounter");
            return (Criteria) this;
        }

        public Criteria andImageVisitCounterBetween(Integer value1, Integer value2) {
            addCriterion("image_visit_counter between", value1, value2, "imageVisitCounter");
            return (Criteria) this;
        }

        public Criteria andImageVisitCounterNotBetween(Integer value1, Integer value2) {
            addCriterion("image_visit_counter not between", value1, value2, "imageVisitCounter");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * images 2019-01-13
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}