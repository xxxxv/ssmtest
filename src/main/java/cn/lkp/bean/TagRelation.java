package cn.lkp.bean;

import javax.persistence.Table;
import org.springframework.stereotype.Component;

/**
 * tag_relation
 * @author 杨孟新 2019-01-13
 */
@Table(name = "tag_relation")

@Component
public class TagRelation {
    private Integer tagRelationId;

    private Integer tagId;

    private Integer resType;

    private Integer resRessetId;

    public Integer getTagRelationId() {
        return tagRelationId;
    }

    public void setTagRelationId(Integer tagRelationId) {
        this.tagRelationId = tagRelationId;
    }

    public Integer getTagId() {
        return tagId;
    }

    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }

    public Integer getResType() {
        return resType;
    }

    public void setResType(Integer resType) {
        this.resType = resType;
    }

    public Integer getResRessetId() {
        return resRessetId;
    }

    public void setResRessetId(Integer resRessetId) {
        this.resRessetId = resRessetId;
    }
}