package cn.lkp.bean;

import java.util.ArrayList;
import java.util.List;

public class TagExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TagExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * tag 2019-01-13
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTagIdIsNull() {
            addCriterion("tag_id is null");
            return (Criteria) this;
        }

        public Criteria andTagIdIsNotNull() {
            addCriterion("tag_id is not null");
            return (Criteria) this;
        }

        public Criteria andTagIdEqualTo(Integer value) {
            addCriterion("tag_id =", value, "tagId");
            return (Criteria) this;
        }

        public Criteria andTagIdNotEqualTo(Integer value) {
            addCriterion("tag_id <>", value, "tagId");
            return (Criteria) this;
        }

        public Criteria andTagIdGreaterThan(Integer value) {
            addCriterion("tag_id >", value, "tagId");
            return (Criteria) this;
        }

        public Criteria andTagIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("tag_id >=", value, "tagId");
            return (Criteria) this;
        }

        public Criteria andTagIdLessThan(Integer value) {
            addCriterion("tag_id <", value, "tagId");
            return (Criteria) this;
        }

        public Criteria andTagIdLessThanOrEqualTo(Integer value) {
            addCriterion("tag_id <=", value, "tagId");
            return (Criteria) this;
        }

        public Criteria andTagIdIn(List<Integer> values) {
            addCriterion("tag_id in", values, "tagId");
            return (Criteria) this;
        }

        public Criteria andTagIdNotIn(List<Integer> values) {
            addCriterion("tag_id not in", values, "tagId");
            return (Criteria) this;
        }

        public Criteria andTagIdBetween(Integer value1, Integer value2) {
            addCriterion("tag_id between", value1, value2, "tagId");
            return (Criteria) this;
        }

        public Criteria andTagIdNotBetween(Integer value1, Integer value2) {
            addCriterion("tag_id not between", value1, value2, "tagId");
            return (Criteria) this;
        }

        public Criteria andTagTypeIsNull() {
            addCriterion("tag_type is null");
            return (Criteria) this;
        }

        public Criteria andTagTypeIsNotNull() {
            addCriterion("tag_type is not null");
            return (Criteria) this;
        }

        public Criteria andTagTypeEqualTo(Integer value) {
            addCriterion("tag_type =", value, "tagType");
            return (Criteria) this;
        }

        public Criteria andTagTypeNotEqualTo(Integer value) {
            addCriterion("tag_type <>", value, "tagType");
            return (Criteria) this;
        }

        public Criteria andTagTypeGreaterThan(Integer value) {
            addCriterion("tag_type >", value, "tagType");
            return (Criteria) this;
        }

        public Criteria andTagTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("tag_type >=", value, "tagType");
            return (Criteria) this;
        }

        public Criteria andTagTypeLessThan(Integer value) {
            addCriterion("tag_type <", value, "tagType");
            return (Criteria) this;
        }

        public Criteria andTagTypeLessThanOrEqualTo(Integer value) {
            addCriterion("tag_type <=", value, "tagType");
            return (Criteria) this;
        }

        public Criteria andTagTypeIn(List<Integer> values) {
            addCriterion("tag_type in", values, "tagType");
            return (Criteria) this;
        }

        public Criteria andTagTypeNotIn(List<Integer> values) {
            addCriterion("tag_type not in", values, "tagType");
            return (Criteria) this;
        }

        public Criteria andTagTypeBetween(Integer value1, Integer value2) {
            addCriterion("tag_type between", value1, value2, "tagType");
            return (Criteria) this;
        }

        public Criteria andTagTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("tag_type not between", value1, value2, "tagType");
            return (Criteria) this;
        }

        public Criteria andTagDescriptionIsNull() {
            addCriterion("tag_description is null");
            return (Criteria) this;
        }

        public Criteria andTagDescriptionIsNotNull() {
            addCriterion("tag_description is not null");
            return (Criteria) this;
        }

        public Criteria andTagDescriptionEqualTo(String value) {
            addCriterion("tag_description =", value, "tagDescription");
            return (Criteria) this;
        }

        public Criteria andTagDescriptionNotEqualTo(String value) {
            addCriterion("tag_description <>", value, "tagDescription");
            return (Criteria) this;
        }

        public Criteria andTagDescriptionGreaterThan(String value) {
            addCriterion("tag_description >", value, "tagDescription");
            return (Criteria) this;
        }

        public Criteria andTagDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("tag_description >=", value, "tagDescription");
            return (Criteria) this;
        }

        public Criteria andTagDescriptionLessThan(String value) {
            addCriterion("tag_description <", value, "tagDescription");
            return (Criteria) this;
        }

        public Criteria andTagDescriptionLessThanOrEqualTo(String value) {
            addCriterion("tag_description <=", value, "tagDescription");
            return (Criteria) this;
        }

        public Criteria andTagDescriptionLike(String value) {
            addCriterion("tag_description like", value, "tagDescription");
            return (Criteria) this;
        }

        public Criteria andTagDescriptionNotLike(String value) {
            addCriterion("tag_description not like", value, "tagDescription");
            return (Criteria) this;
        }

        public Criteria andTagDescriptionIn(List<String> values) {
            addCriterion("tag_description in", values, "tagDescription");
            return (Criteria) this;
        }

        public Criteria andTagDescriptionNotIn(List<String> values) {
            addCriterion("tag_description not in", values, "tagDescription");
            return (Criteria) this;
        }

        public Criteria andTagDescriptionBetween(String value1, String value2) {
            addCriterion("tag_description between", value1, value2, "tagDescription");
            return (Criteria) this;
        }

        public Criteria andTagDescriptionNotBetween(String value1, String value2) {
            addCriterion("tag_description not between", value1, value2, "tagDescription");
            return (Criteria) this;
        }

        public Criteria andTagUuidIsNull() {
            addCriterion("tag_uuid is null");
            return (Criteria) this;
        }

        public Criteria andTagUuidIsNotNull() {
            addCriterion("tag_uuid is not null");
            return (Criteria) this;
        }

        public Criteria andTagUuidEqualTo(String value) {
            addCriterion("tag_uuid =", value, "tagUuid");
            return (Criteria) this;
        }

        public Criteria andTagUuidNotEqualTo(String value) {
            addCriterion("tag_uuid <>", value, "tagUuid");
            return (Criteria) this;
        }

        public Criteria andTagUuidGreaterThan(String value) {
            addCriterion("tag_uuid >", value, "tagUuid");
            return (Criteria) this;
        }

        public Criteria andTagUuidGreaterThanOrEqualTo(String value) {
            addCriterion("tag_uuid >=", value, "tagUuid");
            return (Criteria) this;
        }

        public Criteria andTagUuidLessThan(String value) {
            addCriterion("tag_uuid <", value, "tagUuid");
            return (Criteria) this;
        }

        public Criteria andTagUuidLessThanOrEqualTo(String value) {
            addCriterion("tag_uuid <=", value, "tagUuid");
            return (Criteria) this;
        }

        public Criteria andTagUuidLike(String value) {
            addCriterion("tag_uuid like", value, "tagUuid");
            return (Criteria) this;
        }

        public Criteria andTagUuidNotLike(String value) {
            addCriterion("tag_uuid not like", value, "tagUuid");
            return (Criteria) this;
        }

        public Criteria andTagUuidIn(List<String> values) {
            addCriterion("tag_uuid in", values, "tagUuid");
            return (Criteria) this;
        }

        public Criteria andTagUuidNotIn(List<String> values) {
            addCriterion("tag_uuid not in", values, "tagUuid");
            return (Criteria) this;
        }

        public Criteria andTagUuidBetween(String value1, String value2) {
            addCriterion("tag_uuid between", value1, value2, "tagUuid");
            return (Criteria) this;
        }

        public Criteria andTagUuidNotBetween(String value1, String value2) {
            addCriterion("tag_uuid not between", value1, value2, "tagUuid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * tag 2019-01-13
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}