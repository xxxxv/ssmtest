package cn.lkp.bean;

import javax.persistence.Table;
import org.springframework.stereotype.Component;

/**
 * hypertext
 * @author 杨孟新 2019-01-13
 */
@Table(name = "hypertext")

@Component
public class Hypertext {
    private Integer textId;

    private String textUuid;

    private String textTitle;

    private String textViceTitle;

    private String textTitleColor;

    private String textImageUrl;

    private String textContent;

    private String textTitleUrl;

    private Integer textChecked;

    private String textChecker;

    private String textPublishtime;

    private Integer textPublisher;

    private String textChecktime;

    private String textSourceUrl;

    private Integer textFlag;

    private Integer textDeleted;

    private Integer textAsynid;

    private Integer textVisitCounter;

    public Integer getTextId() {
        return textId;
    }

    public void setTextId(Integer textId) {
        this.textId = textId;
    }

    public String getTextUuid() {
        return textUuid;
    }

    public void setTextUuid(String textUuid) {
        this.textUuid = textUuid == null ? null : textUuid.trim();
    }

    public String getTextTitle() {
        return textTitle;
    }

    public void setTextTitle(String textTitle) {
        this.textTitle = textTitle == null ? null : textTitle.trim();
    }

    public String getTextViceTitle() {
        return textViceTitle;
    }

    public void setTextViceTitle(String textViceTitle) {
        this.textViceTitle = textViceTitle == null ? null : textViceTitle.trim();
    }

    public String getTextTitleColor() {
        return textTitleColor;
    }

    public void setTextTitleColor(String textTitleColor) {
        this.textTitleColor = textTitleColor == null ? null : textTitleColor.trim();
    }

    public String getTextImageUrl() {
        return textImageUrl;
    }

    public void setTextImageUrl(String textImageUrl) {
        this.textImageUrl = textImageUrl == null ? null : textImageUrl.trim();
    }

    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String textContent) {
        this.textContent = textContent == null ? null : textContent.trim();
    }

    public String getTextTitleUrl() {
        return textTitleUrl;
    }

    public void setTextTitleUrl(String textTitleUrl) {
        this.textTitleUrl = textTitleUrl == null ? null : textTitleUrl.trim();
    }

    public Integer getTextChecked() {
        return textChecked;
    }

    public void setTextChecked(Integer textChecked) {
        this.textChecked = textChecked;
    }

    public String getTextChecker() {
        return textChecker;
    }

    public void setTextChecker(String textChecker) {
        this.textChecker = textChecker == null ? null : textChecker.trim();
    }

    public String getTextPublishtime() {
        return textPublishtime;
    }

    public void setTextPublishtime(String textPublishtime) {
        this.textPublishtime = textPublishtime == null ? null : textPublishtime.trim();
    }

    public Integer getTextPublisher() {
        return textPublisher;
    }

    public void setTextPublisher(Integer textPublisher) {
        this.textPublisher = textPublisher;
    }

    public String getTextChecktime() {
        return textChecktime;
    }

    public void setTextChecktime(String textChecktime) {
        this.textChecktime = textChecktime == null ? null : textChecktime.trim();
    }

    public String getTextSourceUrl() {
        return textSourceUrl;
    }

    public void setTextSourceUrl(String textSourceUrl) {
        this.textSourceUrl = textSourceUrl == null ? null : textSourceUrl.trim();
    }

    public Integer getTextFlag() {
        return textFlag;
    }

    public void setTextFlag(Integer textFlag) {
        this.textFlag = textFlag;
    }

    public Integer getTextDeleted() {
        return textDeleted;
    }

    public void setTextDeleted(Integer textDeleted) {
        this.textDeleted = textDeleted;
    }

    public Integer getTextAsynid() {
        return textAsynid;
    }

    public void setTextAsynid(Integer textAsynid) {
        this.textAsynid = textAsynid;
    }

    public Integer getTextVisitCounter() {
        return textVisitCounter;
    }

    public void setTextVisitCounter(Integer textVisitCounter) {
        this.textVisitCounter = textVisitCounter;
    }
}