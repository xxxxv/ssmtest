package cn.lkp.bean;

import javax.persistence.Table;
import org.springframework.stereotype.Component;

/**
 * tag
 * @author 杨孟新 2019-01-13
 */
@Table(name = "tag")

@Component
public class Tag {
    private Integer tagId;

    private Integer tagType;

    private String tagDescription;

    private String tagUuid;

    public Integer getTagId() {
        return tagId;
    }

    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }

    public Integer getTagType() {
        return tagType;
    }

    public void setTagType(Integer tagType) {
        this.tagType = tagType;
    }

    public String getTagDescription() {
        return tagDescription;
    }

    public void setTagDescription(String tagDescription) {
        this.tagDescription = tagDescription == null ? null : tagDescription.trim();
    }

    public String getTagUuid() {
        return tagUuid;
    }

    public void setTagUuid(String tagUuid) {
        this.tagUuid = tagUuid == null ? null : tagUuid.trim();
    }
}