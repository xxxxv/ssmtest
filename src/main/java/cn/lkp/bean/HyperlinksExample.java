package cn.lkp.bean;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class HyperlinksExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public HyperlinksExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * hyperlinks 2019-01-13
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andLinkIdIsNull() {
            addCriterion("link_id is null");
            return (Criteria) this;
        }

        public Criteria andLinkIdIsNotNull() {
            addCriterion("link_id is not null");
            return (Criteria) this;
        }

        public Criteria andLinkIdEqualTo(Integer value) {
            addCriterion("link_id =", value, "linkId");
            return (Criteria) this;
        }

        public Criteria andLinkIdNotEqualTo(Integer value) {
            addCriterion("link_id <>", value, "linkId");
            return (Criteria) this;
        }

        public Criteria andLinkIdGreaterThan(Integer value) {
            addCriterion("link_id >", value, "linkId");
            return (Criteria) this;
        }

        public Criteria andLinkIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("link_id >=", value, "linkId");
            return (Criteria) this;
        }

        public Criteria andLinkIdLessThan(Integer value) {
            addCriterion("link_id <", value, "linkId");
            return (Criteria) this;
        }

        public Criteria andLinkIdLessThanOrEqualTo(Integer value) {
            addCriterion("link_id <=", value, "linkId");
            return (Criteria) this;
        }

        public Criteria andLinkIdIn(List<Integer> values) {
            addCriterion("link_id in", values, "linkId");
            return (Criteria) this;
        }

        public Criteria andLinkIdNotIn(List<Integer> values) {
            addCriterion("link_id not in", values, "linkId");
            return (Criteria) this;
        }

        public Criteria andLinkIdBetween(Integer value1, Integer value2) {
            addCriterion("link_id between", value1, value2, "linkId");
            return (Criteria) this;
        }

        public Criteria andLinkIdNotBetween(Integer value1, Integer value2) {
            addCriterion("link_id not between", value1, value2, "linkId");
            return (Criteria) this;
        }

        public Criteria andLinkUuidIsNull() {
            addCriterion("link_uuid is null");
            return (Criteria) this;
        }

        public Criteria andLinkUuidIsNotNull() {
            addCriterion("link_uuid is not null");
            return (Criteria) this;
        }

        public Criteria andLinkUuidEqualTo(String value) {
            addCriterion("link_uuid =", value, "linkUuid");
            return (Criteria) this;
        }

        public Criteria andLinkUuidNotEqualTo(String value) {
            addCriterion("link_uuid <>", value, "linkUuid");
            return (Criteria) this;
        }

        public Criteria andLinkUuidGreaterThan(String value) {
            addCriterion("link_uuid >", value, "linkUuid");
            return (Criteria) this;
        }

        public Criteria andLinkUuidGreaterThanOrEqualTo(String value) {
            addCriterion("link_uuid >=", value, "linkUuid");
            return (Criteria) this;
        }

        public Criteria andLinkUuidLessThan(String value) {
            addCriterion("link_uuid <", value, "linkUuid");
            return (Criteria) this;
        }

        public Criteria andLinkUuidLessThanOrEqualTo(String value) {
            addCriterion("link_uuid <=", value, "linkUuid");
            return (Criteria) this;
        }

        public Criteria andLinkUuidLike(String value) {
            addCriterion("link_uuid like", value, "linkUuid");
            return (Criteria) this;
        }

        public Criteria andLinkUuidNotLike(String value) {
            addCriterion("link_uuid not like", value, "linkUuid");
            return (Criteria) this;
        }

        public Criteria andLinkUuidIn(List<String> values) {
            addCriterion("link_uuid in", values, "linkUuid");
            return (Criteria) this;
        }

        public Criteria andLinkUuidNotIn(List<String> values) {
            addCriterion("link_uuid not in", values, "linkUuid");
            return (Criteria) this;
        }

        public Criteria andLinkUuidBetween(String value1, String value2) {
            addCriterion("link_uuid between", value1, value2, "linkUuid");
            return (Criteria) this;
        }

        public Criteria andLinkUuidNotBetween(String value1, String value2) {
            addCriterion("link_uuid not between", value1, value2, "linkUuid");
            return (Criteria) this;
        }

        public Criteria andLinkUrlIsNull() {
            addCriterion("link_url is null");
            return (Criteria) this;
        }

        public Criteria andLinkUrlIsNotNull() {
            addCriterion("link_url is not null");
            return (Criteria) this;
        }

        public Criteria andLinkUrlEqualTo(String value) {
            addCriterion("link_url =", value, "linkUrl");
            return (Criteria) this;
        }

        public Criteria andLinkUrlNotEqualTo(String value) {
            addCriterion("link_url <>", value, "linkUrl");
            return (Criteria) this;
        }

        public Criteria andLinkUrlGreaterThan(String value) {
            addCriterion("link_url >", value, "linkUrl");
            return (Criteria) this;
        }

        public Criteria andLinkUrlGreaterThanOrEqualTo(String value) {
            addCriterion("link_url >=", value, "linkUrl");
            return (Criteria) this;
        }

        public Criteria andLinkUrlLessThan(String value) {
            addCriterion("link_url <", value, "linkUrl");
            return (Criteria) this;
        }

        public Criteria andLinkUrlLessThanOrEqualTo(String value) {
            addCriterion("link_url <=", value, "linkUrl");
            return (Criteria) this;
        }

        public Criteria andLinkUrlLike(String value) {
            addCriterion("link_url like", value, "linkUrl");
            return (Criteria) this;
        }

        public Criteria andLinkUrlNotLike(String value) {
            addCriterion("link_url not like", value, "linkUrl");
            return (Criteria) this;
        }

        public Criteria andLinkUrlIn(List<String> values) {
            addCriterion("link_url in", values, "linkUrl");
            return (Criteria) this;
        }

        public Criteria andLinkUrlNotIn(List<String> values) {
            addCriterion("link_url not in", values, "linkUrl");
            return (Criteria) this;
        }

        public Criteria andLinkUrlBetween(String value1, String value2) {
            addCriterion("link_url between", value1, value2, "linkUrl");
            return (Criteria) this;
        }

        public Criteria andLinkUrlNotBetween(String value1, String value2) {
            addCriterion("link_url not between", value1, value2, "linkUrl");
            return (Criteria) this;
        }

        public Criteria andLinkTitleIsNull() {
            addCriterion("link_title is null");
            return (Criteria) this;
        }

        public Criteria andLinkTitleIsNotNull() {
            addCriterion("link_title is not null");
            return (Criteria) this;
        }

        public Criteria andLinkTitleEqualTo(String value) {
            addCriterion("link_title =", value, "linkTitle");
            return (Criteria) this;
        }

        public Criteria andLinkTitleNotEqualTo(String value) {
            addCriterion("link_title <>", value, "linkTitle");
            return (Criteria) this;
        }

        public Criteria andLinkTitleGreaterThan(String value) {
            addCriterion("link_title >", value, "linkTitle");
            return (Criteria) this;
        }

        public Criteria andLinkTitleGreaterThanOrEqualTo(String value) {
            addCriterion("link_title >=", value, "linkTitle");
            return (Criteria) this;
        }

        public Criteria andLinkTitleLessThan(String value) {
            addCriterion("link_title <", value, "linkTitle");
            return (Criteria) this;
        }

        public Criteria andLinkTitleLessThanOrEqualTo(String value) {
            addCriterion("link_title <=", value, "linkTitle");
            return (Criteria) this;
        }

        public Criteria andLinkTitleLike(String value) {
            addCriterion("link_title like", value, "linkTitle");
            return (Criteria) this;
        }

        public Criteria andLinkTitleNotLike(String value) {
            addCriterion("link_title not like", value, "linkTitle");
            return (Criteria) this;
        }

        public Criteria andLinkTitleIn(List<String> values) {
            addCriterion("link_title in", values, "linkTitle");
            return (Criteria) this;
        }

        public Criteria andLinkTitleNotIn(List<String> values) {
            addCriterion("link_title not in", values, "linkTitle");
            return (Criteria) this;
        }

        public Criteria andLinkTitleBetween(String value1, String value2) {
            addCriterion("link_title between", value1, value2, "linkTitle");
            return (Criteria) this;
        }

        public Criteria andLinkTitleNotBetween(String value1, String value2) {
            addCriterion("link_title not between", value1, value2, "linkTitle");
            return (Criteria) this;
        }

        public Criteria andLinkDescriptionIsNull() {
            addCriterion("link_description is null");
            return (Criteria) this;
        }

        public Criteria andLinkDescriptionIsNotNull() {
            addCriterion("link_description is not null");
            return (Criteria) this;
        }

        public Criteria andLinkDescriptionEqualTo(String value) {
            addCriterion("link_description =", value, "linkDescription");
            return (Criteria) this;
        }

        public Criteria andLinkDescriptionNotEqualTo(String value) {
            addCriterion("link_description <>", value, "linkDescription");
            return (Criteria) this;
        }

        public Criteria andLinkDescriptionGreaterThan(String value) {
            addCriterion("link_description >", value, "linkDescription");
            return (Criteria) this;
        }

        public Criteria andLinkDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("link_description >=", value, "linkDescription");
            return (Criteria) this;
        }

        public Criteria andLinkDescriptionLessThan(String value) {
            addCriterion("link_description <", value, "linkDescription");
            return (Criteria) this;
        }

        public Criteria andLinkDescriptionLessThanOrEqualTo(String value) {
            addCriterion("link_description <=", value, "linkDescription");
            return (Criteria) this;
        }

        public Criteria andLinkDescriptionLike(String value) {
            addCriterion("link_description like", value, "linkDescription");
            return (Criteria) this;
        }

        public Criteria andLinkDescriptionNotLike(String value) {
            addCriterion("link_description not like", value, "linkDescription");
            return (Criteria) this;
        }

        public Criteria andLinkDescriptionIn(List<String> values) {
            addCriterion("link_description in", values, "linkDescription");
            return (Criteria) this;
        }

        public Criteria andLinkDescriptionNotIn(List<String> values) {
            addCriterion("link_description not in", values, "linkDescription");
            return (Criteria) this;
        }

        public Criteria andLinkDescriptionBetween(String value1, String value2) {
            addCriterion("link_description between", value1, value2, "linkDescription");
            return (Criteria) this;
        }

        public Criteria andLinkDescriptionNotBetween(String value1, String value2) {
            addCriterion("link_description not between", value1, value2, "linkDescription");
            return (Criteria) this;
        }

        public Criteria andLinkCheckedIsNull() {
            addCriterion("link_checked is null");
            return (Criteria) this;
        }

        public Criteria andLinkCheckedIsNotNull() {
            addCriterion("link_checked is not null");
            return (Criteria) this;
        }

        public Criteria andLinkCheckedEqualTo(Integer value) {
            addCriterion("link_checked =", value, "linkChecked");
            return (Criteria) this;
        }

        public Criteria andLinkCheckedNotEqualTo(Integer value) {
            addCriterion("link_checked <>", value, "linkChecked");
            return (Criteria) this;
        }

        public Criteria andLinkCheckedGreaterThan(Integer value) {
            addCriterion("link_checked >", value, "linkChecked");
            return (Criteria) this;
        }

        public Criteria andLinkCheckedGreaterThanOrEqualTo(Integer value) {
            addCriterion("link_checked >=", value, "linkChecked");
            return (Criteria) this;
        }

        public Criteria andLinkCheckedLessThan(Integer value) {
            addCriterion("link_checked <", value, "linkChecked");
            return (Criteria) this;
        }

        public Criteria andLinkCheckedLessThanOrEqualTo(Integer value) {
            addCriterion("link_checked <=", value, "linkChecked");
            return (Criteria) this;
        }

        public Criteria andLinkCheckedIn(List<Integer> values) {
            addCriterion("link_checked in", values, "linkChecked");
            return (Criteria) this;
        }

        public Criteria andLinkCheckedNotIn(List<Integer> values) {
            addCriterion("link_checked not in", values, "linkChecked");
            return (Criteria) this;
        }

        public Criteria andLinkCheckedBetween(Integer value1, Integer value2) {
            addCriterion("link_checked between", value1, value2, "linkChecked");
            return (Criteria) this;
        }

        public Criteria andLinkCheckedNotBetween(Integer value1, Integer value2) {
            addCriterion("link_checked not between", value1, value2, "linkChecked");
            return (Criteria) this;
        }

        public Criteria andLinkCheckerIsNull() {
            addCriterion("link_checker is null");
            return (Criteria) this;
        }

        public Criteria andLinkCheckerIsNotNull() {
            addCriterion("link_checker is not null");
            return (Criteria) this;
        }

        public Criteria andLinkCheckerEqualTo(Integer value) {
            addCriterion("link_checker =", value, "linkChecker");
            return (Criteria) this;
        }

        public Criteria andLinkCheckerNotEqualTo(Integer value) {
            addCriterion("link_checker <>", value, "linkChecker");
            return (Criteria) this;
        }

        public Criteria andLinkCheckerGreaterThan(Integer value) {
            addCriterion("link_checker >", value, "linkChecker");
            return (Criteria) this;
        }

        public Criteria andLinkCheckerGreaterThanOrEqualTo(Integer value) {
            addCriterion("link_checker >=", value, "linkChecker");
            return (Criteria) this;
        }

        public Criteria andLinkCheckerLessThan(Integer value) {
            addCriterion("link_checker <", value, "linkChecker");
            return (Criteria) this;
        }

        public Criteria andLinkCheckerLessThanOrEqualTo(Integer value) {
            addCriterion("link_checker <=", value, "linkChecker");
            return (Criteria) this;
        }

        public Criteria andLinkCheckerIn(List<Integer> values) {
            addCriterion("link_checker in", values, "linkChecker");
            return (Criteria) this;
        }

        public Criteria andLinkCheckerNotIn(List<Integer> values) {
            addCriterion("link_checker not in", values, "linkChecker");
            return (Criteria) this;
        }

        public Criteria andLinkCheckerBetween(Integer value1, Integer value2) {
            addCriterion("link_checker between", value1, value2, "linkChecker");
            return (Criteria) this;
        }

        public Criteria andLinkCheckerNotBetween(Integer value1, Integer value2) {
            addCriterion("link_checker not between", value1, value2, "linkChecker");
            return (Criteria) this;
        }

        public Criteria andLinkChecktimeIsNull() {
            addCriterion("link_checktime is null");
            return (Criteria) this;
        }

        public Criteria andLinkChecktimeIsNotNull() {
            addCriterion("link_checktime is not null");
            return (Criteria) this;
        }

        public Criteria andLinkChecktimeEqualTo(String value) {
            addCriterion("link_checktime =", value, "linkChecktime");
            return (Criteria) this;
        }

        public Criteria andLinkChecktimeNotEqualTo(String value) {
            addCriterion("link_checktime <>", value, "linkChecktime");
            return (Criteria) this;
        }

        public Criteria andLinkChecktimeGreaterThan(String value) {
            addCriterion("link_checktime >", value, "linkChecktime");
            return (Criteria) this;
        }

        public Criteria andLinkChecktimeGreaterThanOrEqualTo(String value) {
            addCriterion("link_checktime >=", value, "linkChecktime");
            return (Criteria) this;
        }

        public Criteria andLinkChecktimeLessThan(String value) {
            addCriterion("link_checktime <", value, "linkChecktime");
            return (Criteria) this;
        }

        public Criteria andLinkChecktimeLessThanOrEqualTo(String value) {
            addCriterion("link_checktime <=", value, "linkChecktime");
            return (Criteria) this;
        }

        public Criteria andLinkChecktimeLike(String value) {
            addCriterion("link_checktime like", value, "linkChecktime");
            return (Criteria) this;
        }

        public Criteria andLinkChecktimeNotLike(String value) {
            addCriterion("link_checktime not like", value, "linkChecktime");
            return (Criteria) this;
        }

        public Criteria andLinkChecktimeIn(List<String> values) {
            addCriterion("link_checktime in", values, "linkChecktime");
            return (Criteria) this;
        }

        public Criteria andLinkChecktimeNotIn(List<String> values) {
            addCriterion("link_checktime not in", values, "linkChecktime");
            return (Criteria) this;
        }

        public Criteria andLinkChecktimeBetween(String value1, String value2) {
            addCriterion("link_checktime between", value1, value2, "linkChecktime");
            return (Criteria) this;
        }

        public Criteria andLinkChecktimeNotBetween(String value1, String value2) {
            addCriterion("link_checktime not between", value1, value2, "linkChecktime");
            return (Criteria) this;
        }

        public Criteria andLinkImageIsNull() {
            addCriterion("link_image is null");
            return (Criteria) this;
        }

        public Criteria andLinkImageIsNotNull() {
            addCriterion("link_image is not null");
            return (Criteria) this;
        }

        public Criteria andLinkImageEqualTo(String value) {
            addCriterion("link_image =", value, "linkImage");
            return (Criteria) this;
        }

        public Criteria andLinkImageNotEqualTo(String value) {
            addCriterion("link_image <>", value, "linkImage");
            return (Criteria) this;
        }

        public Criteria andLinkImageGreaterThan(String value) {
            addCriterion("link_image >", value, "linkImage");
            return (Criteria) this;
        }

        public Criteria andLinkImageGreaterThanOrEqualTo(String value) {
            addCriterion("link_image >=", value, "linkImage");
            return (Criteria) this;
        }

        public Criteria andLinkImageLessThan(String value) {
            addCriterion("link_image <", value, "linkImage");
            return (Criteria) this;
        }

        public Criteria andLinkImageLessThanOrEqualTo(String value) {
            addCriterion("link_image <=", value, "linkImage");
            return (Criteria) this;
        }

        public Criteria andLinkImageLike(String value) {
            addCriterion("link_image like", value, "linkImage");
            return (Criteria) this;
        }

        public Criteria andLinkImageNotLike(String value) {
            addCriterion("link_image not like", value, "linkImage");
            return (Criteria) this;
        }

        public Criteria andLinkImageIn(List<String> values) {
            addCriterion("link_image in", values, "linkImage");
            return (Criteria) this;
        }

        public Criteria andLinkImageNotIn(List<String> values) {
            addCriterion("link_image not in", values, "linkImage");
            return (Criteria) this;
        }

        public Criteria andLinkImageBetween(String value1, String value2) {
            addCriterion("link_image between", value1, value2, "linkImage");
            return (Criteria) this;
        }

        public Criteria andLinkImageNotBetween(String value1, String value2) {
            addCriterion("link_image not between", value1, value2, "linkImage");
            return (Criteria) this;
        }

        public Criteria andLinkDeletedIsNull() {
            addCriterion("link_deleted is null");
            return (Criteria) this;
        }

        public Criteria andLinkDeletedIsNotNull() {
            addCriterion("link_deleted is not null");
            return (Criteria) this;
        }

        public Criteria andLinkDeletedEqualTo(Integer value) {
            addCriterion("link_deleted =", value, "linkDeleted");
            return (Criteria) this;
        }

        public Criteria andLinkDeletedNotEqualTo(Integer value) {
            addCriterion("link_deleted <>", value, "linkDeleted");
            return (Criteria) this;
        }

        public Criteria andLinkDeletedGreaterThan(Integer value) {
            addCriterion("link_deleted >", value, "linkDeleted");
            return (Criteria) this;
        }

        public Criteria andLinkDeletedGreaterThanOrEqualTo(Integer value) {
            addCriterion("link_deleted >=", value, "linkDeleted");
            return (Criteria) this;
        }

        public Criteria andLinkDeletedLessThan(Integer value) {
            addCriterion("link_deleted <", value, "linkDeleted");
            return (Criteria) this;
        }

        public Criteria andLinkDeletedLessThanOrEqualTo(Integer value) {
            addCriterion("link_deleted <=", value, "linkDeleted");
            return (Criteria) this;
        }

        public Criteria andLinkDeletedIn(List<Integer> values) {
            addCriterion("link_deleted in", values, "linkDeleted");
            return (Criteria) this;
        }

        public Criteria andLinkDeletedNotIn(List<Integer> values) {
            addCriterion("link_deleted not in", values, "linkDeleted");
            return (Criteria) this;
        }

        public Criteria andLinkDeletedBetween(Integer value1, Integer value2) {
            addCriterion("link_deleted between", value1, value2, "linkDeleted");
            return (Criteria) this;
        }

        public Criteria andLinkDeletedNotBetween(Integer value1, Integer value2) {
            addCriterion("link_deleted not between", value1, value2, "linkDeleted");
            return (Criteria) this;
        }

        public Criteria andLinkAsynidIsNull() {
            addCriterion("link_asynid is null");
            return (Criteria) this;
        }

        public Criteria andLinkAsynidIsNotNull() {
            addCriterion("link_asynid is not null");
            return (Criteria) this;
        }

        public Criteria andLinkAsynidEqualTo(Integer value) {
            addCriterion("link_asynid =", value, "linkAsynid");
            return (Criteria) this;
        }

        public Criteria andLinkAsynidNotEqualTo(Integer value) {
            addCriterion("link_asynid <>", value, "linkAsynid");
            return (Criteria) this;
        }

        public Criteria andLinkAsynidGreaterThan(Integer value) {
            addCriterion("link_asynid >", value, "linkAsynid");
            return (Criteria) this;
        }

        public Criteria andLinkAsynidGreaterThanOrEqualTo(Integer value) {
            addCriterion("link_asynid >=", value, "linkAsynid");
            return (Criteria) this;
        }

        public Criteria andLinkAsynidLessThan(Integer value) {
            addCriterion("link_asynid <", value, "linkAsynid");
            return (Criteria) this;
        }

        public Criteria andLinkAsynidLessThanOrEqualTo(Integer value) {
            addCriterion("link_asynid <=", value, "linkAsynid");
            return (Criteria) this;
        }

        public Criteria andLinkAsynidIn(List<Integer> values) {
            addCriterion("link_asynid in", values, "linkAsynid");
            return (Criteria) this;
        }

        public Criteria andLinkAsynidNotIn(List<Integer> values) {
            addCriterion("link_asynid not in", values, "linkAsynid");
            return (Criteria) this;
        }

        public Criteria andLinkAsynidBetween(Integer value1, Integer value2) {
            addCriterion("link_asynid between", value1, value2, "linkAsynid");
            return (Criteria) this;
        }

        public Criteria andLinkAsynidNotBetween(Integer value1, Integer value2) {
            addCriterion("link_asynid not between", value1, value2, "linkAsynid");
            return (Criteria) this;
        }

        public Criteria andLinkVisitCounterIsNull() {
            addCriterion("link_visit_counter is null");
            return (Criteria) this;
        }

        public Criteria andLinkVisitCounterIsNotNull() {
            addCriterion("link_visit_counter is not null");
            return (Criteria) this;
        }

        public Criteria andLinkVisitCounterEqualTo(Integer value) {
            addCriterion("link_visit_counter =", value, "linkVisitCounter");
            return (Criteria) this;
        }

        public Criteria andLinkVisitCounterNotEqualTo(Integer value) {
            addCriterion("link_visit_counter <>", value, "linkVisitCounter");
            return (Criteria) this;
        }

        public Criteria andLinkVisitCounterGreaterThan(Integer value) {
            addCriterion("link_visit_counter >", value, "linkVisitCounter");
            return (Criteria) this;
        }

        public Criteria andLinkVisitCounterGreaterThanOrEqualTo(Integer value) {
            addCriterion("link_visit_counter >=", value, "linkVisitCounter");
            return (Criteria) this;
        }

        public Criteria andLinkVisitCounterLessThan(Integer value) {
            addCriterion("link_visit_counter <", value, "linkVisitCounter");
            return (Criteria) this;
        }

        public Criteria andLinkVisitCounterLessThanOrEqualTo(Integer value) {
            addCriterion("link_visit_counter <=", value, "linkVisitCounter");
            return (Criteria) this;
        }

        public Criteria andLinkVisitCounterIn(List<Integer> values) {
            addCriterion("link_visit_counter in", values, "linkVisitCounter");
            return (Criteria) this;
        }

        public Criteria andLinkVisitCounterNotIn(List<Integer> values) {
            addCriterion("link_visit_counter not in", values, "linkVisitCounter");
            return (Criteria) this;
        }

        public Criteria andLinkVisitCounterBetween(Integer value1, Integer value2) {
            addCriterion("link_visit_counter between", value1, value2, "linkVisitCounter");
            return (Criteria) this;
        }

        public Criteria andLinkVisitCounterNotBetween(Integer value1, Integer value2) {
            addCriterion("link_visit_counter not between", value1, value2, "linkVisitCounter");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * hyperlinks 2019-01-13
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}