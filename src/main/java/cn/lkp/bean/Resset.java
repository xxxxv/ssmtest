package cn.lkp.bean;

import javax.persistence.Table;
import org.springframework.stereotype.Component;

/**
 * resset
 * @author 杨孟新 2019-01-13
 */
@Table(name = "resset")

@Component
public class Resset {
    private Integer setId;

    private String setName;

    private String setType;

    private Integer idInType;

    public Integer getSetId() {
        return setId;
    }

    public void setSetId(Integer setId) {
        this.setId = setId;
    }

    public String getSetName() {
        return setName;
    }

    public void setSetName(String setName) {
        this.setName = setName == null ? null : setName.trim();
    }

    public String getSetType() {
        return setType;
    }

    public void setSetType(String setType) {
        this.setType = setType == null ? null : setType.trim();
    }

    public Integer getIdInType() {
        return idInType;
    }

    public void setIdInType(Integer idInType) {
        this.idInType = idInType;
    }
}