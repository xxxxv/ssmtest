package cn.lkp.bean;

import java.util.ArrayList;
import java.util.List;

public class HypertextExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public HypertextExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * hypertext 2019-01-13
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTextIdIsNull() {
            addCriterion("text_id is null");
            return (Criteria) this;
        }

        public Criteria andTextIdIsNotNull() {
            addCriterion("text_id is not null");
            return (Criteria) this;
        }

        public Criteria andTextIdEqualTo(Integer value) {
            addCriterion("text_id =", value, "textId");
            return (Criteria) this;
        }

        public Criteria andTextIdNotEqualTo(Integer value) {
            addCriterion("text_id <>", value, "textId");
            return (Criteria) this;
        }

        public Criteria andTextIdGreaterThan(Integer value) {
            addCriterion("text_id >", value, "textId");
            return (Criteria) this;
        }

        public Criteria andTextIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("text_id >=", value, "textId");
            return (Criteria) this;
        }

        public Criteria andTextIdLessThan(Integer value) {
            addCriterion("text_id <", value, "textId");
            return (Criteria) this;
        }

        public Criteria andTextIdLessThanOrEqualTo(Integer value) {
            addCriterion("text_id <=", value, "textId");
            return (Criteria) this;
        }

        public Criteria andTextIdIn(List<Integer> values) {
            addCriterion("text_id in", values, "textId");
            return (Criteria) this;
        }

        public Criteria andTextIdNotIn(List<Integer> values) {
            addCriterion("text_id not in", values, "textId");
            return (Criteria) this;
        }

        public Criteria andTextIdBetween(Integer value1, Integer value2) {
            addCriterion("text_id between", value1, value2, "textId");
            return (Criteria) this;
        }

        public Criteria andTextIdNotBetween(Integer value1, Integer value2) {
            addCriterion("text_id not between", value1, value2, "textId");
            return (Criteria) this;
        }

        public Criteria andTextUuidIsNull() {
            addCriterion("text_uuid is null");
            return (Criteria) this;
        }

        public Criteria andTextUuidIsNotNull() {
            addCriterion("text_uuid is not null");
            return (Criteria) this;
        }

        public Criteria andTextUuidEqualTo(String value) {
            addCriterion("text_uuid =", value, "textUuid");
            return (Criteria) this;
        }

        public Criteria andTextUuidNotEqualTo(String value) {
            addCriterion("text_uuid <>", value, "textUuid");
            return (Criteria) this;
        }

        public Criteria andTextUuidGreaterThan(String value) {
            addCriterion("text_uuid >", value, "textUuid");
            return (Criteria) this;
        }

        public Criteria andTextUuidGreaterThanOrEqualTo(String value) {
            addCriterion("text_uuid >=", value, "textUuid");
            return (Criteria) this;
        }

        public Criteria andTextUuidLessThan(String value) {
            addCriterion("text_uuid <", value, "textUuid");
            return (Criteria) this;
        }

        public Criteria andTextUuidLessThanOrEqualTo(String value) {
            addCriterion("text_uuid <=", value, "textUuid");
            return (Criteria) this;
        }

        public Criteria andTextUuidLike(String value) {
            addCriterion("text_uuid like", value, "textUuid");
            return (Criteria) this;
        }

        public Criteria andTextUuidNotLike(String value) {
            addCriterion("text_uuid not like", value, "textUuid");
            return (Criteria) this;
        }

        public Criteria andTextUuidIn(List<String> values) {
            addCriterion("text_uuid in", values, "textUuid");
            return (Criteria) this;
        }

        public Criteria andTextUuidNotIn(List<String> values) {
            addCriterion("text_uuid not in", values, "textUuid");
            return (Criteria) this;
        }

        public Criteria andTextUuidBetween(String value1, String value2) {
            addCriterion("text_uuid between", value1, value2, "textUuid");
            return (Criteria) this;
        }

        public Criteria andTextUuidNotBetween(String value1, String value2) {
            addCriterion("text_uuid not between", value1, value2, "textUuid");
            return (Criteria) this;
        }

        public Criteria andTextTitleIsNull() {
            addCriterion("text_title is null");
            return (Criteria) this;
        }

        public Criteria andTextTitleIsNotNull() {
            addCriterion("text_title is not null");
            return (Criteria) this;
        }

        public Criteria andTextTitleEqualTo(String value) {
            addCriterion("text_title =", value, "textTitle");
            return (Criteria) this;
        }

        public Criteria andTextTitleNotEqualTo(String value) {
            addCriterion("text_title <>", value, "textTitle");
            return (Criteria) this;
        }

        public Criteria andTextTitleGreaterThan(String value) {
            addCriterion("text_title >", value, "textTitle");
            return (Criteria) this;
        }

        public Criteria andTextTitleGreaterThanOrEqualTo(String value) {
            addCriterion("text_title >=", value, "textTitle");
            return (Criteria) this;
        }

        public Criteria andTextTitleLessThan(String value) {
            addCriterion("text_title <", value, "textTitle");
            return (Criteria) this;
        }

        public Criteria andTextTitleLessThanOrEqualTo(String value) {
            addCriterion("text_title <=", value, "textTitle");
            return (Criteria) this;
        }

        public Criteria andTextTitleLike(String value) {
            addCriterion("text_title like", value, "textTitle");
            return (Criteria) this;
        }

        public Criteria andTextTitleNotLike(String value) {
            addCriterion("text_title not like", value, "textTitle");
            return (Criteria) this;
        }

        public Criteria andTextTitleIn(List<String> values) {
            addCriterion("text_title in", values, "textTitle");
            return (Criteria) this;
        }

        public Criteria andTextTitleNotIn(List<String> values) {
            addCriterion("text_title not in", values, "textTitle");
            return (Criteria) this;
        }

        public Criteria andTextTitleBetween(String value1, String value2) {
            addCriterion("text_title between", value1, value2, "textTitle");
            return (Criteria) this;
        }

        public Criteria andTextTitleNotBetween(String value1, String value2) {
            addCriterion("text_title not between", value1, value2, "textTitle");
            return (Criteria) this;
        }

        public Criteria andTextViceTitleIsNull() {
            addCriterion("text_vice_title is null");
            return (Criteria) this;
        }

        public Criteria andTextViceTitleIsNotNull() {
            addCriterion("text_vice_title is not null");
            return (Criteria) this;
        }

        public Criteria andTextViceTitleEqualTo(String value) {
            addCriterion("text_vice_title =", value, "textViceTitle");
            return (Criteria) this;
        }

        public Criteria andTextViceTitleNotEqualTo(String value) {
            addCriterion("text_vice_title <>", value, "textViceTitle");
            return (Criteria) this;
        }

        public Criteria andTextViceTitleGreaterThan(String value) {
            addCriterion("text_vice_title >", value, "textViceTitle");
            return (Criteria) this;
        }

        public Criteria andTextViceTitleGreaterThanOrEqualTo(String value) {
            addCriterion("text_vice_title >=", value, "textViceTitle");
            return (Criteria) this;
        }

        public Criteria andTextViceTitleLessThan(String value) {
            addCriterion("text_vice_title <", value, "textViceTitle");
            return (Criteria) this;
        }

        public Criteria andTextViceTitleLessThanOrEqualTo(String value) {
            addCriterion("text_vice_title <=", value, "textViceTitle");
            return (Criteria) this;
        }

        public Criteria andTextViceTitleLike(String value) {
            addCriterion("text_vice_title like", value, "textViceTitle");
            return (Criteria) this;
        }

        public Criteria andTextViceTitleNotLike(String value) {
            addCriterion("text_vice_title not like", value, "textViceTitle");
            return (Criteria) this;
        }

        public Criteria andTextViceTitleIn(List<String> values) {
            addCriterion("text_vice_title in", values, "textViceTitle");
            return (Criteria) this;
        }

        public Criteria andTextViceTitleNotIn(List<String> values) {
            addCriterion("text_vice_title not in", values, "textViceTitle");
            return (Criteria) this;
        }

        public Criteria andTextViceTitleBetween(String value1, String value2) {
            addCriterion("text_vice_title between", value1, value2, "textViceTitle");
            return (Criteria) this;
        }

        public Criteria andTextViceTitleNotBetween(String value1, String value2) {
            addCriterion("text_vice_title not between", value1, value2, "textViceTitle");
            return (Criteria) this;
        }

        public Criteria andTextTitleColorIsNull() {
            addCriterion("text_title_color is null");
            return (Criteria) this;
        }

        public Criteria andTextTitleColorIsNotNull() {
            addCriterion("text_title_color is not null");
            return (Criteria) this;
        }

        public Criteria andTextTitleColorEqualTo(String value) {
            addCriterion("text_title_color =", value, "textTitleColor");
            return (Criteria) this;
        }

        public Criteria andTextTitleColorNotEqualTo(String value) {
            addCriterion("text_title_color <>", value, "textTitleColor");
            return (Criteria) this;
        }

        public Criteria andTextTitleColorGreaterThan(String value) {
            addCriterion("text_title_color >", value, "textTitleColor");
            return (Criteria) this;
        }

        public Criteria andTextTitleColorGreaterThanOrEqualTo(String value) {
            addCriterion("text_title_color >=", value, "textTitleColor");
            return (Criteria) this;
        }

        public Criteria andTextTitleColorLessThan(String value) {
            addCriterion("text_title_color <", value, "textTitleColor");
            return (Criteria) this;
        }

        public Criteria andTextTitleColorLessThanOrEqualTo(String value) {
            addCriterion("text_title_color <=", value, "textTitleColor");
            return (Criteria) this;
        }

        public Criteria andTextTitleColorLike(String value) {
            addCriterion("text_title_color like", value, "textTitleColor");
            return (Criteria) this;
        }

        public Criteria andTextTitleColorNotLike(String value) {
            addCriterion("text_title_color not like", value, "textTitleColor");
            return (Criteria) this;
        }

        public Criteria andTextTitleColorIn(List<String> values) {
            addCriterion("text_title_color in", values, "textTitleColor");
            return (Criteria) this;
        }

        public Criteria andTextTitleColorNotIn(List<String> values) {
            addCriterion("text_title_color not in", values, "textTitleColor");
            return (Criteria) this;
        }

        public Criteria andTextTitleColorBetween(String value1, String value2) {
            addCriterion("text_title_color between", value1, value2, "textTitleColor");
            return (Criteria) this;
        }

        public Criteria andTextTitleColorNotBetween(String value1, String value2) {
            addCriterion("text_title_color not between", value1, value2, "textTitleColor");
            return (Criteria) this;
        }

        public Criteria andTextImageUrlIsNull() {
            addCriterion("text_image_url is null");
            return (Criteria) this;
        }

        public Criteria andTextImageUrlIsNotNull() {
            addCriterion("text_image_url is not null");
            return (Criteria) this;
        }

        public Criteria andTextImageUrlEqualTo(String value) {
            addCriterion("text_image_url =", value, "textImageUrl");
            return (Criteria) this;
        }

        public Criteria andTextImageUrlNotEqualTo(String value) {
            addCriterion("text_image_url <>", value, "textImageUrl");
            return (Criteria) this;
        }

        public Criteria andTextImageUrlGreaterThan(String value) {
            addCriterion("text_image_url >", value, "textImageUrl");
            return (Criteria) this;
        }

        public Criteria andTextImageUrlGreaterThanOrEqualTo(String value) {
            addCriterion("text_image_url >=", value, "textImageUrl");
            return (Criteria) this;
        }

        public Criteria andTextImageUrlLessThan(String value) {
            addCriterion("text_image_url <", value, "textImageUrl");
            return (Criteria) this;
        }

        public Criteria andTextImageUrlLessThanOrEqualTo(String value) {
            addCriterion("text_image_url <=", value, "textImageUrl");
            return (Criteria) this;
        }

        public Criteria andTextImageUrlLike(String value) {
            addCriterion("text_image_url like", value, "textImageUrl");
            return (Criteria) this;
        }

        public Criteria andTextImageUrlNotLike(String value) {
            addCriterion("text_image_url not like", value, "textImageUrl");
            return (Criteria) this;
        }

        public Criteria andTextImageUrlIn(List<String> values) {
            addCriterion("text_image_url in", values, "textImageUrl");
            return (Criteria) this;
        }

        public Criteria andTextImageUrlNotIn(List<String> values) {
            addCriterion("text_image_url not in", values, "textImageUrl");
            return (Criteria) this;
        }

        public Criteria andTextImageUrlBetween(String value1, String value2) {
            addCriterion("text_image_url between", value1, value2, "textImageUrl");
            return (Criteria) this;
        }

        public Criteria andTextImageUrlNotBetween(String value1, String value2) {
            addCriterion("text_image_url not between", value1, value2, "textImageUrl");
            return (Criteria) this;
        }

        public Criteria andTextContentIsNull() {
            addCriterion("text_content is null");
            return (Criteria) this;
        }

        public Criteria andTextContentIsNotNull() {
            addCriterion("text_content is not null");
            return (Criteria) this;
        }

        public Criteria andTextContentEqualTo(String value) {
            addCriterion("text_content =", value, "textContent");
            return (Criteria) this;
        }

        public Criteria andTextContentNotEqualTo(String value) {
            addCriterion("text_content <>", value, "textContent");
            return (Criteria) this;
        }

        public Criteria andTextContentGreaterThan(String value) {
            addCriterion("text_content >", value, "textContent");
            return (Criteria) this;
        }

        public Criteria andTextContentGreaterThanOrEqualTo(String value) {
            addCriterion("text_content >=", value, "textContent");
            return (Criteria) this;
        }

        public Criteria andTextContentLessThan(String value) {
            addCriterion("text_content <", value, "textContent");
            return (Criteria) this;
        }

        public Criteria andTextContentLessThanOrEqualTo(String value) {
            addCriterion("text_content <=", value, "textContent");
            return (Criteria) this;
        }

        public Criteria andTextContentLike(String value) {
            addCriterion("text_content like", value, "textContent");
            return (Criteria) this;
        }

        public Criteria andTextContentNotLike(String value) {
            addCriterion("text_content not like", value, "textContent");
            return (Criteria) this;
        }

        public Criteria andTextContentIn(List<String> values) {
            addCriterion("text_content in", values, "textContent");
            return (Criteria) this;
        }

        public Criteria andTextContentNotIn(List<String> values) {
            addCriterion("text_content not in", values, "textContent");
            return (Criteria) this;
        }

        public Criteria andTextContentBetween(String value1, String value2) {
            addCriterion("text_content between", value1, value2, "textContent");
            return (Criteria) this;
        }

        public Criteria andTextContentNotBetween(String value1, String value2) {
            addCriterion("text_content not between", value1, value2, "textContent");
            return (Criteria) this;
        }

        public Criteria andTextTitleUrlIsNull() {
            addCriterion("text_title_url is null");
            return (Criteria) this;
        }

        public Criteria andTextTitleUrlIsNotNull() {
            addCriterion("text_title_url is not null");
            return (Criteria) this;
        }

        public Criteria andTextTitleUrlEqualTo(String value) {
            addCriterion("text_title_url =", value, "textTitleUrl");
            return (Criteria) this;
        }

        public Criteria andTextTitleUrlNotEqualTo(String value) {
            addCriterion("text_title_url <>", value, "textTitleUrl");
            return (Criteria) this;
        }

        public Criteria andTextTitleUrlGreaterThan(String value) {
            addCriterion("text_title_url >", value, "textTitleUrl");
            return (Criteria) this;
        }

        public Criteria andTextTitleUrlGreaterThanOrEqualTo(String value) {
            addCriterion("text_title_url >=", value, "textTitleUrl");
            return (Criteria) this;
        }

        public Criteria andTextTitleUrlLessThan(String value) {
            addCriterion("text_title_url <", value, "textTitleUrl");
            return (Criteria) this;
        }

        public Criteria andTextTitleUrlLessThanOrEqualTo(String value) {
            addCriterion("text_title_url <=", value, "textTitleUrl");
            return (Criteria) this;
        }

        public Criteria andTextTitleUrlLike(String value) {
            addCriterion("text_title_url like", value, "textTitleUrl");
            return (Criteria) this;
        }

        public Criteria andTextTitleUrlNotLike(String value) {
            addCriterion("text_title_url not like", value, "textTitleUrl");
            return (Criteria) this;
        }

        public Criteria andTextTitleUrlIn(List<String> values) {
            addCriterion("text_title_url in", values, "textTitleUrl");
            return (Criteria) this;
        }

        public Criteria andTextTitleUrlNotIn(List<String> values) {
            addCriterion("text_title_url not in", values, "textTitleUrl");
            return (Criteria) this;
        }

        public Criteria andTextTitleUrlBetween(String value1, String value2) {
            addCriterion("text_title_url between", value1, value2, "textTitleUrl");
            return (Criteria) this;
        }

        public Criteria andTextTitleUrlNotBetween(String value1, String value2) {
            addCriterion("text_title_url not between", value1, value2, "textTitleUrl");
            return (Criteria) this;
        }

        public Criteria andTextCheckedIsNull() {
            addCriterion("text_checked is null");
            return (Criteria) this;
        }

        public Criteria andTextCheckedIsNotNull() {
            addCriterion("text_checked is not null");
            return (Criteria) this;
        }

        public Criteria andTextCheckedEqualTo(Integer value) {
            addCriterion("text_checked =", value, "textChecked");
            return (Criteria) this;
        }

        public Criteria andTextCheckedNotEqualTo(Integer value) {
            addCriterion("text_checked <>", value, "textChecked");
            return (Criteria) this;
        }

        public Criteria andTextCheckedGreaterThan(Integer value) {
            addCriterion("text_checked >", value, "textChecked");
            return (Criteria) this;
        }

        public Criteria andTextCheckedGreaterThanOrEqualTo(Integer value) {
            addCriterion("text_checked >=", value, "textChecked");
            return (Criteria) this;
        }

        public Criteria andTextCheckedLessThan(Integer value) {
            addCriterion("text_checked <", value, "textChecked");
            return (Criteria) this;
        }

        public Criteria andTextCheckedLessThanOrEqualTo(Integer value) {
            addCriterion("text_checked <=", value, "textChecked");
            return (Criteria) this;
        }

        public Criteria andTextCheckedIn(List<Integer> values) {
            addCriterion("text_checked in", values, "textChecked");
            return (Criteria) this;
        }

        public Criteria andTextCheckedNotIn(List<Integer> values) {
            addCriterion("text_checked not in", values, "textChecked");
            return (Criteria) this;
        }

        public Criteria andTextCheckedBetween(Integer value1, Integer value2) {
            addCriterion("text_checked between", value1, value2, "textChecked");
            return (Criteria) this;
        }

        public Criteria andTextCheckedNotBetween(Integer value1, Integer value2) {
            addCriterion("text_checked not between", value1, value2, "textChecked");
            return (Criteria) this;
        }

        public Criteria andTextCheckerIsNull() {
            addCriterion("text_checker is null");
            return (Criteria) this;
        }

        public Criteria andTextCheckerIsNotNull() {
            addCriterion("text_checker is not null");
            return (Criteria) this;
        }

        public Criteria andTextCheckerEqualTo(String value) {
            addCriterion("text_checker =", value, "textChecker");
            return (Criteria) this;
        }

        public Criteria andTextCheckerNotEqualTo(String value) {
            addCriterion("text_checker <>", value, "textChecker");
            return (Criteria) this;
        }

        public Criteria andTextCheckerGreaterThan(String value) {
            addCriterion("text_checker >", value, "textChecker");
            return (Criteria) this;
        }

        public Criteria andTextCheckerGreaterThanOrEqualTo(String value) {
            addCriterion("text_checker >=", value, "textChecker");
            return (Criteria) this;
        }

        public Criteria andTextCheckerLessThan(String value) {
            addCriterion("text_checker <", value, "textChecker");
            return (Criteria) this;
        }

        public Criteria andTextCheckerLessThanOrEqualTo(String value) {
            addCriterion("text_checker <=", value, "textChecker");
            return (Criteria) this;
        }

        public Criteria andTextCheckerLike(String value) {
            addCriterion("text_checker like", value, "textChecker");
            return (Criteria) this;
        }

        public Criteria andTextCheckerNotLike(String value) {
            addCriterion("text_checker not like", value, "textChecker");
            return (Criteria) this;
        }

        public Criteria andTextCheckerIn(List<String> values) {
            addCriterion("text_checker in", values, "textChecker");
            return (Criteria) this;
        }

        public Criteria andTextCheckerNotIn(List<String> values) {
            addCriterion("text_checker not in", values, "textChecker");
            return (Criteria) this;
        }

        public Criteria andTextCheckerBetween(String value1, String value2) {
            addCriterion("text_checker between", value1, value2, "textChecker");
            return (Criteria) this;
        }

        public Criteria andTextCheckerNotBetween(String value1, String value2) {
            addCriterion("text_checker not between", value1, value2, "textChecker");
            return (Criteria) this;
        }

        public Criteria andTextPublishtimeIsNull() {
            addCriterion("text_publishtime is null");
            return (Criteria) this;
        }

        public Criteria andTextPublishtimeIsNotNull() {
            addCriterion("text_publishtime is not null");
            return (Criteria) this;
        }

        public Criteria andTextPublishtimeEqualTo(String value) {
            addCriterion("text_publishtime =", value, "textPublishtime");
            return (Criteria) this;
        }

        public Criteria andTextPublishtimeNotEqualTo(String value) {
            addCriterion("text_publishtime <>", value, "textPublishtime");
            return (Criteria) this;
        }

        public Criteria andTextPublishtimeGreaterThan(String value) {
            addCriterion("text_publishtime >", value, "textPublishtime");
            return (Criteria) this;
        }

        public Criteria andTextPublishtimeGreaterThanOrEqualTo(String value) {
            addCriterion("text_publishtime >=", value, "textPublishtime");
            return (Criteria) this;
        }

        public Criteria andTextPublishtimeLessThan(String value) {
            addCriterion("text_publishtime <", value, "textPublishtime");
            return (Criteria) this;
        }

        public Criteria andTextPublishtimeLessThanOrEqualTo(String value) {
            addCriterion("text_publishtime <=", value, "textPublishtime");
            return (Criteria) this;
        }

        public Criteria andTextPublishtimeLike(String value) {
            addCriterion("text_publishtime like", value, "textPublishtime");
            return (Criteria) this;
        }

        public Criteria andTextPublishtimeNotLike(String value) {
            addCriterion("text_publishtime not like", value, "textPublishtime");
            return (Criteria) this;
        }

        public Criteria andTextPublishtimeIn(List<String> values) {
            addCriterion("text_publishtime in", values, "textPublishtime");
            return (Criteria) this;
        }

        public Criteria andTextPublishtimeNotIn(List<String> values) {
            addCriterion("text_publishtime not in", values, "textPublishtime");
            return (Criteria) this;
        }

        public Criteria andTextPublishtimeBetween(String value1, String value2) {
            addCriterion("text_publishtime between", value1, value2, "textPublishtime");
            return (Criteria) this;
        }

        public Criteria andTextPublishtimeNotBetween(String value1, String value2) {
            addCriterion("text_publishtime not between", value1, value2, "textPublishtime");
            return (Criteria) this;
        }

        public Criteria andTextPublisherIsNull() {
            addCriterion("text_publisher is null");
            return (Criteria) this;
        }

        public Criteria andTextPublisherIsNotNull() {
            addCriterion("text_publisher is not null");
            return (Criteria) this;
        }

        public Criteria andTextPublisherEqualTo(Integer value) {
            addCriterion("text_publisher =", value, "textPublisher");
            return (Criteria) this;
        }

        public Criteria andTextPublisherNotEqualTo(Integer value) {
            addCriterion("text_publisher <>", value, "textPublisher");
            return (Criteria) this;
        }

        public Criteria andTextPublisherGreaterThan(Integer value) {
            addCriterion("text_publisher >", value, "textPublisher");
            return (Criteria) this;
        }

        public Criteria andTextPublisherGreaterThanOrEqualTo(Integer value) {
            addCriterion("text_publisher >=", value, "textPublisher");
            return (Criteria) this;
        }

        public Criteria andTextPublisherLessThan(Integer value) {
            addCriterion("text_publisher <", value, "textPublisher");
            return (Criteria) this;
        }

        public Criteria andTextPublisherLessThanOrEqualTo(Integer value) {
            addCriterion("text_publisher <=", value, "textPublisher");
            return (Criteria) this;
        }

        public Criteria andTextPublisherIn(List<Integer> values) {
            addCriterion("text_publisher in", values, "textPublisher");
            return (Criteria) this;
        }

        public Criteria andTextPublisherNotIn(List<Integer> values) {
            addCriterion("text_publisher not in", values, "textPublisher");
            return (Criteria) this;
        }

        public Criteria andTextPublisherBetween(Integer value1, Integer value2) {
            addCriterion("text_publisher between", value1, value2, "textPublisher");
            return (Criteria) this;
        }

        public Criteria andTextPublisherNotBetween(Integer value1, Integer value2) {
            addCriterion("text_publisher not between", value1, value2, "textPublisher");
            return (Criteria) this;
        }

        public Criteria andTextChecktimeIsNull() {
            addCriterion("text_checktime is null");
            return (Criteria) this;
        }

        public Criteria andTextChecktimeIsNotNull() {
            addCriterion("text_checktime is not null");
            return (Criteria) this;
        }

        public Criteria andTextChecktimeEqualTo(String value) {
            addCriterion("text_checktime =", value, "textChecktime");
            return (Criteria) this;
        }

        public Criteria andTextChecktimeNotEqualTo(String value) {
            addCriterion("text_checktime <>", value, "textChecktime");
            return (Criteria) this;
        }

        public Criteria andTextChecktimeGreaterThan(String value) {
            addCriterion("text_checktime >", value, "textChecktime");
            return (Criteria) this;
        }

        public Criteria andTextChecktimeGreaterThanOrEqualTo(String value) {
            addCriterion("text_checktime >=", value, "textChecktime");
            return (Criteria) this;
        }

        public Criteria andTextChecktimeLessThan(String value) {
            addCriterion("text_checktime <", value, "textChecktime");
            return (Criteria) this;
        }

        public Criteria andTextChecktimeLessThanOrEqualTo(String value) {
            addCriterion("text_checktime <=", value, "textChecktime");
            return (Criteria) this;
        }

        public Criteria andTextChecktimeLike(String value) {
            addCriterion("text_checktime like", value, "textChecktime");
            return (Criteria) this;
        }

        public Criteria andTextChecktimeNotLike(String value) {
            addCriterion("text_checktime not like", value, "textChecktime");
            return (Criteria) this;
        }

        public Criteria andTextChecktimeIn(List<String> values) {
            addCriterion("text_checktime in", values, "textChecktime");
            return (Criteria) this;
        }

        public Criteria andTextChecktimeNotIn(List<String> values) {
            addCriterion("text_checktime not in", values, "textChecktime");
            return (Criteria) this;
        }

        public Criteria andTextChecktimeBetween(String value1, String value2) {
            addCriterion("text_checktime between", value1, value2, "textChecktime");
            return (Criteria) this;
        }

        public Criteria andTextChecktimeNotBetween(String value1, String value2) {
            addCriterion("text_checktime not between", value1, value2, "textChecktime");
            return (Criteria) this;
        }

        public Criteria andTextSourceUrlIsNull() {
            addCriterion("text_source_url is null");
            return (Criteria) this;
        }

        public Criteria andTextSourceUrlIsNotNull() {
            addCriterion("text_source_url is not null");
            return (Criteria) this;
        }

        public Criteria andTextSourceUrlEqualTo(String value) {
            addCriterion("text_source_url =", value, "textSourceUrl");
            return (Criteria) this;
        }

        public Criteria andTextSourceUrlNotEqualTo(String value) {
            addCriterion("text_source_url <>", value, "textSourceUrl");
            return (Criteria) this;
        }

        public Criteria andTextSourceUrlGreaterThan(String value) {
            addCriterion("text_source_url >", value, "textSourceUrl");
            return (Criteria) this;
        }

        public Criteria andTextSourceUrlGreaterThanOrEqualTo(String value) {
            addCriterion("text_source_url >=", value, "textSourceUrl");
            return (Criteria) this;
        }

        public Criteria andTextSourceUrlLessThan(String value) {
            addCriterion("text_source_url <", value, "textSourceUrl");
            return (Criteria) this;
        }

        public Criteria andTextSourceUrlLessThanOrEqualTo(String value) {
            addCriterion("text_source_url <=", value, "textSourceUrl");
            return (Criteria) this;
        }

        public Criteria andTextSourceUrlLike(String value) {
            addCriterion("text_source_url like", value, "textSourceUrl");
            return (Criteria) this;
        }

        public Criteria andTextSourceUrlNotLike(String value) {
            addCriterion("text_source_url not like", value, "textSourceUrl");
            return (Criteria) this;
        }

        public Criteria andTextSourceUrlIn(List<String> values) {
            addCriterion("text_source_url in", values, "textSourceUrl");
            return (Criteria) this;
        }

        public Criteria andTextSourceUrlNotIn(List<String> values) {
            addCriterion("text_source_url not in", values, "textSourceUrl");
            return (Criteria) this;
        }

        public Criteria andTextSourceUrlBetween(String value1, String value2) {
            addCriterion("text_source_url between", value1, value2, "textSourceUrl");
            return (Criteria) this;
        }

        public Criteria andTextSourceUrlNotBetween(String value1, String value2) {
            addCriterion("text_source_url not between", value1, value2, "textSourceUrl");
            return (Criteria) this;
        }

        public Criteria andTextFlagIsNull() {
            addCriterion("text_flag is null");
            return (Criteria) this;
        }

        public Criteria andTextFlagIsNotNull() {
            addCriterion("text_flag is not null");
            return (Criteria) this;
        }

        public Criteria andTextFlagEqualTo(Integer value) {
            addCriterion("text_flag =", value, "textFlag");
            return (Criteria) this;
        }

        public Criteria andTextFlagNotEqualTo(Integer value) {
            addCriterion("text_flag <>", value, "textFlag");
            return (Criteria) this;
        }

        public Criteria andTextFlagGreaterThan(Integer value) {
            addCriterion("text_flag >", value, "textFlag");
            return (Criteria) this;
        }

        public Criteria andTextFlagGreaterThanOrEqualTo(Integer value) {
            addCriterion("text_flag >=", value, "textFlag");
            return (Criteria) this;
        }

        public Criteria andTextFlagLessThan(Integer value) {
            addCriterion("text_flag <", value, "textFlag");
            return (Criteria) this;
        }

        public Criteria andTextFlagLessThanOrEqualTo(Integer value) {
            addCriterion("text_flag <=", value, "textFlag");
            return (Criteria) this;
        }

        public Criteria andTextFlagIn(List<Integer> values) {
            addCriterion("text_flag in", values, "textFlag");
            return (Criteria) this;
        }

        public Criteria andTextFlagNotIn(List<Integer> values) {
            addCriterion("text_flag not in", values, "textFlag");
            return (Criteria) this;
        }

        public Criteria andTextFlagBetween(Integer value1, Integer value2) {
            addCriterion("text_flag between", value1, value2, "textFlag");
            return (Criteria) this;
        }

        public Criteria andTextFlagNotBetween(Integer value1, Integer value2) {
            addCriterion("text_flag not between", value1, value2, "textFlag");
            return (Criteria) this;
        }

        public Criteria andTextDeletedIsNull() {
            addCriterion("text_deleted is null");
            return (Criteria) this;
        }

        public Criteria andTextDeletedIsNotNull() {
            addCriterion("text_deleted is not null");
            return (Criteria) this;
        }

        public Criteria andTextDeletedEqualTo(Integer value) {
            addCriterion("text_deleted =", value, "textDeleted");
            return (Criteria) this;
        }

        public Criteria andTextDeletedNotEqualTo(Integer value) {
            addCriterion("text_deleted <>", value, "textDeleted");
            return (Criteria) this;
        }

        public Criteria andTextDeletedGreaterThan(Integer value) {
            addCriterion("text_deleted >", value, "textDeleted");
            return (Criteria) this;
        }

        public Criteria andTextDeletedGreaterThanOrEqualTo(Integer value) {
            addCriterion("text_deleted >=", value, "textDeleted");
            return (Criteria) this;
        }

        public Criteria andTextDeletedLessThan(Integer value) {
            addCriterion("text_deleted <", value, "textDeleted");
            return (Criteria) this;
        }

        public Criteria andTextDeletedLessThanOrEqualTo(Integer value) {
            addCriterion("text_deleted <=", value, "textDeleted");
            return (Criteria) this;
        }

        public Criteria andTextDeletedIn(List<Integer> values) {
            addCriterion("text_deleted in", values, "textDeleted");
            return (Criteria) this;
        }

        public Criteria andTextDeletedNotIn(List<Integer> values) {
            addCriterion("text_deleted not in", values, "textDeleted");
            return (Criteria) this;
        }

        public Criteria andTextDeletedBetween(Integer value1, Integer value2) {
            addCriterion("text_deleted between", value1, value2, "textDeleted");
            return (Criteria) this;
        }

        public Criteria andTextDeletedNotBetween(Integer value1, Integer value2) {
            addCriterion("text_deleted not between", value1, value2, "textDeleted");
            return (Criteria) this;
        }

        public Criteria andTextAsynidIsNull() {
            addCriterion("text_asynid is null");
            return (Criteria) this;
        }

        public Criteria andTextAsynidIsNotNull() {
            addCriterion("text_asynid is not null");
            return (Criteria) this;
        }

        public Criteria andTextAsynidEqualTo(Integer value) {
            addCriterion("text_asynid =", value, "textAsynid");
            return (Criteria) this;
        }

        public Criteria andTextAsynidNotEqualTo(Integer value) {
            addCriterion("text_asynid <>", value, "textAsynid");
            return (Criteria) this;
        }

        public Criteria andTextAsynidGreaterThan(Integer value) {
            addCriterion("text_asynid >", value, "textAsynid");
            return (Criteria) this;
        }

        public Criteria andTextAsynidGreaterThanOrEqualTo(Integer value) {
            addCriterion("text_asynid >=", value, "textAsynid");
            return (Criteria) this;
        }

        public Criteria andTextAsynidLessThan(Integer value) {
            addCriterion("text_asynid <", value, "textAsynid");
            return (Criteria) this;
        }

        public Criteria andTextAsynidLessThanOrEqualTo(Integer value) {
            addCriterion("text_asynid <=", value, "textAsynid");
            return (Criteria) this;
        }

        public Criteria andTextAsynidIn(List<Integer> values) {
            addCriterion("text_asynid in", values, "textAsynid");
            return (Criteria) this;
        }

        public Criteria andTextAsynidNotIn(List<Integer> values) {
            addCriterion("text_asynid not in", values, "textAsynid");
            return (Criteria) this;
        }

        public Criteria andTextAsynidBetween(Integer value1, Integer value2) {
            addCriterion("text_asynid between", value1, value2, "textAsynid");
            return (Criteria) this;
        }

        public Criteria andTextAsynidNotBetween(Integer value1, Integer value2) {
            addCriterion("text_asynid not between", value1, value2, "textAsynid");
            return (Criteria) this;
        }

        public Criteria andTextVisitCounterIsNull() {
            addCriterion("text_visit_counter is null");
            return (Criteria) this;
        }

        public Criteria andTextVisitCounterIsNotNull() {
            addCriterion("text_visit_counter is not null");
            return (Criteria) this;
        }

        public Criteria andTextVisitCounterEqualTo(Integer value) {
            addCriterion("text_visit_counter =", value, "textVisitCounter");
            return (Criteria) this;
        }

        public Criteria andTextVisitCounterNotEqualTo(Integer value) {
            addCriterion("text_visit_counter <>", value, "textVisitCounter");
            return (Criteria) this;
        }

        public Criteria andTextVisitCounterGreaterThan(Integer value) {
            addCriterion("text_visit_counter >", value, "textVisitCounter");
            return (Criteria) this;
        }

        public Criteria andTextVisitCounterGreaterThanOrEqualTo(Integer value) {
            addCriterion("text_visit_counter >=", value, "textVisitCounter");
            return (Criteria) this;
        }

        public Criteria andTextVisitCounterLessThan(Integer value) {
            addCriterion("text_visit_counter <", value, "textVisitCounter");
            return (Criteria) this;
        }

        public Criteria andTextVisitCounterLessThanOrEqualTo(Integer value) {
            addCriterion("text_visit_counter <=", value, "textVisitCounter");
            return (Criteria) this;
        }

        public Criteria andTextVisitCounterIn(List<Integer> values) {
            addCriterion("text_visit_counter in", values, "textVisitCounter");
            return (Criteria) this;
        }

        public Criteria andTextVisitCounterNotIn(List<Integer> values) {
            addCriterion("text_visit_counter not in", values, "textVisitCounter");
            return (Criteria) this;
        }

        public Criteria andTextVisitCounterBetween(Integer value1, Integer value2) {
            addCriterion("text_visit_counter between", value1, value2, "textVisitCounter");
            return (Criteria) this;
        }

        public Criteria andTextVisitCounterNotBetween(Integer value1, Integer value2) {
            addCriterion("text_visit_counter not between", value1, value2, "textVisitCounter");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * hypertext 2019-01-13
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}