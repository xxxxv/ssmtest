package cn.lkp.bean;

import javax.persistence.Table;
import org.springframework.stereotype.Component;

/**
 * categorynode
 * @author 杨孟新 2019-01-13
 */
@Table(name = "categorynode")

@Component
public class Categorynode {
    private Integer categoryId;

    private Integer categoryParentId;

    private String categoryLogoPath;

    private String categoriyUrl;

    private Integer categoryChecked;

    private String categoryComment;

    private String categoriyDescription;

    private Integer categorieyRessetId;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getCategoryParentId() {
        return categoryParentId;
    }

    public void setCategoryParentId(Integer categoryParentId) {
        this.categoryParentId = categoryParentId;
    }

    public String getCategoryLogoPath() {
        return categoryLogoPath;
    }

    public void setCategoryLogoPath(String categoryLogoPath) {
        this.categoryLogoPath = categoryLogoPath == null ? null : categoryLogoPath.trim();
    }

    public String getCategoriyUrl() {
        return categoriyUrl;
    }

    public void setCategoriyUrl(String categoriyUrl) {
        this.categoriyUrl = categoriyUrl == null ? null : categoriyUrl.trim();
    }

    public Integer getCategoryChecked() {
        return categoryChecked;
    }

    public void setCategoryChecked(Integer categoryChecked) {
        this.categoryChecked = categoryChecked;
    }

    public String getCategoryComment() {
        return categoryComment;
    }

    public void setCategoryComment(String categoryComment) {
        this.categoryComment = categoryComment == null ? null : categoryComment.trim();
    }

    public String getCategoriyDescription() {
        return categoriyDescription;
    }

    public void setCategoriyDescription(String categoriyDescription) {
        this.categoriyDescription = categoriyDescription == null ? null : categoriyDescription.trim();
    }

    public Integer getCategorieyRessetId() {
        return categorieyRessetId;
    }

    public void setCategorieyRessetId(Integer categorieyRessetId) {
        this.categorieyRessetId = categorieyRessetId;
    }
}