package cn.lkp.bean;

import javax.persistence.Table;
import org.springframework.stereotype.Component;

/**
 * privilege
 * @author 杨孟新 2019-01-13
 */
@Table(name = "privilege")

@Component
public class Privilege {
    private Integer priviledgeId;

    private String description;

    private Integer tag;

    public Integer getPriviledgeId() {
        return priviledgeId;
    }

    public void setPriviledgeId(Integer priviledgeId) {
        this.priviledgeId = priviledgeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Integer getTag() {
        return tag;
    }

    public void setTag(Integer tag) {
        this.tag = tag;
    }
}