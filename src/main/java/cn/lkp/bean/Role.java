package cn.lkp.bean;

import javax.persistence.Table;
import org.springframework.stereotype.Component;

/**
 * role
 * @author 杨孟新 2019-01-13
 */
@Table(name = "role")

@Component
public class Role {
    private Integer roleId;

    private String roleName;

    private String roleDescription;

    private Integer rolePrivilegeValue;

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }

    public String getRoleDescription() {
        return roleDescription;
    }

    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription == null ? null : roleDescription.trim();
    }

    public Integer getRolePrivilegeValue() {
        return rolePrivilegeValue;
    }

    public void setRolePrivilegeValue(Integer rolePrivilegeValue) {
        this.rolePrivilegeValue = rolePrivilegeValue;
    }
}