package cn.lkp.bean;

import javax.persistence.Table;
import org.springframework.stereotype.Component;

/**
 * hyperlinks
 * @author 杨孟新 2019-01-13
 */
@Table(name = "hyperlinks")

@Component
public class Hyperlinks {
    private Integer linkId;

    private String linkUuid;

    private String linkUrl;

    private String linkTitle;

    private String linkDescription;

    private Integer linkChecked;

    private Integer linkChecker;

    private String linkChecktime;

    private String linkImage;

    private Integer linkDeleted;

    private Integer linkAsynid;

    private Integer linkVisitCounter;

    public Integer getLinkId() {
        return linkId;
    }

    public void setLinkId(Integer linkId) {
        this.linkId = linkId;
    }

    public String getLinkUuid() {
        return linkUuid;
    }

    public void setLinkUuid(String linkUuid) {
        this.linkUuid = linkUuid == null ? null : linkUuid.trim();
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl == null ? null : linkUrl.trim();
    }

    public String getLinkTitle() {
        return linkTitle;
    }

    public void setLinkTitle(String linkTitle) {
        this.linkTitle = linkTitle == null ? null : linkTitle.trim();
    }

    public String getLinkDescription() {
        return linkDescription;
    }

    public void setLinkDescription(String linkDescription) {
        this.linkDescription = linkDescription == null ? null : linkDescription.trim();
    }

    public Integer getLinkChecked() {
        return linkChecked;
    }

    public void setLinkChecked(Integer linkChecked) {
        this.linkChecked = linkChecked;
    }

    public Integer getLinkChecker() {
        return linkChecker;
    }

    public void setLinkChecker(Integer linkChecker) {
        this.linkChecker = linkChecker;
    }

    public String getLinkChecktime() {
        return linkChecktime;
    }

    public void setLinkChecktime(String linkChecktime) {
        this.linkChecktime = linkChecktime == null ? null : linkChecktime.trim();
    }

    public String getLinkImage() {
        return linkImage;
    }

    public void setLinkImage(String linkImage) {
        this.linkImage = linkImage == null ? null : linkImage.trim();
    }

    public Integer getLinkDeleted() {
        return linkDeleted;
    }

    public void setLinkDeleted(Integer linkDeleted) {
        this.linkDeleted = linkDeleted;
    }

    public Integer getLinkAsynid() {
        return linkAsynid;
    }

    public void setLinkAsynid(Integer linkAsynid) {
        this.linkAsynid = linkAsynid;
    }

    public Integer getLinkVisitCounter() {
        return linkVisitCounter;
    }

    public void setLinkVisitCounter(Integer linkVisitCounter) {
        this.linkVisitCounter = linkVisitCounter;
    }
}