package cn.lkp.bean;

import java.util.ArrayList;
import java.util.List;

public class TagRelationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TagRelationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * tag_relation 2019-01-13
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTagRelationIdIsNull() {
            addCriterion("tag_relation_id is null");
            return (Criteria) this;
        }

        public Criteria andTagRelationIdIsNotNull() {
            addCriterion("tag_relation_id is not null");
            return (Criteria) this;
        }

        public Criteria andTagRelationIdEqualTo(Integer value) {
            addCriterion("tag_relation_id =", value, "tagRelationId");
            return (Criteria) this;
        }

        public Criteria andTagRelationIdNotEqualTo(Integer value) {
            addCriterion("tag_relation_id <>", value, "tagRelationId");
            return (Criteria) this;
        }

        public Criteria andTagRelationIdGreaterThan(Integer value) {
            addCriterion("tag_relation_id >", value, "tagRelationId");
            return (Criteria) this;
        }

        public Criteria andTagRelationIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("tag_relation_id >=", value, "tagRelationId");
            return (Criteria) this;
        }

        public Criteria andTagRelationIdLessThan(Integer value) {
            addCriterion("tag_relation_id <", value, "tagRelationId");
            return (Criteria) this;
        }

        public Criteria andTagRelationIdLessThanOrEqualTo(Integer value) {
            addCriterion("tag_relation_id <=", value, "tagRelationId");
            return (Criteria) this;
        }

        public Criteria andTagRelationIdIn(List<Integer> values) {
            addCriterion("tag_relation_id in", values, "tagRelationId");
            return (Criteria) this;
        }

        public Criteria andTagRelationIdNotIn(List<Integer> values) {
            addCriterion("tag_relation_id not in", values, "tagRelationId");
            return (Criteria) this;
        }

        public Criteria andTagRelationIdBetween(Integer value1, Integer value2) {
            addCriterion("tag_relation_id between", value1, value2, "tagRelationId");
            return (Criteria) this;
        }

        public Criteria andTagRelationIdNotBetween(Integer value1, Integer value2) {
            addCriterion("tag_relation_id not between", value1, value2, "tagRelationId");
            return (Criteria) this;
        }

        public Criteria andTagIdIsNull() {
            addCriterion("tag_id is null");
            return (Criteria) this;
        }

        public Criteria andTagIdIsNotNull() {
            addCriterion("tag_id is not null");
            return (Criteria) this;
        }

        public Criteria andTagIdEqualTo(Integer value) {
            addCriterion("tag_id =", value, "tagId");
            return (Criteria) this;
        }

        public Criteria andTagIdNotEqualTo(Integer value) {
            addCriterion("tag_id <>", value, "tagId");
            return (Criteria) this;
        }

        public Criteria andTagIdGreaterThan(Integer value) {
            addCriterion("tag_id >", value, "tagId");
            return (Criteria) this;
        }

        public Criteria andTagIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("tag_id >=", value, "tagId");
            return (Criteria) this;
        }

        public Criteria andTagIdLessThan(Integer value) {
            addCriterion("tag_id <", value, "tagId");
            return (Criteria) this;
        }

        public Criteria andTagIdLessThanOrEqualTo(Integer value) {
            addCriterion("tag_id <=", value, "tagId");
            return (Criteria) this;
        }

        public Criteria andTagIdIn(List<Integer> values) {
            addCriterion("tag_id in", values, "tagId");
            return (Criteria) this;
        }

        public Criteria andTagIdNotIn(List<Integer> values) {
            addCriterion("tag_id not in", values, "tagId");
            return (Criteria) this;
        }

        public Criteria andTagIdBetween(Integer value1, Integer value2) {
            addCriterion("tag_id between", value1, value2, "tagId");
            return (Criteria) this;
        }

        public Criteria andTagIdNotBetween(Integer value1, Integer value2) {
            addCriterion("tag_id not between", value1, value2, "tagId");
            return (Criteria) this;
        }

        public Criteria andResTypeIsNull() {
            addCriterion("res_type is null");
            return (Criteria) this;
        }

        public Criteria andResTypeIsNotNull() {
            addCriterion("res_type is not null");
            return (Criteria) this;
        }

        public Criteria andResTypeEqualTo(Integer value) {
            addCriterion("res_type =", value, "resType");
            return (Criteria) this;
        }

        public Criteria andResTypeNotEqualTo(Integer value) {
            addCriterion("res_type <>", value, "resType");
            return (Criteria) this;
        }

        public Criteria andResTypeGreaterThan(Integer value) {
            addCriterion("res_type >", value, "resType");
            return (Criteria) this;
        }

        public Criteria andResTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("res_type >=", value, "resType");
            return (Criteria) this;
        }

        public Criteria andResTypeLessThan(Integer value) {
            addCriterion("res_type <", value, "resType");
            return (Criteria) this;
        }

        public Criteria andResTypeLessThanOrEqualTo(Integer value) {
            addCriterion("res_type <=", value, "resType");
            return (Criteria) this;
        }

        public Criteria andResTypeIn(List<Integer> values) {
            addCriterion("res_type in", values, "resType");
            return (Criteria) this;
        }

        public Criteria andResTypeNotIn(List<Integer> values) {
            addCriterion("res_type not in", values, "resType");
            return (Criteria) this;
        }

        public Criteria andResTypeBetween(Integer value1, Integer value2) {
            addCriterion("res_type between", value1, value2, "resType");
            return (Criteria) this;
        }

        public Criteria andResTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("res_type not between", value1, value2, "resType");
            return (Criteria) this;
        }

        public Criteria andResRessetIdIsNull() {
            addCriterion("res_resset_id is null");
            return (Criteria) this;
        }

        public Criteria andResRessetIdIsNotNull() {
            addCriterion("res_resset_id is not null");
            return (Criteria) this;
        }

        public Criteria andResRessetIdEqualTo(Integer value) {
            addCriterion("res_resset_id =", value, "resRessetId");
            return (Criteria) this;
        }

        public Criteria andResRessetIdNotEqualTo(Integer value) {
            addCriterion("res_resset_id <>", value, "resRessetId");
            return (Criteria) this;
        }

        public Criteria andResRessetIdGreaterThan(Integer value) {
            addCriterion("res_resset_id >", value, "resRessetId");
            return (Criteria) this;
        }

        public Criteria andResRessetIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("res_resset_id >=", value, "resRessetId");
            return (Criteria) this;
        }

        public Criteria andResRessetIdLessThan(Integer value) {
            addCriterion("res_resset_id <", value, "resRessetId");
            return (Criteria) this;
        }

        public Criteria andResRessetIdLessThanOrEqualTo(Integer value) {
            addCriterion("res_resset_id <=", value, "resRessetId");
            return (Criteria) this;
        }

        public Criteria andResRessetIdIn(List<Integer> values) {
            addCriterion("res_resset_id in", values, "resRessetId");
            return (Criteria) this;
        }

        public Criteria andResRessetIdNotIn(List<Integer> values) {
            addCriterion("res_resset_id not in", values, "resRessetId");
            return (Criteria) this;
        }

        public Criteria andResRessetIdBetween(Integer value1, Integer value2) {
            addCriterion("res_resset_id between", value1, value2, "resRessetId");
            return (Criteria) this;
        }

        public Criteria andResRessetIdNotBetween(Integer value1, Integer value2) {
            addCriterion("res_resset_id not between", value1, value2, "resRessetId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * tag_relation 2019-01-13
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}