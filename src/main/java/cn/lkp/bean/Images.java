package cn.lkp.bean;

import javax.persistence.GeneratedValue;
import javax.persistence.Table;

import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Component;

/**
 * images
 * @author 杨孟新 2019-01-13
 */
@Table(name = "images")

@Component
public class Images {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer imageId;

    private String imageUuid;

    private String imageTitle;

    private String imageDescription;

    private String imageUrl;

    private Integer imageChecked;

    private Integer imagePublisher;

    private String imagePublishtime;

    private Integer imageChecker;

    private String imageChecktime;

    private String imageSrc;

    private Integer imageDeleted;

    private Integer imageAsynid;

    private Integer imageVisitCounter;

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public String getImageUuid() {
        return imageUuid;
    }

    public void setImageUuid(String imageUuid) {
        this.imageUuid = imageUuid == null ? null : imageUuid.trim();
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle == null ? null : imageTitle.trim();
    }

    public String getImageDescription() {
        return imageDescription;
    }

    public void setImageDescription(String imageDescription) {
        this.imageDescription = imageDescription == null ? null : imageDescription.trim();
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl == null ? null : imageUrl.trim();
    }

    public Integer getImageChecked() {
        return imageChecked;
    }

    public void setImageChecked(Integer imageChecked) {
        this.imageChecked = imageChecked;
    }

    public Integer getImagePublisher() {
        return imagePublisher;
    }

    public void setImagePublisher(Integer imagePublisher) {
        this.imagePublisher = imagePublisher;
    }

    public String getImagePublishtime() {
        return imagePublishtime;
    }

    public void setImagePublishtime(String imagePublishtime) {
        this.imagePublishtime = imagePublishtime == null ? null : imagePublishtime.trim();
    }

    public Integer getImageChecker() {
        return imageChecker;
    }

    public void setImageChecker(Integer imageChecker) {
        this.imageChecker = imageChecker;
    }

    public String getImageChecktime() {
        return imageChecktime;
    }

    public void setImageChecktime(String imageChecktime) {
        this.imageChecktime = imageChecktime == null ? null : imageChecktime.trim();
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc == null ? null : imageSrc.trim();
    }

    public Integer getImageDeleted() {
        return imageDeleted;
    }

    public void setImageDeleted(Integer imageDeleted) {
        this.imageDeleted = imageDeleted;
    }

    public Integer getImageAsynid() {
        return imageAsynid;
    }

    public void setImageAsynid(Integer imageAsynid) {
        this.imageAsynid = imageAsynid;
    }

    public Integer getImageVisitCounter() {
        return imageVisitCounter;
    }

    public void setImageVisitCounter(Integer imageVisitCounter) {
        this.imageVisitCounter = imageVisitCounter;
    }
}