package cn.lkp.bean;

import java.util.ArrayList;
import java.util.List;

public class CategorynodeExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CategorynodeExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * categorynode 2019-01-13
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCategoryIdIsNull() {
            addCriterion("category_id is null");
            return (Criteria) this;
        }

        public Criteria andCategoryIdIsNotNull() {
            addCriterion("category_id is not null");
            return (Criteria) this;
        }

        public Criteria andCategoryIdEqualTo(Integer value) {
            addCriterion("category_id =", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdNotEqualTo(Integer value) {
            addCriterion("category_id <>", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdGreaterThan(Integer value) {
            addCriterion("category_id >", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("category_id >=", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdLessThan(Integer value) {
            addCriterion("category_id <", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdLessThanOrEqualTo(Integer value) {
            addCriterion("category_id <=", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdIn(List<Integer> values) {
            addCriterion("category_id in", values, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdNotIn(List<Integer> values) {
            addCriterion("category_id not in", values, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdBetween(Integer value1, Integer value2) {
            addCriterion("category_id between", value1, value2, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdNotBetween(Integer value1, Integer value2) {
            addCriterion("category_id not between", value1, value2, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryParentIdIsNull() {
            addCriterion("category_parent_id is null");
            return (Criteria) this;
        }

        public Criteria andCategoryParentIdIsNotNull() {
            addCriterion("category_parent_id is not null");
            return (Criteria) this;
        }

        public Criteria andCategoryParentIdEqualTo(Integer value) {
            addCriterion("category_parent_id =", value, "categoryParentId");
            return (Criteria) this;
        }

        public Criteria andCategoryParentIdNotEqualTo(Integer value) {
            addCriterion("category_parent_id <>", value, "categoryParentId");
            return (Criteria) this;
        }

        public Criteria andCategoryParentIdGreaterThan(Integer value) {
            addCriterion("category_parent_id >", value, "categoryParentId");
            return (Criteria) this;
        }

        public Criteria andCategoryParentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("category_parent_id >=", value, "categoryParentId");
            return (Criteria) this;
        }

        public Criteria andCategoryParentIdLessThan(Integer value) {
            addCriterion("category_parent_id <", value, "categoryParentId");
            return (Criteria) this;
        }

        public Criteria andCategoryParentIdLessThanOrEqualTo(Integer value) {
            addCriterion("category_parent_id <=", value, "categoryParentId");
            return (Criteria) this;
        }

        public Criteria andCategoryParentIdIn(List<Integer> values) {
            addCriterion("category_parent_id in", values, "categoryParentId");
            return (Criteria) this;
        }

        public Criteria andCategoryParentIdNotIn(List<Integer> values) {
            addCriterion("category_parent_id not in", values, "categoryParentId");
            return (Criteria) this;
        }

        public Criteria andCategoryParentIdBetween(Integer value1, Integer value2) {
            addCriterion("category_parent_id between", value1, value2, "categoryParentId");
            return (Criteria) this;
        }

        public Criteria andCategoryParentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("category_parent_id not between", value1, value2, "categoryParentId");
            return (Criteria) this;
        }

        public Criteria andCategoryLogoPathIsNull() {
            addCriterion("category_logo_path is null");
            return (Criteria) this;
        }

        public Criteria andCategoryLogoPathIsNotNull() {
            addCriterion("category_logo_path is not null");
            return (Criteria) this;
        }

        public Criteria andCategoryLogoPathEqualTo(String value) {
            addCriterion("category_logo_path =", value, "categoryLogoPath");
            return (Criteria) this;
        }

        public Criteria andCategoryLogoPathNotEqualTo(String value) {
            addCriterion("category_logo_path <>", value, "categoryLogoPath");
            return (Criteria) this;
        }

        public Criteria andCategoryLogoPathGreaterThan(String value) {
            addCriterion("category_logo_path >", value, "categoryLogoPath");
            return (Criteria) this;
        }

        public Criteria andCategoryLogoPathGreaterThanOrEqualTo(String value) {
            addCriterion("category_logo_path >=", value, "categoryLogoPath");
            return (Criteria) this;
        }

        public Criteria andCategoryLogoPathLessThan(String value) {
            addCriterion("category_logo_path <", value, "categoryLogoPath");
            return (Criteria) this;
        }

        public Criteria andCategoryLogoPathLessThanOrEqualTo(String value) {
            addCriterion("category_logo_path <=", value, "categoryLogoPath");
            return (Criteria) this;
        }

        public Criteria andCategoryLogoPathLike(String value) {
            addCriterion("category_logo_path like", value, "categoryLogoPath");
            return (Criteria) this;
        }

        public Criteria andCategoryLogoPathNotLike(String value) {
            addCriterion("category_logo_path not like", value, "categoryLogoPath");
            return (Criteria) this;
        }

        public Criteria andCategoryLogoPathIn(List<String> values) {
            addCriterion("category_logo_path in", values, "categoryLogoPath");
            return (Criteria) this;
        }

        public Criteria andCategoryLogoPathNotIn(List<String> values) {
            addCriterion("category_logo_path not in", values, "categoryLogoPath");
            return (Criteria) this;
        }

        public Criteria andCategoryLogoPathBetween(String value1, String value2) {
            addCriterion("category_logo_path between", value1, value2, "categoryLogoPath");
            return (Criteria) this;
        }

        public Criteria andCategoryLogoPathNotBetween(String value1, String value2) {
            addCriterion("category_logo_path not between", value1, value2, "categoryLogoPath");
            return (Criteria) this;
        }

        public Criteria andCategoriyUrlIsNull() {
            addCriterion("categoriy_url is null");
            return (Criteria) this;
        }

        public Criteria andCategoriyUrlIsNotNull() {
            addCriterion("categoriy_url is not null");
            return (Criteria) this;
        }

        public Criteria andCategoriyUrlEqualTo(String value) {
            addCriterion("categoriy_url =", value, "categoriyUrl");
            return (Criteria) this;
        }

        public Criteria andCategoriyUrlNotEqualTo(String value) {
            addCriterion("categoriy_url <>", value, "categoriyUrl");
            return (Criteria) this;
        }

        public Criteria andCategoriyUrlGreaterThan(String value) {
            addCriterion("categoriy_url >", value, "categoriyUrl");
            return (Criteria) this;
        }

        public Criteria andCategoriyUrlGreaterThanOrEqualTo(String value) {
            addCriterion("categoriy_url >=", value, "categoriyUrl");
            return (Criteria) this;
        }

        public Criteria andCategoriyUrlLessThan(String value) {
            addCriterion("categoriy_url <", value, "categoriyUrl");
            return (Criteria) this;
        }

        public Criteria andCategoriyUrlLessThanOrEqualTo(String value) {
            addCriterion("categoriy_url <=", value, "categoriyUrl");
            return (Criteria) this;
        }

        public Criteria andCategoriyUrlLike(String value) {
            addCriterion("categoriy_url like", value, "categoriyUrl");
            return (Criteria) this;
        }

        public Criteria andCategoriyUrlNotLike(String value) {
            addCriterion("categoriy_url not like", value, "categoriyUrl");
            return (Criteria) this;
        }

        public Criteria andCategoriyUrlIn(List<String> values) {
            addCriterion("categoriy_url in", values, "categoriyUrl");
            return (Criteria) this;
        }

        public Criteria andCategoriyUrlNotIn(List<String> values) {
            addCriterion("categoriy_url not in", values, "categoriyUrl");
            return (Criteria) this;
        }

        public Criteria andCategoriyUrlBetween(String value1, String value2) {
            addCriterion("categoriy_url between", value1, value2, "categoriyUrl");
            return (Criteria) this;
        }

        public Criteria andCategoriyUrlNotBetween(String value1, String value2) {
            addCriterion("categoriy_url not between", value1, value2, "categoriyUrl");
            return (Criteria) this;
        }

        public Criteria andCategoryCheckedIsNull() {
            addCriterion("category_checked is null");
            return (Criteria) this;
        }

        public Criteria andCategoryCheckedIsNotNull() {
            addCriterion("category_checked is not null");
            return (Criteria) this;
        }

        public Criteria andCategoryCheckedEqualTo(Integer value) {
            addCriterion("category_checked =", value, "categoryChecked");
            return (Criteria) this;
        }

        public Criteria andCategoryCheckedNotEqualTo(Integer value) {
            addCriterion("category_checked <>", value, "categoryChecked");
            return (Criteria) this;
        }

        public Criteria andCategoryCheckedGreaterThan(Integer value) {
            addCriterion("category_checked >", value, "categoryChecked");
            return (Criteria) this;
        }

        public Criteria andCategoryCheckedGreaterThanOrEqualTo(Integer value) {
            addCriterion("category_checked >=", value, "categoryChecked");
            return (Criteria) this;
        }

        public Criteria andCategoryCheckedLessThan(Integer value) {
            addCriterion("category_checked <", value, "categoryChecked");
            return (Criteria) this;
        }

        public Criteria andCategoryCheckedLessThanOrEqualTo(Integer value) {
            addCriterion("category_checked <=", value, "categoryChecked");
            return (Criteria) this;
        }

        public Criteria andCategoryCheckedIn(List<Integer> values) {
            addCriterion("category_checked in", values, "categoryChecked");
            return (Criteria) this;
        }

        public Criteria andCategoryCheckedNotIn(List<Integer> values) {
            addCriterion("category_checked not in", values, "categoryChecked");
            return (Criteria) this;
        }

        public Criteria andCategoryCheckedBetween(Integer value1, Integer value2) {
            addCriterion("category_checked between", value1, value2, "categoryChecked");
            return (Criteria) this;
        }

        public Criteria andCategoryCheckedNotBetween(Integer value1, Integer value2) {
            addCriterion("category_checked not between", value1, value2, "categoryChecked");
            return (Criteria) this;
        }

        public Criteria andCategoryCommentIsNull() {
            addCriterion("category_comment is null");
            return (Criteria) this;
        }

        public Criteria andCategoryCommentIsNotNull() {
            addCriterion("category_comment is not null");
            return (Criteria) this;
        }

        public Criteria andCategoryCommentEqualTo(String value) {
            addCriterion("category_comment =", value, "categoryComment");
            return (Criteria) this;
        }

        public Criteria andCategoryCommentNotEqualTo(String value) {
            addCriterion("category_comment <>", value, "categoryComment");
            return (Criteria) this;
        }

        public Criteria andCategoryCommentGreaterThan(String value) {
            addCriterion("category_comment >", value, "categoryComment");
            return (Criteria) this;
        }

        public Criteria andCategoryCommentGreaterThanOrEqualTo(String value) {
            addCriterion("category_comment >=", value, "categoryComment");
            return (Criteria) this;
        }

        public Criteria andCategoryCommentLessThan(String value) {
            addCriterion("category_comment <", value, "categoryComment");
            return (Criteria) this;
        }

        public Criteria andCategoryCommentLessThanOrEqualTo(String value) {
            addCriterion("category_comment <=", value, "categoryComment");
            return (Criteria) this;
        }

        public Criteria andCategoryCommentLike(String value) {
            addCriterion("category_comment like", value, "categoryComment");
            return (Criteria) this;
        }

        public Criteria andCategoryCommentNotLike(String value) {
            addCriterion("category_comment not like", value, "categoryComment");
            return (Criteria) this;
        }

        public Criteria andCategoryCommentIn(List<String> values) {
            addCriterion("category_comment in", values, "categoryComment");
            return (Criteria) this;
        }

        public Criteria andCategoryCommentNotIn(List<String> values) {
            addCriterion("category_comment not in", values, "categoryComment");
            return (Criteria) this;
        }

        public Criteria andCategoryCommentBetween(String value1, String value2) {
            addCriterion("category_comment between", value1, value2, "categoryComment");
            return (Criteria) this;
        }

        public Criteria andCategoryCommentNotBetween(String value1, String value2) {
            addCriterion("category_comment not between", value1, value2, "categoryComment");
            return (Criteria) this;
        }

        public Criteria andCategoriyDescriptionIsNull() {
            addCriterion("categoriy_description is null");
            return (Criteria) this;
        }

        public Criteria andCategoriyDescriptionIsNotNull() {
            addCriterion("categoriy_description is not null");
            return (Criteria) this;
        }

        public Criteria andCategoriyDescriptionEqualTo(String value) {
            addCriterion("categoriy_description =", value, "categoriyDescription");
            return (Criteria) this;
        }

        public Criteria andCategoriyDescriptionNotEqualTo(String value) {
            addCriterion("categoriy_description <>", value, "categoriyDescription");
            return (Criteria) this;
        }

        public Criteria andCategoriyDescriptionGreaterThan(String value) {
            addCriterion("categoriy_description >", value, "categoriyDescription");
            return (Criteria) this;
        }

        public Criteria andCategoriyDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("categoriy_description >=", value, "categoriyDescription");
            return (Criteria) this;
        }

        public Criteria andCategoriyDescriptionLessThan(String value) {
            addCriterion("categoriy_description <", value, "categoriyDescription");
            return (Criteria) this;
        }

        public Criteria andCategoriyDescriptionLessThanOrEqualTo(String value) {
            addCriterion("categoriy_description <=", value, "categoriyDescription");
            return (Criteria) this;
        }

        public Criteria andCategoriyDescriptionLike(String value) {
            addCriterion("categoriy_description like", value, "categoriyDescription");
            return (Criteria) this;
        }

        public Criteria andCategoriyDescriptionNotLike(String value) {
            addCriterion("categoriy_description not like", value, "categoriyDescription");
            return (Criteria) this;
        }

        public Criteria andCategoriyDescriptionIn(List<String> values) {
            addCriterion("categoriy_description in", values, "categoriyDescription");
            return (Criteria) this;
        }

        public Criteria andCategoriyDescriptionNotIn(List<String> values) {
            addCriterion("categoriy_description not in", values, "categoriyDescription");
            return (Criteria) this;
        }

        public Criteria andCategoriyDescriptionBetween(String value1, String value2) {
            addCriterion("categoriy_description between", value1, value2, "categoriyDescription");
            return (Criteria) this;
        }

        public Criteria andCategoriyDescriptionNotBetween(String value1, String value2) {
            addCriterion("categoriy_description not between", value1, value2, "categoriyDescription");
            return (Criteria) this;
        }

        public Criteria andCategorieyRessetIdIsNull() {
            addCriterion("categoriey_resset_id is null");
            return (Criteria) this;
        }

        public Criteria andCategorieyRessetIdIsNotNull() {
            addCriterion("categoriey_resset_id is not null");
            return (Criteria) this;
        }

        public Criteria andCategorieyRessetIdEqualTo(Integer value) {
            addCriterion("categoriey_resset_id =", value, "categorieyRessetId");
            return (Criteria) this;
        }

        public Criteria andCategorieyRessetIdNotEqualTo(Integer value) {
            addCriterion("categoriey_resset_id <>", value, "categorieyRessetId");
            return (Criteria) this;
        }

        public Criteria andCategorieyRessetIdGreaterThan(Integer value) {
            addCriterion("categoriey_resset_id >", value, "categorieyRessetId");
            return (Criteria) this;
        }

        public Criteria andCategorieyRessetIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("categoriey_resset_id >=", value, "categorieyRessetId");
            return (Criteria) this;
        }

        public Criteria andCategorieyRessetIdLessThan(Integer value) {
            addCriterion("categoriey_resset_id <", value, "categorieyRessetId");
            return (Criteria) this;
        }

        public Criteria andCategorieyRessetIdLessThanOrEqualTo(Integer value) {
            addCriterion("categoriey_resset_id <=", value, "categorieyRessetId");
            return (Criteria) this;
        }

        public Criteria andCategorieyRessetIdIn(List<Integer> values) {
            addCriterion("categoriey_resset_id in", values, "categorieyRessetId");
            return (Criteria) this;
        }

        public Criteria andCategorieyRessetIdNotIn(List<Integer> values) {
            addCriterion("categoriey_resset_id not in", values, "categorieyRessetId");
            return (Criteria) this;
        }

        public Criteria andCategorieyRessetIdBetween(Integer value1, Integer value2) {
            addCriterion("categoriey_resset_id between", value1, value2, "categorieyRessetId");
            return (Criteria) this;
        }

        public Criteria andCategorieyRessetIdNotBetween(Integer value1, Integer value2) {
            addCriterion("categoriey_resset_id not between", value1, value2, "categorieyRessetId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * categorynode 2019-01-13
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}