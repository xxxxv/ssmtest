package cn.lkp.bean;

import java.util.ArrayList;
import java.util.List;

public class AttachmentExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AttachmentExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * attachment 2019-01-13
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andAttachmentIdIsNull() {
            addCriterion("attachment_id is null");
            return (Criteria) this;
        }

        public Criteria andAttachmentIdIsNotNull() {
            addCriterion("attachment_id is not null");
            return (Criteria) this;
        }

        public Criteria andAttachmentIdEqualTo(Integer value) {
            addCriterion("attachment_id =", value, "attachmentId");
            return (Criteria) this;
        }

        public Criteria andAttachmentIdNotEqualTo(Integer value) {
            addCriterion("attachment_id <>", value, "attachmentId");
            return (Criteria) this;
        }

        public Criteria andAttachmentIdGreaterThan(Integer value) {
            addCriterion("attachment_id >", value, "attachmentId");
            return (Criteria) this;
        }

        public Criteria andAttachmentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("attachment_id >=", value, "attachmentId");
            return (Criteria) this;
        }

        public Criteria andAttachmentIdLessThan(Integer value) {
            addCriterion("attachment_id <", value, "attachmentId");
            return (Criteria) this;
        }

        public Criteria andAttachmentIdLessThanOrEqualTo(Integer value) {
            addCriterion("attachment_id <=", value, "attachmentId");
            return (Criteria) this;
        }

        public Criteria andAttachmentIdIn(List<Integer> values) {
            addCriterion("attachment_id in", values, "attachmentId");
            return (Criteria) this;
        }

        public Criteria andAttachmentIdNotIn(List<Integer> values) {
            addCriterion("attachment_id not in", values, "attachmentId");
            return (Criteria) this;
        }

        public Criteria andAttachmentIdBetween(Integer value1, Integer value2) {
            addCriterion("attachment_id between", value1, value2, "attachmentId");
            return (Criteria) this;
        }

        public Criteria andAttachmentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("attachment_id not between", value1, value2, "attachmentId");
            return (Criteria) this;
        }

        public Criteria andAttachmentUuidIsNull() {
            addCriterion("attachment_uuid is null");
            return (Criteria) this;
        }

        public Criteria andAttachmentUuidIsNotNull() {
            addCriterion("attachment_uuid is not null");
            return (Criteria) this;
        }

        public Criteria andAttachmentUuidEqualTo(String value) {
            addCriterion("attachment_uuid =", value, "attachmentUuid");
            return (Criteria) this;
        }

        public Criteria andAttachmentUuidNotEqualTo(String value) {
            addCriterion("attachment_uuid <>", value, "attachmentUuid");
            return (Criteria) this;
        }

        public Criteria andAttachmentUuidGreaterThan(String value) {
            addCriterion("attachment_uuid >", value, "attachmentUuid");
            return (Criteria) this;
        }

        public Criteria andAttachmentUuidGreaterThanOrEqualTo(String value) {
            addCriterion("attachment_uuid >=", value, "attachmentUuid");
            return (Criteria) this;
        }

        public Criteria andAttachmentUuidLessThan(String value) {
            addCriterion("attachment_uuid <", value, "attachmentUuid");
            return (Criteria) this;
        }

        public Criteria andAttachmentUuidLessThanOrEqualTo(String value) {
            addCriterion("attachment_uuid <=", value, "attachmentUuid");
            return (Criteria) this;
        }

        public Criteria andAttachmentUuidLike(String value) {
            addCriterion("attachment_uuid like", value, "attachmentUuid");
            return (Criteria) this;
        }

        public Criteria andAttachmentUuidNotLike(String value) {
            addCriterion("attachment_uuid not like", value, "attachmentUuid");
            return (Criteria) this;
        }

        public Criteria andAttachmentUuidIn(List<String> values) {
            addCriterion("attachment_uuid in", values, "attachmentUuid");
            return (Criteria) this;
        }

        public Criteria andAttachmentUuidNotIn(List<String> values) {
            addCriterion("attachment_uuid not in", values, "attachmentUuid");
            return (Criteria) this;
        }

        public Criteria andAttachmentUuidBetween(String value1, String value2) {
            addCriterion("attachment_uuid between", value1, value2, "attachmentUuid");
            return (Criteria) this;
        }

        public Criteria andAttachmentUuidNotBetween(String value1, String value2) {
            addCriterion("attachment_uuid not between", value1, value2, "attachmentUuid");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublisherIsNull() {
            addCriterion("attachment_publisher is null");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublisherIsNotNull() {
            addCriterion("attachment_publisher is not null");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublisherEqualTo(Integer value) {
            addCriterion("attachment_publisher =", value, "attachmentPublisher");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublisherNotEqualTo(Integer value) {
            addCriterion("attachment_publisher <>", value, "attachmentPublisher");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublisherGreaterThan(Integer value) {
            addCriterion("attachment_publisher >", value, "attachmentPublisher");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublisherGreaterThanOrEqualTo(Integer value) {
            addCriterion("attachment_publisher >=", value, "attachmentPublisher");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublisherLessThan(Integer value) {
            addCriterion("attachment_publisher <", value, "attachmentPublisher");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublisherLessThanOrEqualTo(Integer value) {
            addCriterion("attachment_publisher <=", value, "attachmentPublisher");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublisherIn(List<Integer> values) {
            addCriterion("attachment_publisher in", values, "attachmentPublisher");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublisherNotIn(List<Integer> values) {
            addCriterion("attachment_publisher not in", values, "attachmentPublisher");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublisherBetween(Integer value1, Integer value2) {
            addCriterion("attachment_publisher between", value1, value2, "attachmentPublisher");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublisherNotBetween(Integer value1, Integer value2) {
            addCriterion("attachment_publisher not between", value1, value2, "attachmentPublisher");
            return (Criteria) this;
        }

        public Criteria andAttachmentTitleIsNull() {
            addCriterion("attachment_title is null");
            return (Criteria) this;
        }

        public Criteria andAttachmentTitleIsNotNull() {
            addCriterion("attachment_title is not null");
            return (Criteria) this;
        }

        public Criteria andAttachmentTitleEqualTo(String value) {
            addCriterion("attachment_title =", value, "attachmentTitle");
            return (Criteria) this;
        }

        public Criteria andAttachmentTitleNotEqualTo(String value) {
            addCriterion("attachment_title <>", value, "attachmentTitle");
            return (Criteria) this;
        }

        public Criteria andAttachmentTitleGreaterThan(String value) {
            addCriterion("attachment_title >", value, "attachmentTitle");
            return (Criteria) this;
        }

        public Criteria andAttachmentTitleGreaterThanOrEqualTo(String value) {
            addCriterion("attachment_title >=", value, "attachmentTitle");
            return (Criteria) this;
        }

        public Criteria andAttachmentTitleLessThan(String value) {
            addCriterion("attachment_title <", value, "attachmentTitle");
            return (Criteria) this;
        }

        public Criteria andAttachmentTitleLessThanOrEqualTo(String value) {
            addCriterion("attachment_title <=", value, "attachmentTitle");
            return (Criteria) this;
        }

        public Criteria andAttachmentTitleLike(String value) {
            addCriterion("attachment_title like", value, "attachmentTitle");
            return (Criteria) this;
        }

        public Criteria andAttachmentTitleNotLike(String value) {
            addCriterion("attachment_title not like", value, "attachmentTitle");
            return (Criteria) this;
        }

        public Criteria andAttachmentTitleIn(List<String> values) {
            addCriterion("attachment_title in", values, "attachmentTitle");
            return (Criteria) this;
        }

        public Criteria andAttachmentTitleNotIn(List<String> values) {
            addCriterion("attachment_title not in", values, "attachmentTitle");
            return (Criteria) this;
        }

        public Criteria andAttachmentTitleBetween(String value1, String value2) {
            addCriterion("attachment_title between", value1, value2, "attachmentTitle");
            return (Criteria) this;
        }

        public Criteria andAttachmentTitleNotBetween(String value1, String value2) {
            addCriterion("attachment_title not between", value1, value2, "attachmentTitle");
            return (Criteria) this;
        }

        public Criteria andAttachmentNameIsNull() {
            addCriterion("attachment_name is null");
            return (Criteria) this;
        }

        public Criteria andAttachmentNameIsNotNull() {
            addCriterion("attachment_name is not null");
            return (Criteria) this;
        }

        public Criteria andAttachmentNameEqualTo(String value) {
            addCriterion("attachment_name =", value, "attachmentName");
            return (Criteria) this;
        }

        public Criteria andAttachmentNameNotEqualTo(String value) {
            addCriterion("attachment_name <>", value, "attachmentName");
            return (Criteria) this;
        }

        public Criteria andAttachmentNameGreaterThan(String value) {
            addCriterion("attachment_name >", value, "attachmentName");
            return (Criteria) this;
        }

        public Criteria andAttachmentNameGreaterThanOrEqualTo(String value) {
            addCriterion("attachment_name >=", value, "attachmentName");
            return (Criteria) this;
        }

        public Criteria andAttachmentNameLessThan(String value) {
            addCriterion("attachment_name <", value, "attachmentName");
            return (Criteria) this;
        }

        public Criteria andAttachmentNameLessThanOrEqualTo(String value) {
            addCriterion("attachment_name <=", value, "attachmentName");
            return (Criteria) this;
        }

        public Criteria andAttachmentNameLike(String value) {
            addCriterion("attachment_name like", value, "attachmentName");
            return (Criteria) this;
        }

        public Criteria andAttachmentNameNotLike(String value) {
            addCriterion("attachment_name not like", value, "attachmentName");
            return (Criteria) this;
        }

        public Criteria andAttachmentNameIn(List<String> values) {
            addCriterion("attachment_name in", values, "attachmentName");
            return (Criteria) this;
        }

        public Criteria andAttachmentNameNotIn(List<String> values) {
            addCriterion("attachment_name not in", values, "attachmentName");
            return (Criteria) this;
        }

        public Criteria andAttachmentNameBetween(String value1, String value2) {
            addCriterion("attachment_name between", value1, value2, "attachmentName");
            return (Criteria) this;
        }

        public Criteria andAttachmentNameNotBetween(String value1, String value2) {
            addCriterion("attachment_name not between", value1, value2, "attachmentName");
            return (Criteria) this;
        }

        public Criteria andAttachmentSrcIsNull() {
            addCriterion("attachment_src is null");
            return (Criteria) this;
        }

        public Criteria andAttachmentSrcIsNotNull() {
            addCriterion("attachment_src is not null");
            return (Criteria) this;
        }

        public Criteria andAttachmentSrcEqualTo(String value) {
            addCriterion("attachment_src =", value, "attachmentSrc");
            return (Criteria) this;
        }

        public Criteria andAttachmentSrcNotEqualTo(String value) {
            addCriterion("attachment_src <>", value, "attachmentSrc");
            return (Criteria) this;
        }

        public Criteria andAttachmentSrcGreaterThan(String value) {
            addCriterion("attachment_src >", value, "attachmentSrc");
            return (Criteria) this;
        }

        public Criteria andAttachmentSrcGreaterThanOrEqualTo(String value) {
            addCriterion("attachment_src >=", value, "attachmentSrc");
            return (Criteria) this;
        }

        public Criteria andAttachmentSrcLessThan(String value) {
            addCriterion("attachment_src <", value, "attachmentSrc");
            return (Criteria) this;
        }

        public Criteria andAttachmentSrcLessThanOrEqualTo(String value) {
            addCriterion("attachment_src <=", value, "attachmentSrc");
            return (Criteria) this;
        }

        public Criteria andAttachmentSrcLike(String value) {
            addCriterion("attachment_src like", value, "attachmentSrc");
            return (Criteria) this;
        }

        public Criteria andAttachmentSrcNotLike(String value) {
            addCriterion("attachment_src not like", value, "attachmentSrc");
            return (Criteria) this;
        }

        public Criteria andAttachmentSrcIn(List<String> values) {
            addCriterion("attachment_src in", values, "attachmentSrc");
            return (Criteria) this;
        }

        public Criteria andAttachmentSrcNotIn(List<String> values) {
            addCriterion("attachment_src not in", values, "attachmentSrc");
            return (Criteria) this;
        }

        public Criteria andAttachmentSrcBetween(String value1, String value2) {
            addCriterion("attachment_src between", value1, value2, "attachmentSrc");
            return (Criteria) this;
        }

        public Criteria andAttachmentSrcNotBetween(String value1, String value2) {
            addCriterion("attachment_src not between", value1, value2, "attachmentSrc");
            return (Criteria) this;
        }

        public Criteria andAttachmentDescriptionIsNull() {
            addCriterion("attachment_description is null");
            return (Criteria) this;
        }

        public Criteria andAttachmentDescriptionIsNotNull() {
            addCriterion("attachment_description is not null");
            return (Criteria) this;
        }

        public Criteria andAttachmentDescriptionEqualTo(String value) {
            addCriterion("attachment_description =", value, "attachmentDescription");
            return (Criteria) this;
        }

        public Criteria andAttachmentDescriptionNotEqualTo(String value) {
            addCriterion("attachment_description <>", value, "attachmentDescription");
            return (Criteria) this;
        }

        public Criteria andAttachmentDescriptionGreaterThan(String value) {
            addCriterion("attachment_description >", value, "attachmentDescription");
            return (Criteria) this;
        }

        public Criteria andAttachmentDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("attachment_description >=", value, "attachmentDescription");
            return (Criteria) this;
        }

        public Criteria andAttachmentDescriptionLessThan(String value) {
            addCriterion("attachment_description <", value, "attachmentDescription");
            return (Criteria) this;
        }

        public Criteria andAttachmentDescriptionLessThanOrEqualTo(String value) {
            addCriterion("attachment_description <=", value, "attachmentDescription");
            return (Criteria) this;
        }

        public Criteria andAttachmentDescriptionLike(String value) {
            addCriterion("attachment_description like", value, "attachmentDescription");
            return (Criteria) this;
        }

        public Criteria andAttachmentDescriptionNotLike(String value) {
            addCriterion("attachment_description not like", value, "attachmentDescription");
            return (Criteria) this;
        }

        public Criteria andAttachmentDescriptionIn(List<String> values) {
            addCriterion("attachment_description in", values, "attachmentDescription");
            return (Criteria) this;
        }

        public Criteria andAttachmentDescriptionNotIn(List<String> values) {
            addCriterion("attachment_description not in", values, "attachmentDescription");
            return (Criteria) this;
        }

        public Criteria andAttachmentDescriptionBetween(String value1, String value2) {
            addCriterion("attachment_description between", value1, value2, "attachmentDescription");
            return (Criteria) this;
        }

        public Criteria andAttachmentDescriptionNotBetween(String value1, String value2) {
            addCriterion("attachment_description not between", value1, value2, "attachmentDescription");
            return (Criteria) this;
        }

        public Criteria andAttachmentTypeIsNull() {
            addCriterion("attachment_type is null");
            return (Criteria) this;
        }

        public Criteria andAttachmentTypeIsNotNull() {
            addCriterion("attachment_type is not null");
            return (Criteria) this;
        }

        public Criteria andAttachmentTypeEqualTo(String value) {
            addCriterion("attachment_type =", value, "attachmentType");
            return (Criteria) this;
        }

        public Criteria andAttachmentTypeNotEqualTo(String value) {
            addCriterion("attachment_type <>", value, "attachmentType");
            return (Criteria) this;
        }

        public Criteria andAttachmentTypeGreaterThan(String value) {
            addCriterion("attachment_type >", value, "attachmentType");
            return (Criteria) this;
        }

        public Criteria andAttachmentTypeGreaterThanOrEqualTo(String value) {
            addCriterion("attachment_type >=", value, "attachmentType");
            return (Criteria) this;
        }

        public Criteria andAttachmentTypeLessThan(String value) {
            addCriterion("attachment_type <", value, "attachmentType");
            return (Criteria) this;
        }

        public Criteria andAttachmentTypeLessThanOrEqualTo(String value) {
            addCriterion("attachment_type <=", value, "attachmentType");
            return (Criteria) this;
        }

        public Criteria andAttachmentTypeLike(String value) {
            addCriterion("attachment_type like", value, "attachmentType");
            return (Criteria) this;
        }

        public Criteria andAttachmentTypeNotLike(String value) {
            addCriterion("attachment_type not like", value, "attachmentType");
            return (Criteria) this;
        }

        public Criteria andAttachmentTypeIn(List<String> values) {
            addCriterion("attachment_type in", values, "attachmentType");
            return (Criteria) this;
        }

        public Criteria andAttachmentTypeNotIn(List<String> values) {
            addCriterion("attachment_type not in", values, "attachmentType");
            return (Criteria) this;
        }

        public Criteria andAttachmentTypeBetween(String value1, String value2) {
            addCriterion("attachment_type between", value1, value2, "attachmentType");
            return (Criteria) this;
        }

        public Criteria andAttachmentTypeNotBetween(String value1, String value2) {
            addCriterion("attachment_type not between", value1, value2, "attachmentType");
            return (Criteria) this;
        }

        public Criteria andAttachmentSizeIsNull() {
            addCriterion("attachment_size is null");
            return (Criteria) this;
        }

        public Criteria andAttachmentSizeIsNotNull() {
            addCriterion("attachment_size is not null");
            return (Criteria) this;
        }

        public Criteria andAttachmentSizeEqualTo(String value) {
            addCriterion("attachment_size =", value, "attachmentSize");
            return (Criteria) this;
        }

        public Criteria andAttachmentSizeNotEqualTo(String value) {
            addCriterion("attachment_size <>", value, "attachmentSize");
            return (Criteria) this;
        }

        public Criteria andAttachmentSizeGreaterThan(String value) {
            addCriterion("attachment_size >", value, "attachmentSize");
            return (Criteria) this;
        }

        public Criteria andAttachmentSizeGreaterThanOrEqualTo(String value) {
            addCriterion("attachment_size >=", value, "attachmentSize");
            return (Criteria) this;
        }

        public Criteria andAttachmentSizeLessThan(String value) {
            addCriterion("attachment_size <", value, "attachmentSize");
            return (Criteria) this;
        }

        public Criteria andAttachmentSizeLessThanOrEqualTo(String value) {
            addCriterion("attachment_size <=", value, "attachmentSize");
            return (Criteria) this;
        }

        public Criteria andAttachmentSizeLike(String value) {
            addCriterion("attachment_size like", value, "attachmentSize");
            return (Criteria) this;
        }

        public Criteria andAttachmentSizeNotLike(String value) {
            addCriterion("attachment_size not like", value, "attachmentSize");
            return (Criteria) this;
        }

        public Criteria andAttachmentSizeIn(List<String> values) {
            addCriterion("attachment_size in", values, "attachmentSize");
            return (Criteria) this;
        }

        public Criteria andAttachmentSizeNotIn(List<String> values) {
            addCriterion("attachment_size not in", values, "attachmentSize");
            return (Criteria) this;
        }

        public Criteria andAttachmentSizeBetween(String value1, String value2) {
            addCriterion("attachment_size between", value1, value2, "attachmentSize");
            return (Criteria) this;
        }

        public Criteria andAttachmentSizeNotBetween(String value1, String value2) {
            addCriterion("attachment_size not between", value1, value2, "attachmentSize");
            return (Criteria) this;
        }

        public Criteria andAttachmentDownloadCounterIsNull() {
            addCriterion("attachment_download_counter is null");
            return (Criteria) this;
        }

        public Criteria andAttachmentDownloadCounterIsNotNull() {
            addCriterion("attachment_download_counter is not null");
            return (Criteria) this;
        }

        public Criteria andAttachmentDownloadCounterEqualTo(Integer value) {
            addCriterion("attachment_download_counter =", value, "attachmentDownloadCounter");
            return (Criteria) this;
        }

        public Criteria andAttachmentDownloadCounterNotEqualTo(Integer value) {
            addCriterion("attachment_download_counter <>", value, "attachmentDownloadCounter");
            return (Criteria) this;
        }

        public Criteria andAttachmentDownloadCounterGreaterThan(Integer value) {
            addCriterion("attachment_download_counter >", value, "attachmentDownloadCounter");
            return (Criteria) this;
        }

        public Criteria andAttachmentDownloadCounterGreaterThanOrEqualTo(Integer value) {
            addCriterion("attachment_download_counter >=", value, "attachmentDownloadCounter");
            return (Criteria) this;
        }

        public Criteria andAttachmentDownloadCounterLessThan(Integer value) {
            addCriterion("attachment_download_counter <", value, "attachmentDownloadCounter");
            return (Criteria) this;
        }

        public Criteria andAttachmentDownloadCounterLessThanOrEqualTo(Integer value) {
            addCriterion("attachment_download_counter <=", value, "attachmentDownloadCounter");
            return (Criteria) this;
        }

        public Criteria andAttachmentDownloadCounterIn(List<Integer> values) {
            addCriterion("attachment_download_counter in", values, "attachmentDownloadCounter");
            return (Criteria) this;
        }

        public Criteria andAttachmentDownloadCounterNotIn(List<Integer> values) {
            addCriterion("attachment_download_counter not in", values, "attachmentDownloadCounter");
            return (Criteria) this;
        }

        public Criteria andAttachmentDownloadCounterBetween(Integer value1, Integer value2) {
            addCriterion("attachment_download_counter between", value1, value2, "attachmentDownloadCounter");
            return (Criteria) this;
        }

        public Criteria andAttachmentDownloadCounterNotBetween(Integer value1, Integer value2) {
            addCriterion("attachment_download_counter not between", value1, value2, "attachmentDownloadCounter");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckedIsNull() {
            addCriterion("attachment_checked is null");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckedIsNotNull() {
            addCriterion("attachment_checked is not null");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckedEqualTo(Integer value) {
            addCriterion("attachment_checked =", value, "attachmentChecked");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckedNotEqualTo(Integer value) {
            addCriterion("attachment_checked <>", value, "attachmentChecked");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckedGreaterThan(Integer value) {
            addCriterion("attachment_checked >", value, "attachmentChecked");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckedGreaterThanOrEqualTo(Integer value) {
            addCriterion("attachment_checked >=", value, "attachmentChecked");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckedLessThan(Integer value) {
            addCriterion("attachment_checked <", value, "attachmentChecked");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckedLessThanOrEqualTo(Integer value) {
            addCriterion("attachment_checked <=", value, "attachmentChecked");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckedIn(List<Integer> values) {
            addCriterion("attachment_checked in", values, "attachmentChecked");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckedNotIn(List<Integer> values) {
            addCriterion("attachment_checked not in", values, "attachmentChecked");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckedBetween(Integer value1, Integer value2) {
            addCriterion("attachment_checked between", value1, value2, "attachmentChecked");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckedNotBetween(Integer value1, Integer value2) {
            addCriterion("attachment_checked not between", value1, value2, "attachmentChecked");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckerIsNull() {
            addCriterion("attachment_checker is null");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckerIsNotNull() {
            addCriterion("attachment_checker is not null");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckerEqualTo(Integer value) {
            addCriterion("attachment_checker =", value, "attachmentChecker");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckerNotEqualTo(Integer value) {
            addCriterion("attachment_checker <>", value, "attachmentChecker");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckerGreaterThan(Integer value) {
            addCriterion("attachment_checker >", value, "attachmentChecker");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckerGreaterThanOrEqualTo(Integer value) {
            addCriterion("attachment_checker >=", value, "attachmentChecker");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckerLessThan(Integer value) {
            addCriterion("attachment_checker <", value, "attachmentChecker");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckerLessThanOrEqualTo(Integer value) {
            addCriterion("attachment_checker <=", value, "attachmentChecker");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckerIn(List<Integer> values) {
            addCriterion("attachment_checker in", values, "attachmentChecker");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckerNotIn(List<Integer> values) {
            addCriterion("attachment_checker not in", values, "attachmentChecker");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckerBetween(Integer value1, Integer value2) {
            addCriterion("attachment_checker between", value1, value2, "attachmentChecker");
            return (Criteria) this;
        }

        public Criteria andAttachmentCheckerNotBetween(Integer value1, Integer value2) {
            addCriterion("attachment_checker not between", value1, value2, "attachmentChecker");
            return (Criteria) this;
        }

        public Criteria andAttachmentChecktimeIsNull() {
            addCriterion("attachment_checktime is null");
            return (Criteria) this;
        }

        public Criteria andAttachmentChecktimeIsNotNull() {
            addCriterion("attachment_checktime is not null");
            return (Criteria) this;
        }

        public Criteria andAttachmentChecktimeEqualTo(String value) {
            addCriterion("attachment_checktime =", value, "attachmentChecktime");
            return (Criteria) this;
        }

        public Criteria andAttachmentChecktimeNotEqualTo(String value) {
            addCriterion("attachment_checktime <>", value, "attachmentChecktime");
            return (Criteria) this;
        }

        public Criteria andAttachmentChecktimeGreaterThan(String value) {
            addCriterion("attachment_checktime >", value, "attachmentChecktime");
            return (Criteria) this;
        }

        public Criteria andAttachmentChecktimeGreaterThanOrEqualTo(String value) {
            addCriterion("attachment_checktime >=", value, "attachmentChecktime");
            return (Criteria) this;
        }

        public Criteria andAttachmentChecktimeLessThan(String value) {
            addCriterion("attachment_checktime <", value, "attachmentChecktime");
            return (Criteria) this;
        }

        public Criteria andAttachmentChecktimeLessThanOrEqualTo(String value) {
            addCriterion("attachment_checktime <=", value, "attachmentChecktime");
            return (Criteria) this;
        }

        public Criteria andAttachmentChecktimeLike(String value) {
            addCriterion("attachment_checktime like", value, "attachmentChecktime");
            return (Criteria) this;
        }

        public Criteria andAttachmentChecktimeNotLike(String value) {
            addCriterion("attachment_checktime not like", value, "attachmentChecktime");
            return (Criteria) this;
        }

        public Criteria andAttachmentChecktimeIn(List<String> values) {
            addCriterion("attachment_checktime in", values, "attachmentChecktime");
            return (Criteria) this;
        }

        public Criteria andAttachmentChecktimeNotIn(List<String> values) {
            addCriterion("attachment_checktime not in", values, "attachmentChecktime");
            return (Criteria) this;
        }

        public Criteria andAttachmentChecktimeBetween(String value1, String value2) {
            addCriterion("attachment_checktime between", value1, value2, "attachmentChecktime");
            return (Criteria) this;
        }

        public Criteria andAttachmentChecktimeNotBetween(String value1, String value2) {
            addCriterion("attachment_checktime not between", value1, value2, "attachmentChecktime");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublishtimeIsNull() {
            addCriterion("attachment_publishtime is null");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublishtimeIsNotNull() {
            addCriterion("attachment_publishtime is not null");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublishtimeEqualTo(String value) {
            addCriterion("attachment_publishtime =", value, "attachmentPublishtime");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublishtimeNotEqualTo(String value) {
            addCriterion("attachment_publishtime <>", value, "attachmentPublishtime");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublishtimeGreaterThan(String value) {
            addCriterion("attachment_publishtime >", value, "attachmentPublishtime");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublishtimeGreaterThanOrEqualTo(String value) {
            addCriterion("attachment_publishtime >=", value, "attachmentPublishtime");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublishtimeLessThan(String value) {
            addCriterion("attachment_publishtime <", value, "attachmentPublishtime");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublishtimeLessThanOrEqualTo(String value) {
            addCriterion("attachment_publishtime <=", value, "attachmentPublishtime");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublishtimeLike(String value) {
            addCriterion("attachment_publishtime like", value, "attachmentPublishtime");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublishtimeNotLike(String value) {
            addCriterion("attachment_publishtime not like", value, "attachmentPublishtime");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublishtimeIn(List<String> values) {
            addCriterion("attachment_publishtime in", values, "attachmentPublishtime");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublishtimeNotIn(List<String> values) {
            addCriterion("attachment_publishtime not in", values, "attachmentPublishtime");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublishtimeBetween(String value1, String value2) {
            addCriterion("attachment_publishtime between", value1, value2, "attachmentPublishtime");
            return (Criteria) this;
        }

        public Criteria andAttachmentPublishtimeNotBetween(String value1, String value2) {
            addCriterion("attachment_publishtime not between", value1, value2, "attachmentPublishtime");
            return (Criteria) this;
        }

        public Criteria andAttachmentInHypertextIsNull() {
            addCriterion("attachment_in_hypertext is null");
            return (Criteria) this;
        }

        public Criteria andAttachmentInHypertextIsNotNull() {
            addCriterion("attachment_in_hypertext is not null");
            return (Criteria) this;
        }

        public Criteria andAttachmentInHypertextEqualTo(Integer value) {
            addCriterion("attachment_in_hypertext =", value, "attachmentInHypertext");
            return (Criteria) this;
        }

        public Criteria andAttachmentInHypertextNotEqualTo(Integer value) {
            addCriterion("attachment_in_hypertext <>", value, "attachmentInHypertext");
            return (Criteria) this;
        }

        public Criteria andAttachmentInHypertextGreaterThan(Integer value) {
            addCriterion("attachment_in_hypertext >", value, "attachmentInHypertext");
            return (Criteria) this;
        }

        public Criteria andAttachmentInHypertextGreaterThanOrEqualTo(Integer value) {
            addCriterion("attachment_in_hypertext >=", value, "attachmentInHypertext");
            return (Criteria) this;
        }

        public Criteria andAttachmentInHypertextLessThan(Integer value) {
            addCriterion("attachment_in_hypertext <", value, "attachmentInHypertext");
            return (Criteria) this;
        }

        public Criteria andAttachmentInHypertextLessThanOrEqualTo(Integer value) {
            addCriterion("attachment_in_hypertext <=", value, "attachmentInHypertext");
            return (Criteria) this;
        }

        public Criteria andAttachmentInHypertextIn(List<Integer> values) {
            addCriterion("attachment_in_hypertext in", values, "attachmentInHypertext");
            return (Criteria) this;
        }

        public Criteria andAttachmentInHypertextNotIn(List<Integer> values) {
            addCriterion("attachment_in_hypertext not in", values, "attachmentInHypertext");
            return (Criteria) this;
        }

        public Criteria andAttachmentInHypertextBetween(Integer value1, Integer value2) {
            addCriterion("attachment_in_hypertext between", value1, value2, "attachmentInHypertext");
            return (Criteria) this;
        }

        public Criteria andAttachmentInHypertextNotBetween(Integer value1, Integer value2) {
            addCriterion("attachment_in_hypertext not between", value1, value2, "attachmentInHypertext");
            return (Criteria) this;
        }

        public Criteria andAttachmentDeletedIsNull() {
            addCriterion("attachment_deleted is null");
            return (Criteria) this;
        }

        public Criteria andAttachmentDeletedIsNotNull() {
            addCriterion("attachment_deleted is not null");
            return (Criteria) this;
        }

        public Criteria andAttachmentDeletedEqualTo(Integer value) {
            addCriterion("attachment_deleted =", value, "attachmentDeleted");
            return (Criteria) this;
        }

        public Criteria andAttachmentDeletedNotEqualTo(Integer value) {
            addCriterion("attachment_deleted <>", value, "attachmentDeleted");
            return (Criteria) this;
        }

        public Criteria andAttachmentDeletedGreaterThan(Integer value) {
            addCriterion("attachment_deleted >", value, "attachmentDeleted");
            return (Criteria) this;
        }

        public Criteria andAttachmentDeletedGreaterThanOrEqualTo(Integer value) {
            addCriterion("attachment_deleted >=", value, "attachmentDeleted");
            return (Criteria) this;
        }

        public Criteria andAttachmentDeletedLessThan(Integer value) {
            addCriterion("attachment_deleted <", value, "attachmentDeleted");
            return (Criteria) this;
        }

        public Criteria andAttachmentDeletedLessThanOrEqualTo(Integer value) {
            addCriterion("attachment_deleted <=", value, "attachmentDeleted");
            return (Criteria) this;
        }

        public Criteria andAttachmentDeletedIn(List<Integer> values) {
            addCriterion("attachment_deleted in", values, "attachmentDeleted");
            return (Criteria) this;
        }

        public Criteria andAttachmentDeletedNotIn(List<Integer> values) {
            addCriterion("attachment_deleted not in", values, "attachmentDeleted");
            return (Criteria) this;
        }

        public Criteria andAttachmentDeletedBetween(Integer value1, Integer value2) {
            addCriterion("attachment_deleted between", value1, value2, "attachmentDeleted");
            return (Criteria) this;
        }

        public Criteria andAttachmentDeletedNotBetween(Integer value1, Integer value2) {
            addCriterion("attachment_deleted not between", value1, value2, "attachmentDeleted");
            return (Criteria) this;
        }

        public Criteria andAttachmentAsynidIsNull() {
            addCriterion("attachment_asynid is null");
            return (Criteria) this;
        }

        public Criteria andAttachmentAsynidIsNotNull() {
            addCriterion("attachment_asynid is not null");
            return (Criteria) this;
        }

        public Criteria andAttachmentAsynidEqualTo(Integer value) {
            addCriterion("attachment_asynid =", value, "attachmentAsynid");
            return (Criteria) this;
        }

        public Criteria andAttachmentAsynidNotEqualTo(Integer value) {
            addCriterion("attachment_asynid <>", value, "attachmentAsynid");
            return (Criteria) this;
        }

        public Criteria andAttachmentAsynidGreaterThan(Integer value) {
            addCriterion("attachment_asynid >", value, "attachmentAsynid");
            return (Criteria) this;
        }

        public Criteria andAttachmentAsynidGreaterThanOrEqualTo(Integer value) {
            addCriterion("attachment_asynid >=", value, "attachmentAsynid");
            return (Criteria) this;
        }

        public Criteria andAttachmentAsynidLessThan(Integer value) {
            addCriterion("attachment_asynid <", value, "attachmentAsynid");
            return (Criteria) this;
        }

        public Criteria andAttachmentAsynidLessThanOrEqualTo(Integer value) {
            addCriterion("attachment_asynid <=", value, "attachmentAsynid");
            return (Criteria) this;
        }

        public Criteria andAttachmentAsynidIn(List<Integer> values) {
            addCriterion("attachment_asynid in", values, "attachmentAsynid");
            return (Criteria) this;
        }

        public Criteria andAttachmentAsynidNotIn(List<Integer> values) {
            addCriterion("attachment_asynid not in", values, "attachmentAsynid");
            return (Criteria) this;
        }

        public Criteria andAttachmentAsynidBetween(Integer value1, Integer value2) {
            addCriterion("attachment_asynid between", value1, value2, "attachmentAsynid");
            return (Criteria) this;
        }

        public Criteria andAttachmentAsynidNotBetween(Integer value1, Integer value2) {
            addCriterion("attachment_asynid not between", value1, value2, "attachmentAsynid");
            return (Criteria) this;
        }

        public Criteria andAttachmentVisitCounterIsNull() {
            addCriterion("attachment_visit_counter is null");
            return (Criteria) this;
        }

        public Criteria andAttachmentVisitCounterIsNotNull() {
            addCriterion("attachment_visit_counter is not null");
            return (Criteria) this;
        }

        public Criteria andAttachmentVisitCounterEqualTo(Integer value) {
            addCriterion("attachment_visit_counter =", value, "attachmentVisitCounter");
            return (Criteria) this;
        }

        public Criteria andAttachmentVisitCounterNotEqualTo(Integer value) {
            addCriterion("attachment_visit_counter <>", value, "attachmentVisitCounter");
            return (Criteria) this;
        }

        public Criteria andAttachmentVisitCounterGreaterThan(Integer value) {
            addCriterion("attachment_visit_counter >", value, "attachmentVisitCounter");
            return (Criteria) this;
        }

        public Criteria andAttachmentVisitCounterGreaterThanOrEqualTo(Integer value) {
            addCriterion("attachment_visit_counter >=", value, "attachmentVisitCounter");
            return (Criteria) this;
        }

        public Criteria andAttachmentVisitCounterLessThan(Integer value) {
            addCriterion("attachment_visit_counter <", value, "attachmentVisitCounter");
            return (Criteria) this;
        }

        public Criteria andAttachmentVisitCounterLessThanOrEqualTo(Integer value) {
            addCriterion("attachment_visit_counter <=", value, "attachmentVisitCounter");
            return (Criteria) this;
        }

        public Criteria andAttachmentVisitCounterIn(List<Integer> values) {
            addCriterion("attachment_visit_counter in", values, "attachmentVisitCounter");
            return (Criteria) this;
        }

        public Criteria andAttachmentVisitCounterNotIn(List<Integer> values) {
            addCriterion("attachment_visit_counter not in", values, "attachmentVisitCounter");
            return (Criteria) this;
        }

        public Criteria andAttachmentVisitCounterBetween(Integer value1, Integer value2) {
            addCriterion("attachment_visit_counter between", value1, value2, "attachmentVisitCounter");
            return (Criteria) this;
        }

        public Criteria andAttachmentVisitCounterNotBetween(Integer value1, Integer value2) {
            addCriterion("attachment_visit_counter not between", value1, value2, "attachmentVisitCounter");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * attachment 2019-01-13
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}