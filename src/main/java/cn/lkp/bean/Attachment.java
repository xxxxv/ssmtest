package cn.lkp.bean;

import javax.persistence.Table;
import org.springframework.stereotype.Component;

/**
 * attachment
 * @author 杨孟新 2019-01-13
 */
@Table(name = "attachment")

@Component
public class Attachment {
    private Integer attachmentId;

    private String attachmentUuid;

    private Integer attachmentPublisher;

    private String attachmentTitle;

    private String attachmentName;

    private String attachmentSrc;

    private String attachmentDescription;

    private String attachmentType;

    private String attachmentSize;

    private Integer attachmentDownloadCounter;

    private Integer attachmentChecked;

    private Integer attachmentChecker;

    private String attachmentChecktime;

    private String attachmentPublishtime;

    private Integer attachmentInHypertext;

    private Integer attachmentDeleted;

    private Integer attachmentAsynid;

    private Integer attachmentVisitCounter;

    public Integer getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Integer attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getAttachmentUuid() {
        return attachmentUuid;
    }

    public void setAttachmentUuid(String attachmentUuid) {
        this.attachmentUuid = attachmentUuid == null ? null : attachmentUuid.trim();
    }

    public Integer getAttachmentPublisher() {
        return attachmentPublisher;
    }

    public void setAttachmentPublisher(Integer attachmentPublisher) {
        this.attachmentPublisher = attachmentPublisher;
    }

    public String getAttachmentTitle() {
        return attachmentTitle;
    }

    public void setAttachmentTitle(String attachmentTitle) {
        this.attachmentTitle = attachmentTitle == null ? null : attachmentTitle.trim();
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName == null ? null : attachmentName.trim();
    }

    public String getAttachmentSrc() {
        return attachmentSrc;
    }

    public void setAttachmentSrc(String attachmentSrc) {
        this.attachmentSrc = attachmentSrc == null ? null : attachmentSrc.trim();
    }

    public String getAttachmentDescription() {
        return attachmentDescription;
    }

    public void setAttachmentDescription(String attachmentDescription) {
        this.attachmentDescription = attachmentDescription == null ? null : attachmentDescription.trim();
    }

    public String getAttachmentType() {
        return attachmentType;
    }

    public void setAttachmentType(String attachmentType) {
        this.attachmentType = attachmentType == null ? null : attachmentType.trim();
    }

    public String getAttachmentSize() {
        return attachmentSize;
    }

    public void setAttachmentSize(String attachmentSize) {
        this.attachmentSize = attachmentSize == null ? null : attachmentSize.trim();
    }

    public Integer getAttachmentDownloadCounter() {
        return attachmentDownloadCounter;
    }

    public void setAttachmentDownloadCounter(Integer attachmentDownloadCounter) {
        this.attachmentDownloadCounter = attachmentDownloadCounter;
    }

    public Integer getAttachmentChecked() {
        return attachmentChecked;
    }

    public void setAttachmentChecked(Integer attachmentChecked) {
        this.attachmentChecked = attachmentChecked;
    }

    public Integer getAttachmentChecker() {
        return attachmentChecker;
    }

    public void setAttachmentChecker(Integer attachmentChecker) {
        this.attachmentChecker = attachmentChecker;
    }

    public String getAttachmentChecktime() {
        return attachmentChecktime;
    }

    public void setAttachmentChecktime(String attachmentChecktime) {
        this.attachmentChecktime = attachmentChecktime == null ? null : attachmentChecktime.trim();
    }

    public String getAttachmentPublishtime() {
        return attachmentPublishtime;
    }

    public void setAttachmentPublishtime(String attachmentPublishtime) {
        this.attachmentPublishtime = attachmentPublishtime == null ? null : attachmentPublishtime.trim();
    }

    public Integer getAttachmentInHypertext() {
        return attachmentInHypertext;
    }

    public void setAttachmentInHypertext(Integer attachmentInHypertext) {
        this.attachmentInHypertext = attachmentInHypertext;
    }

    public Integer getAttachmentDeleted() {
        return attachmentDeleted;
    }

    public void setAttachmentDeleted(Integer attachmentDeleted) {
        this.attachmentDeleted = attachmentDeleted;
    }

    public Integer getAttachmentAsynid() {
        return attachmentAsynid;
    }

    public void setAttachmentAsynid(Integer attachmentAsynid) {
        this.attachmentAsynid = attachmentAsynid;
    }

    public Integer getAttachmentVisitCounter() {
        return attachmentVisitCounter;
    }

    public void setAttachmentVisitCounter(Integer attachmentVisitCounter) {
        this.attachmentVisitCounter = attachmentVisitCounter;
    }
}