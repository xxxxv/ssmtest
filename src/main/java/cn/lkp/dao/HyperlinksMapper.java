package cn.lkp.dao;

import cn.lkp.bean.Hyperlinks;
import cn.lkp.bean.HyperlinksExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface HyperlinksMapper {
    long countByExample(HyperlinksExample example);

    int deleteByExample(HyperlinksExample example);

    int deleteByPrimaryKey(Integer linkId);

    int insert(Hyperlinks record);

    int insertSelective(Hyperlinks record);

    List<Hyperlinks> selectByExample(HyperlinksExample example);

    Hyperlinks selectByPrimaryKey(Integer linkId);

    int updateByExampleSelective(@Param("record") Hyperlinks record, @Param("example") HyperlinksExample example);

    int updateByExample(@Param("record") Hyperlinks record, @Param("example") HyperlinksExample example);

    int updateByPrimaryKeySelective(Hyperlinks record);

    int updateByPrimaryKey(Hyperlinks record);
}