package cn.lkp.dao;

import cn.lkp.bean.Hypertext;
import cn.lkp.bean.HypertextExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface HypertextMapper {
    long countByExample(HypertextExample example);

    int deleteByExample(HypertextExample example);

    int deleteByPrimaryKey(Integer textId);

    int insert(Hypertext record);

    int insertSelective(Hypertext record);

    List<Hypertext> selectByExample(HypertextExample example);

    Hypertext selectByPrimaryKey(Integer textId);

    int updateByExampleSelective(@Param("record") Hypertext record, @Param("example") HypertextExample example);

    int updateByExample(@Param("record") Hypertext record, @Param("example") HypertextExample example);

    int updateByPrimaryKeySelective(Hypertext record);

    int updateByPrimaryKey(Hypertext record);
}