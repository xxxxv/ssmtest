package cn.lkp.dao;

import cn.lkp.bean.Resset;
import cn.lkp.bean.RessetExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RessetMapper {
    long countByExample(RessetExample example);

    int deleteByExample(RessetExample example);

    int deleteByPrimaryKey(Integer setId);

    int insert(Resset record);

    int insertSelective(Resset record);

    List<Resset> selectByExample(RessetExample example);

    Resset selectByPrimaryKey(Integer setId);

    int updateByExampleSelective(@Param("record") Resset record, @Param("example") RessetExample example);

    int updateByExample(@Param("record") Resset record, @Param("example") RessetExample example);

    int updateByPrimaryKeySelective(Resset record);

    int updateByPrimaryKey(Resset record);
}