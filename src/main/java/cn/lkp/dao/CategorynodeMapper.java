package cn.lkp.dao;

import cn.lkp.bean.Categorynode;
import cn.lkp.bean.CategorynodeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CategorynodeMapper {
    long countByExample(CategorynodeExample example);

    int deleteByExample(CategorynodeExample example);

    int deleteByPrimaryKey(Integer categoryId);

    int insert(Categorynode record);

    int insertSelective(Categorynode record);

    List<Categorynode> selectByExample(CategorynodeExample example);

    Categorynode selectByPrimaryKey(Integer categoryId);

    int updateByExampleSelective(@Param("record") Categorynode record, @Param("example") CategorynodeExample example);

    int updateByExample(@Param("record") Categorynode record, @Param("example") CategorynodeExample example);

    int updateByPrimaryKeySelective(Categorynode record);

    int updateByPrimaryKey(Categorynode record);
}