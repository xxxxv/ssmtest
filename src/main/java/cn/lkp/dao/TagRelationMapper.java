package cn.lkp.dao;

import cn.lkp.bean.TagRelation;
import cn.lkp.bean.TagRelationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TagRelationMapper {
    long countByExample(TagRelationExample example);

    int deleteByExample(TagRelationExample example);

    int deleteByPrimaryKey(Integer tagRelationId);

    int insert(TagRelation record);

    int insertSelective(TagRelation record);

    List<TagRelation> selectByExample(TagRelationExample example);

    TagRelation selectByPrimaryKey(Integer tagRelationId);

    int updateByExampleSelective(@Param("record") TagRelation record, @Param("example") TagRelationExample example);

    int updateByExample(@Param("record") TagRelation record, @Param("example") TagRelationExample example);

    int updateByPrimaryKeySelective(TagRelation record);

    int updateByPrimaryKey(TagRelation record);
}